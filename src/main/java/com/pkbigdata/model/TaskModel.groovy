package com.pkbigdata.model

/**
 * Created by jay on 2016/12/14 0014.
 * 定时任务模型
 */
class TaskModel {
    String taskname
    String taskclass="com.songwie.task.task.MyTask"//可以不传
    String taskurl//链接
    String taskpost="1"//post为1,get为2
    String taskparams//可以不传
    String cronexpression//时间
    String taskid = ""//任务id

    String getTaskname() {
        return taskname
    }

    void setTaskname(String taskname) {
        this.taskname = taskname
    }

    String getTaskclass() {
        return taskclass
    }

    void setTaskclass(String taskclass) {
        this.taskclass = taskclass
    }

    String getTaskurl() {
        return taskurl
    }

    void setTaskurl(String taskurl) {
        this.taskurl = taskurl
    }

    String getTaskpost() {
        return taskpost
    }

    void setTaskpost(String taskpost) {
        this.taskpost = taskpost
    }

    String getTaskparams() {
        return taskparams
    }

    void setTaskparams(String taskparams) {
        this.taskparams = taskparams
    }

    String getCronexpression() {
        return cronexpression
    }

    void setCronexpression(String cronexpression) {
        this.cronexpression = cronexpression
    }

    String getTaskid() {
        return taskid
    }

    void setTaskid(String taskid) {
        this.taskid = taskid
    }
}
