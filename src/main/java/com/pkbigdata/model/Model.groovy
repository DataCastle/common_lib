package com.pkbigdata.model


import com.fasterxml.jackson.annotation.JsonIgnore
import com.pkbigdata.util.JsonToMap

/**
 * Created by 赵仁杰 on 2015/3/25.
 * 控制器使用的通用model
 */
public class Model {

    private Map<String, Object> data = new HashMap<String, Object>();
    public String msg = "";
    public Boolean flag = false;
    public Boolean login = true;


    public Model(){

    }
    public Model(String msg){
        this.msg=msg;

    }
    public Model(String msg, Map<String, Object> data){
        this.msg = msg;
        this.data = data;
    }
    public Model(Map<String, Object> data){
        this.data = data
    }

    /**
     * 清空map
     */
    public void clear() {
        msg = "";
        data.clear();
        flag = false;
    }

    public Model put(String key, Object value) {
        data.put(key, value);
        return this;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public Model setData(Map<String, Object> data) {
        this.data = data;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public Model setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Boolean getFlag() {
        return flag;
    }

    public Model setFlag(Boolean flag) {
        this.flag = flag;
        return this;
    }

    public Boolean getLogin() {
        return login;
    }

    public Model setLogin(Boolean login) {
        this.login = login;
        return this;
    }
    @JsonIgnore
    private Boolean isInclude = false;
    /**
     * 包含
     * @param inPath
     * @return
     */
    def includes(List<String> inPath){
        if (isInclude){
            throw new Exception("method includes can not allow use twice")
        }
        def map = JsonToMap.jsonToMap(JsonToMap.beanToJson(getData()))
        def orienteds = new HashMap<Map,List>()
        def name = new ArrayList<String>()
        inPath.each {path->
            def paths = path.split("/")
            def oriented = [map] as List
            paths.eachWithIndex { String entry, int i ->
                if (i<paths.length-1){
                    name.add(entry)
                    def temp = [] as List
                    oriented.each {o->
                        def result = o[entry]
                        if (result instanceof List){
                            temp.addAll(result)
                        }else{
                            temp.add(result)
                        }
                    }
                    oriented = temp
                }else {
                    oriented.each {o->
                        def params = orienteds.get(o)
                        if (!params)
                            params = new ArrayList()
                        params.add(entry)
                        orienteds.put(o,params)
                    }
                }
            }
        }
        orienteds.each {m,l->
            Iterator<Map.Entry<Object,Object>> iterator = m.entrySet().iterator()
            while (iterator.hasNext()){
                Map.Entry<Object,Object> entry = iterator.next();
                if (!l.contains(entry.key)&&!name.contains(entry.key)){
                    iterator.remove()
                }
            }
        }
        isInclude = true
        this.setData(map)
    }
    /**
     * 批量排除
     * 处理的根目录是model里的data，不是model,切记切记
     * 不要用/开始和结尾
     * @param paths
     * @return
     */
    def excludes(List<String> paths){
        paths.each {
            exclude(it)
        }
        return this
    }
    /**
     * 单条路径排除,不要用/开始和结尾
     * 处理的根目录是model里的data，不是model,切记切记
     * data/pageSize
     * @param path
     * @return
     */
    private def exclude(String path){
        def map = JsonToMap.jsonToMap(JsonToMap.beanToJson(getData()))
        def paths = path.split("/")
        def oriented = [map] as List
        paths.eachWithIndex { String entry, int i ->
            if (i<paths.length-1){
                def temp = [] as List
                oriented.each {o->
                    def result = o[entry]
                    if (result instanceof List){
                        temp.addAll(result)
                    }else{
                        temp.add(result)
                    }
                }
                oriented = temp
            }else {
                oriented.each {
                    if(it)
                    it.remove(entry)
                }
            }
        }
        this.setData(map)
    }

    Boolean getIsInclude() {
        return isInclude
    }

    void setIsInclude(Boolean isInclude) {
        this.isInclude = isInclude
    }
}
