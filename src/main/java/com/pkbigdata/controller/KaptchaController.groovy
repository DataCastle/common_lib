package com.pkbigdata.controller

import com.google.code.kaptcha.Constants
import com.google.code.kaptcha.impl.DefaultKaptcha
import com.pkbigdata.model.Model
import com.pkbigdata.model.Mv
import com.pkbigdata.service.util.RedisUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

import javax.annotation.Resource
import javax.imageio.ImageIO
import javax.servlet.ServletOutputStream
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.awt.image.BufferedImage
import java.util.concurrent.TimeUnit

/**
 * Created by jay on 2016/5/9.
 */
@Controller('kc')
@RequestMapping("/common/allow")
class KaptchaController {
    @Resource(name = "captchaProducer")
    DefaultKaptcha captchaProducer;
    @Value('${host}')
    String HOST;
    @Autowired
    RedisUtil redisUtil

    /**
     * 验证码实例
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping("/captcha.image")
    public void initCaptcha(HttpServletRequest request,
                            HttpServletResponse response) throws Exception {

        response.setDateHeader("Expires", 0);
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        response.setContentType("image/jpeg");
        String capText = captchaProducer.createText();
        request.getSession().setAttribute(Constants.KAPTCHA_SESSION_KEY, capText);
        BufferedImage bi = captchaProducer.createImage(capText);

        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(bi, "jpg", out);
        System.out.println("test");
        System.out.println("test");

        try {
            out.flush();
        } finally {
            out.close();
        }
    }
    /**
     * 滑动验证码
     * 需要refer
     * 需要cookie
     * 需要加载验证码的样式图，确保未被缓存干扰
     * 需要加载一个伪造小图片
     * 需要屏幕像素 SR
     * 需要鼠标像素点（起始点）MP
     */
    @RequestMapping("/slideCaptcha")
    @ResponseBody
    def slideCaptcha(@CookieValue(required = false) String SR,
                     @RequestHeader(required = false) String MP,
                     @RequestHeader(name = "Referer",required = false) String refer,
                     HttpServletRequest request){
        Model model = new Model()
        //def mp = MP.split('\\*')
        //def length = Math.sqrt(Math.pow(mp[2].toInteger()-mp[0].toInteger(),2)+Math.pow(mp[3].toInteger()-mp[1].toInteger(),2))
        //print("鼠标轨迹长度：${length}")
        if (!(SR&&MP&&refer)){
            return model.setMsg("fail:屏幕像素，鼠标轨迹不存在");
        }
        if((SR+MP).contains("un")){
            return model.setMsg("fail:屏幕或者鼠标参数未定义")
        }
        if (!(redisUtil.getValue(request.getSession().getId()+refer+"gou")!=null
                &&redisUtil.getValue(request.getSession().getId()+refer+"logo")!=null)){
            return model.setMsg("fail：前置图像未加载")
        }
        if (MP.count("*")!=3){
            return model.setMsg("fail：鼠标起始点参数错误")
        }
        if (SR.count("*")!=1){
            return model.setMsg("fail：屏幕像素参数错误")
        }
        //def mp = MP.split('\\\\*')
        //def length = Math.sqrt(Math.pow(mp[2].toInteger()-mp[0].toInteger(),2)+Math.pow(mp[3].toInteger()-mp[1].toInteger(),2))
        //print("鼠标轨迹长度：${length}")
        redisUtil.addValue(request.getSession().getId()+refer,true)
        return model.setFlag(true)
    }
    /**
     * 样式图
     * @return
     */
    @RequestMapping("/gou.jpg")
    def captchaCheat(@RequestHeader(name = "Referer",required = false) String refer, HttpServletRequest request){
        redisUtil.addValue(request.getSession().getId()+refer+"gou","",30, TimeUnit.MINUTES)
        return "redirect:/DCTrain/static/img/user/common/gou.png"
    }


    @RequestMapping("/logo.png")
    def logo(@RequestHeader(name = "Referer",required = false) String refer, HttpServletRequest request){
        redisUtil.addValue(request.getSession().getId()+refer+"logo","",30, TimeUnit.MINUTES)
        return "redirect:/DCTrain/static/img/user/common/logo-new.png"
    }


    /**
     * 滑动验证码
     * 需要refer
     * 需要cookie
     * 需要加载验证码的样式图，确保未被缓存干扰
     * 需要加载一个伪造小图片
     * 需要屏幕像素 SR
     * 需要鼠标像素点（起始点）MP
     */
    @RequestMapping("/clickCaptcha")
    @ResponseBody
    def clickCaptcha(@RequestHeader(name = "Referer",required = false) String refer,
                     HttpServletRequest request){
        Model model = new Model()
        redisUtil.addValue(request.getSession().getId()+refer,true)
        return model.setFlag(true)
    }

    @RequestMapping("/defaultAvatar.image")
    def defaultAvatar(@RequestParam(required = false,defaultValue = "4407")Integer id){
        new Mv("redirect:/a_new_static/img/user/avatar/${(id%54)+1}.jpg")
    }

}
