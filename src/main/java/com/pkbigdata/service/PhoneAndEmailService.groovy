package com.pkbigdata.service

/**
 * Created by Administrator on 2017/7/14.
 */
interface PhoneAndEmailService extends BaseService {
    /**
     * 发送手机短息-云片国内营销短信
     * @param phone
     * @param content
     * @return
     */
    boolean sendPhoneMessage(String apiKey,String url,String phone,String content,Map map);

    /**
     * 异步发送邮件
     * @param emailMap 邮件列表，
     * @param content 内容
     * @param subject 邮件主题
     * @return
     */
    def asynSendEmail(Map emailMap,def content,def subject)
}