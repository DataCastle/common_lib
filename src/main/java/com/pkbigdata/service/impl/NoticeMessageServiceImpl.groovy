package com.pkbigdata.service.impl

import com.pkbigdata.service.NoticeMessageService
import com.pkbigdata.util.HttpUtils
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Created by Administrator on 2017/7/25.
 */
@Transactional
@Service
class NoticeMessageServiceImpl extends  BaseServiceImpl implements  NoticeMessageService{

    @Override
    def sendEmail(String toEmails, String title, String content, String url) {
       Map paramMap = new HashMap()
        paramMap.put("toEmails",toEmails)
        paramMap.put("title",title)
        paramMap.put("content",content)
        Map resultMap =  HttpUtils.getResponse(url,"post",paramMap)
        return resultMap.get("flag")
    }


    @Override
    def sendMarketingSms(String phone, String content, String url) {
        Map paramMap = new HashMap()
        paramMap.put("phone",phone)
        paramMap.put("content",content)
        Map resultMap =  HttpUtils.getResponse(url,"post",paramMap)
        return resultMap.get("flag")
    }

    @Override
    def sendNoticeSms(String phone, String vCode, String expTime,String url) {
        Map paramMap = new HashMap()
        paramMap.put("phone",phone)
        paramMap.put("vCode",vCode)
        paramMap.put("expTime",expTime)
        Map resultMap =  HttpUtils.getResponse(url,"post",paramMap)
        return resultMap.get("flag")
    }
}
