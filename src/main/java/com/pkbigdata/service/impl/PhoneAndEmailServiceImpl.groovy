package com.pkbigdata.service.impl

import com.pkbigdata.entity.email.GroupMail
import com.pkbigdata.entity.email.User
import com.pkbigdata.service.PhoneAndEmailService
import com.pkbigdata.service.util.PhoneMeg
import com.pkbigdata.util.CommonUtil
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import javax.annotation.Resource
import java.sql.Timestamp

/**
 * Created by Administrator on 2017/7/14.
 */
@Transactional
@Service
class PhoneAndEmailServiceImpl extends BaseServiceImpl implements  PhoneAndEmailService {
    @Resource
    private PhoneMeg phoneMeg;
    @Override
    boolean sendPhoneMessage(String phone, String content,String apiKey,String url,Map map) {
        phoneMeg.sendTemplateSMS(apiKey,url,phone,content,map)
    }

    /**
     * 异步发送邮件
     * @param emailMap 邮件列表，
     * @param content 内容
     * @param subject 邮件主题
     * @return
     */
    @Override
    def asynSendEmail(Map emailMap, Object content, Object subject) {
        //接收邮件
        String uuid = UUID.randomUUID().toString()
        List<User> users = new ArrayList<>()
        emailMap.each {mail->
            if (CommonUtil.isEmail(mail.key.toString())){
                User user = new User()
                user.with {
                    setEmail(mail.key.toString())
                    setName(mail.value.toString())
                    setFile(uuid+subject)
                    setTime(new Timestamp(System.currentTimeMillis()))
                }
                users.add(user)
            }
        }
        batchSave(users)
        GroupMail groupMail = new GroupMail()
        groupMail.with {
            it.setContent(content.toString())
            it.setCreateTime(new Timestamp(System.currentTimeMillis()))
            it.setFile(uuid+subject)
            it.setSub(subject.toString())
        }
        save(groupMail)
        return true
    }


}
