package com.pkbigdata.service;

import org.hibernate.type.Type;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by jay on 2014/11/28.
 */
public interface BaseService {

    /**
     * 保存实体
     * @param entity
     * @param <T>
     */
    public <T> void save(T entity);

    /**
     * 更新实体
     * @param entity
     * @param <T>
     */
    public <T> void saveOrUpdate(T entity);

    /**
     * 删除实体
     * @param entity
     * @param <T>
     */
    public <T> void delete(T entity);

    public <T> void batchUpdate(List<T> entitys);

    /**
     * 批量保存
     * @param entitys
     * @param <T>
     */
    public <T> void batchSave(List<T> entitys);

    /**
     *通过id获取实体
     * @param t
     * @param id
     * @param <T>
     * @return
     */
    public <T> T get(Class<T> t, String id);

    /**
     * 根据实体名称和实体属性获取唯一值
     * @param t
     * @param propertyName
     * @param value
     * @param <T>
     * @return
     */
    public <T> T findUniqueByProperty(Class<T> t, String propertyName, Object value);

    /**
     * 通过实体名和实体属性获取列表值
     * @param entityClass
     * @param propertyName
     * @param value
     * @param <T>
     * @return
     */
    public <T> List<T> findByProperty(Class<T> entityClass,
                                      String propertyName, Object value);

    /**
     * 通过实体名获取全部实体
     * @param entityClass
     * @param <T>
     * @return
     */
    public <T> List<T> getAll(Class<T> entityClass);

    /**
     * 通过id,删除实体
     * @param entityClass
     * @param id
     * @param <T>
     */
    public <T> void deleteEntityById(Class<T> entityClass, String id);

    /**
     * 通过实体集合删除集合
     * @param entities
     * @param <T>
     */
    public <T> void deleteAllEntitie(Collection<T> entities);

    /**
     * 通过hql获取实体列表
     * @param hql
     * @param <T>
     * @return
     */
    public <T> List<T> findByHqlString(String hql);

    /**
     * 通过sql更新
     * @param sql
     * @return
     */
    public int updateBySqlString(String sql);

    /**
     * 通过sql查找list
     * @param query
     * @param <T>
     * @return
     */
    public <T> List<T> findListbySql(String query);

    /**
     * 通过属性查找列表，并排序
     * @param entityClass
     * @param propertyName
     * @param value
     * @param isAsc
     * @param <T>
     * @return
     */
    public <T> List<T> findByPropertyisOrder(Class<T> entityClass, String propertyName, Object value, boolean isAsc);

    /**
     * 通过hql获取唯一实体
     * @param hql
     * @param <T>
     * @return
     */
    public <T> T findsingleResult(String hql);
    
    /**
     * 单表分页
     * @return
     */
    public <T> Map<String,Object> queryForPage(Class<T> entityClass, int page, int pagesize);
    /**
     * 单表分页查询，通过属性排序,TRUE则使用order by property ASC
     * false 则使用order by property DESC
     * @param entityClass
     * @param page
     * @param pagesize
     * @param propertyName
     * @param isAsc
     * @return
     */
    public <T> Map<String,Object> queryForPage(Class<T> entityClass, int page,
                                               int pagesize, String propertyName, boolean isAsc);
    /**
     * 通过hql分页
     * @param hql
     * @param page
     * @param pagesize
     * @return
     */
    public <T> Map<String,Object> queryForPageByHQL(String hql, int page, int pagesize);
    
   
	/**
	 * 执行绑定参数的hql。可用于添加，修改，删除
	 * @param hql 在HQL查询语句中用”?”来定义参数位置
	 * 
	 * @param params 可变参数 注意：参数只能是基本数据类型
	 */
	public void executeHql(String hql, Object... params);
	
	/**
	 * 执行绑定参数的hql。可用于添加，修改，删除
	 * @param hql 使用name占位符 即在HQL语句中定义命名参数要用”:”开头
	 * @param params 可变参数 注意：参数只能是基本数据类型
	 */
	public void executeHql(String hql, Map<String, Object> params);
	
	
	/**
	 * 执行绑定参数的hql。用于查询出单个对象
	 * @param hql 在HQL查询语句中用”?”来定义参数位置
	 * @param params 可变参数 注意：参数只能是基本数据类型
	 * @return 单个查询对象
	 */
	public <T> T selectByHql(String hql, Object... params);
	
	/**
	 * 执行绑定参数的hql。用于查询出集合对象
	 * @param hql 在HQL查询语句中用”?”来定义参数位置
	 * @param params 可变参数 注意：参数只能是基本数据类型
	 * @return 集合对象
	 */
	public <T> List<T> selectListByHql(String hql, Object... params);

    /**
     * 执行绑定参数的hql。用于查询出集合对象
     * @param hql 在HQL查询语句中用”?”来定义参数位置
     * @param params 可变参数 注意：参数只能是基本数据类型
     * @param limit 取这条hql的前几条数据
     * @return 集合对象
     */
     public <T> List<T> selectListByLimit(String hql, int limit, Object... params);

	/**
	 * 执行绑定参数SQL。用于添加，修改，删除
	 * @param sql 用“？”来定义参数位置
	 * @param params 基本数据类型的参数
	 */
	public void executeSql(String sql, Object... params);
	
	/**
	 * 执行绑定参数的SQL。用于查询单个对象
	 * @param sql 用“？”来定义参数位置
	 * @param params 基本数据类型的参数
	 * @return
	 */
	public <T> T selectBySql(String sql, Class<T> entity, Object... params);
    /**
     * 执行绑定参数的SQL。用于查询单个对象
     * @param sql 用“？”来定义参数位置
     * @param params 基本数据类型的参数
     * @return
     */
    public <T> List<T> selectBySql(String sql, Object... params);
    public <T> List<T> selectBySql(String sql, Map<String, Type> type, Object... params);

    /**
     * 执行绑定参数的SQL。用于查询单个对象
     * @param sql 用“？”来定义参数位置
     * @param params 基本数据类型的参数
     * @return
     */
    public <T> List<T> selectListBySql(String sql, Class<T> entity, Object... params);

    public <T> Map<String, Object> queryForPageByHQL(String hql, int page,
                                                     int pageSize, Object... params);

    public <T> Map<String, Object> queryForPageBySQL(String sql, int page,
                                                     int pageSize, Object... params);
    public <T> Map<String, Object> queryForPageBySQL(String sql, int page,
                                                     int pageSize);
    public <T> Map<String, Object> queryForPageBySQL(String sql, Class<T> entity,
                                                     int page, int pageSize, Object... params);

    /**
     * 通过hql取第一条
     * @param hql
     * @param <T>
     * @return
     */
    public <T> T findByHqlFirst(String hql);
    /**
     * 通过hql取第一条
     * @param hql
     * @param <T>
     * @return
     */
    public <T> T findByHqlFirst(String hql, Object... params);

    public Integer getTableCount(String sql);


    /**
     * 多参数查找，严格遵循params的长度是2的倍数
     * @param entity
     * @param params
     * @param <T>
     * @return
     */
    public <T> List<T> findByProperties(Class<T> entity, Object... params);

    /**
     * 排序，多参数查找，严格遵循params的长度是2的倍数,
     * @param entity 实体表名
     * @param orderProperty 排序字段
     * @param isAsc 怎么排序
     * @param params 查找参数
     * @param <T>
     * @return
     */
    public <T> List<T> findByProperties(Class<T> entity, String orderProperty, boolean isAsc, Object... params);

    /**
     * currentSession.flush
     */
    void flush();

}
