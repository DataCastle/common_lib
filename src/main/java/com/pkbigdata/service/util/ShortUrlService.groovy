package com.pkbigdata.service.util

import com.pkbigdata.entity.util.DcShortUrl
import com.pkbigdata.service.BaseService
import com.pkbigdata.service.util.RedisUtil
import com.pkbigdata.util.CommonUtil
import com.pkbigdata.util.ConstantsConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * Created by ck on 2017/2/9.
 */
@Component
class ShortUrlService {
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    BaseService service
    /**
     * 长链接转短连接
     * @param longUrl
     * @return
     */
     String  longUrl2shortUrl(String longUrl){
        String shortUrl = CommonUtil.encLongUrl()
        shortUrl = ConstantsConfig.SHORT_HOST+shortUrl
        DcShortUrl dcShortUrl = new DcShortUrl()
        dcShortUrl.setLongUrl(longUrl)
        dcShortUrl.setShortUrl(shortUrl)
        service.saveOrUpdate(dcShortUrl)
        redisUtil.addValue(shortUrl,longUrl)
        return  shortUrl;
    }

    /**
     * 通过短链接获取长链接
     * @param shortUrl
     * @return
     */
    String getLongUrl(String shortUrl){
         String longUrl = redisUtil.getValue(shortUrl)
          if(!longUrl){
             DcShortUrl dcShortUrl =  service.selectByHql("from DcShortUrl where shortUrl = ? ",shortUrl)
              if(dcShortUrl){
                  longUrl = dcShortUrl.getLongUrl()
              }
            }
        return longUrl
    }



}
