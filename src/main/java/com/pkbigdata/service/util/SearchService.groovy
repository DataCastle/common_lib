package com.pkbigdata.service.util

import com.pkbigdata.entity.bbs.BbsTopic
import com.pkbigdata.util.HttpUtils
import com.pkbigdata.util.JsonToMap
import groovy.json.JsonOutput
import groovy.json.JsonParser
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class SearchService {
    @Value('${search-engine}')
    String searchEngine
    @Value('${dev}')
    String dev
    @Value('${cachePrefix}')
    String cachePrefix

    public static Map search_user_password = ['Authorization':"Basic ${Base64.getEncoder().encodeToString('elastic:changeme'.getBytes('UTF-8'))}".toString()]

    /**
     * 通用搜索
     * @param index 哪一个数据库里面的数据,例如轻社区-帖子列表 lightBBS-topics
     * @param words 搜索内容 大师赛 关键词空格表示或
     */
    def commonSearch(String index,String words,int page, int pageSize){
        String url = "${searchEngine}/${(cachePrefix+dev+index).toLowerCase()}/employee/_search?size=${pageSize}&from=${(page-1)*pageSize}"

        def sw = ['query':['match':['_all':words]]]

        def response = HttpUtils.post(url,JsonToMap.beanToJson(sw),
                ['Authorization':"Basic ${Base64.getEncoder().encodeToString('elastic:changeme'.getBytes('UTF-8'))}".toString()])
        def result = JsonToMap.jsonToMap(response)

        List list = result['hits']['hits']['_source'] as List;
        Long allRow = result['hits']['total'].toString().toLong();

        return page_util(list,allRow,page,pageSize)

    }

   static def page_util(List result,Long sum,int page, int pageSize){
        Map<String,Object> map = new HashMap<String,Object>();
        List list = result; // 要返回的某一页的记录列表
        Long allRow = sum; // 总记录数
        int totalPage = (int) (allRow%pageSize==0?allRow/pageSize:allRow/pageSize+1); // 总页数
        int currentPage = page; // 当前页
        boolean isFirstPage = currentPage==1; // 是否为第一页
        boolean isLastPage = currentPage==totalPage; // 是否为最后一页
        boolean hasPreviousPage = currentPage>1; // 是否有前一页
        boolean hasNextPage = currentPage<totalPage; // 是否有下一页

        map.put("list", list);
        map.put("allRow", allRow);
        map.put("totalPage", totalPage);
        map.put("currentPage", currentPage);
        map.put("pageSize", pageSize);
        map.put("isFirstPage", isFirstPage);
        map.put("isLasePage", isLastPage);
        map.put("hasPreviousPage", hasPreviousPage);
        map.put("hasNextPage", hasNextPage);
       return map
    }

    /**
     * 支持批量增加或者单独,推荐使用自定义ID
     * 采用的是save or update
     * source,key是id,value是对应的值
     * @param index 是索引名称
     * @param source
     * @return
     */
    def index(String index,Map<String,Object> source){
        def text = new StringBuilder("")
        String url = "${searchEngine}/_bulk"
        source.each {
            text.append("{ \"index\": { \"_index\": \"${cachePrefix+dev+index}\", \"_type\": \"employee\", \"_id\": \"${it.key}\" }}\n")
            text.append(JsonToMap.beanToJsonWithDataFormat(it.value)+"\n")
        }
        HttpUtils.post(url,text.toString(),search_user_password)
    }

    /**
     * 支持批量增加或者单独
     * 采用的是save or update
     * source需要索引的对象列表，id是随机的
     * @param index 是索引名称
     * @param source
     * @return
     */
    def index(String index,List<Object> source){
        def text = new StringBuilder("")
        String url = "${searchEngine}/_bulk"
        source.each {
            text.append("{ \"index\": { \"_index\": \"${cachePrefix+dev+index}\", \"_type\": \"employee\"}}\n")
            text.append(JsonToMap.beanToJsonWithDataFormat(it)+"\n")
        }
        HttpUtils.post(url,text.toString(),search_user_password)
    }
}
