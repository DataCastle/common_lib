package com.pkbigdata.service.util

import com.pkbigdata.service.BaseService
import com.sun.org.apache.xpath.internal.operations.Bool
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.redis.core.HashOperations
import org.springframework.data.redis.core.ListOperations
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.core.SetOperations
import org.springframework.data.redis.core.ValueOperations
import org.springframework.data.redis.core.ZSetOperations
import org.springframework.stereotype.Service

import javax.annotation.Resource
import javax.servlet.http.HttpServletRequest
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

/**
 * Created by 赵仁杰 on 2016/3/28.
 */
@Service
class RedisUtil {
    @Resource
    RedisTemplate redisTemplate;

    ValueOperations<String, Object> valueOps;


    private static final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 发布消息
     * @param channel
     * @param message
     */
    void pushMessage(String channel, String message){
        redisTemplate.convertAndSend(channel,message)
    }

    /**
     * 获取redis数据
     * @param key
     * @return
     */
    Object getValue(String key){
        ValueOperations<String, Object> valueOps = redisTemplate.opsForValue();
        Object o = valueOps.get(key);
        return o;
    }
    /**
     * 保存数据到redis
     * @param key
     * @param value
     * @return
     */
    Boolean addValue(String key,Object value){
        ValueOperations<String, Object> valueOps = redisTemplate.opsForValue();
        valueOps.set(key, value,2,TimeUnit.DAYS);
        return true;
    }
    /**
     * 保存数据到redis
     * @param key
     * @param value
     * @return
     */
    Boolean addValue(String key,Object value,long time,TimeUnit unit){
        ValueOperations<String, Object> valueOps = redisTemplate.opsForValue();
        valueOps.set(key, value,time,unit);
        return true;
    }

    /**
     * 删除当前sessiondi所对应的用户
     * @param sessionid
     */
    void remove(String key){
        redisTemplate.delete(key);
    }










    /**
     * 被动退出,有其他用户在登录账号，被挤出登录
     * @param request
     */
    void byLogout(HttpServletRequest request){
        remove(request.getSession().getId()+'dcUser');
        remove('loginUser'+request.getSession().getId());
        request.getSession().removeAttribute("loginUser");
    }



    /**
     *入列
     * @param key
     * @param value
     * @return
     */
    def lPush(String key,def value){
       ListOperations<String,Object> listOps= redisTemplate.opsForList()
        if (value instanceof GString)
            listOps.leftPush(key,value.toString())
        listOps.leftPush(key,value)
    }

    /**
     * set出列
     * @param key
     * @return
     */
    def setPop(String key){
        SetOperations<String,String> setOperations = redisTemplate.opsForSet()
        setOperations.pop(key)
    }
    /**
     *批量入列
     * @param key
     * @param value
     * @return
     */
    def batchLPush(String key,Object... value){
        ListOperations<String,Object> listOps= redisTemplate.opsForList()
        (0..(value.length/10000)).each{
            int end = 0;
            if (((it+1)*10000)>=value.length){
                end = value.length-1
            }else{
                end=(it+1)*10000
            }
            def model = value[it*10000..end]
            listOps.leftPushAll(key,model)
        }
    }

    /***
     * 出列
     * @param key
     * @return
     */
    def rPop(String key){
        ListOperations<String,Object> listOps= redisTemplate.opsForList()
        listOps.rightPop(key)
    }

    /**
     * 模糊前缀删除
     * @param prefix
     * @return
     */
    def removeByPrefix(String prefix) {
        Set<String> keys=redisTemplate.keys(prefix+"*");
        redisTemplate.delete(keys);
    }

    /***
     * 查询key有多少个
     * @param key
     * @return
     */
    Long listSize(String key){
        ListOperations<String,Object> listOps= redisTemplate.opsForList()
        listOps.size(key)
    }

    /**
     * 设置一个竞争锁 使用原子性的  SETNX
     * 如果返回值没有或者为false，就返回true
     * @return
     */
    def getAndsetLock(String key ){
        ValueOperations ops = redisTemplate.opsForValue()
        return ops.setIfAbsent("lock"+key,new Boolean(true))

    }

    /**
     * 清除锁
     * @param key
     */
    void removeLock(String key){
        remove("lock"+key)
    }

    /**
     * 获取list的全部数据
     * @param key
     * @return
     */
    def getList(String key){
        ListOperations<String,Object> listOps= redisTemplate.opsForList()
        listOps.range(key,0 as Long,listSize(key))
    }

    /**
     * 向某个永久  set 中添加值
     * @param setName
     * @param value
     * @return
     */
    def addValueToSet(String setName, Object... value ){
        SetOperations<String,Object> setOperations = redisTemplate.opsForSet();
        setOperations.add(setName,value);
    }

    /**
     * 从set中获取值
     * @param setName
     * @param type
     * @return
     */
    public <T> Set<T> getAllValueFromSet(String setName,Class<T> type){
        SetOperations<String,T> setOperations = redisTemplate.opsForSet();
        setOperations.members(setName)
    }

    def isContainKeyInSet(String setName,Object key){
        SetOperations<String,Object> setOperations = redisTemplate.opsForSet();
        setOperations.isMember(setName,key)
    }

    /**
     * 从set中移除一个值
     * @param setName
     * @param type
     * @return
     */
    public Long removeValueFromSet(String setName, Object... values){
        SetOperations<String,Object> setOperations = redisTemplate.opsForSet();
        Long removedItemNumber = setOperations.remove(setName,values)
        return removedItemNumber
    }

    /**
     * hash 设置 key value ，并将过期时间刷新至最新传的参数
     * @param hashName
     * @param key
     * @param o
     * @param times
     * @param timeUnit
     * @return
     */
    def addHashValue(String hashName, String key, Object o, Long times, TimeUnit timeUnit){
        HashOperations<String,String,Object> hashOperations = redisTemplate.opsForHash();
        hashOperations.put(hashName,key,o)
        redisTemplate.expire(hashName,times,timeUnit)
    }

    /**
     * hash 设置 key value 并持久化：无过期时间。
     * @param hashName
     * @param key
     * @param o
     * @return
     */
    def addHashValueWithoutExpire(String hashName, String key, Object o ){
        HashOperations<String,String,Object> hashOperations = redisTemplate.opsForHash();
        hashOperations.put(hashName,key,o)
        redisTemplate.persist(hashName)
    }

    /**
     * 默认30分钟
     * @param hashName
     * @param key
     * @param o
     * @return
     */
    def addHashValue(String hashName, String key, Object o ){
        addHashValue(hashName,key,o,30,TimeUnit.MINUTES)
    }

    /**
     * 设置新hash key 值，不考虑是否有过期时间
     * @param hashName
     * @param key
     * @param o
     * @return
     */
    def addHashValueNoFreshExpire(String hashName, Object key, Object o ){
        HashOperations<String,Object,Object> hashOperations = redisTemplate.opsForHash();
        hashOperations.put(hashName,key,o)
    }
    /**
     * 设置新hash key 值，不考虑是否有过期时间
     * @param hashName
     * @param map
     * @return
     */
    def addHashValueNoFreshExpire(String hashName,Map<Object,Object> map){
        HashOperations<String,Object,Object> hashOperations = redisTemplate.opsForHash();
        hashOperations.putAll(hashName,map)
    }
    /**
     * 从hash  key 得到value
     * @param hashName
     * @param key
     * @param clazz
     * @return
     */
    public <T> T getHashValue(String hashName,String key,Class<T> clazz){
        HashOperations<String,String,T> hashOperations = redisTemplate.opsForHash();
        hashOperations.get(hashName,key)
    }
    /**
     * 从hash结构中，移除一个key
     * @param hashName
     * @param hKey
     */
    public void removeHashKey(String hashName,String hKey){
        HashOperations<String,String,Object> hashOperations = redisTemplate.opsForHash();
        hashOperations.delete(hashName,hKey)
    }

    /**
     * 判断hash中是不是有这个key
     * @param hashName
     * @param hKey
     * @return
     */
    Boolean hasHashKey(String hashName,String hKey) {
        HashOperations<String, String, Object> hashOperations = redisTemplate.opsForHash();
        hashOperations.hasKey(hashName,hKey)
    }

    /**
     * 设置永久生效
     * @param key
     * @return
     */
    public Boolean persist(Object key){
        redisTemplate.persist(key)
    }
    
    /**
     * 设置过期时间
     * @param key
     * @param timeout
     * @param timeUnit
     * @return
     */
    public Boolean expire(Object key,long timeout, TimeUnit timeUnit){
        redisTemplate.expire(key,timeout,timeUnit)
    }

    public RedisTemplate getRedisTemplate(){
        return this.redisTemplate
    }

    /**
     * 在一段时间之内问尝试设置独占锁，
     * 在seconds内，如果设置锁成功返回true
     * 超时时返回false
     */
    public boolean queryForLock(String lockName,Integer seconds){
        lockName ="lock"+lockName
        boolean exist = redisTemplate.hasKey(lockName)
        if(!exist) return true
        final int INTERVAL = 200   //
        final int MAX = 1000 * seconds
        int usedTime = 0
        while(usedTime <= MAX){
            exist = redisTemplate.hasKey(lockName)
            if(exist){
                usedTime+=INTERVAL
                TimeUnit.MILLISECONDS.sleep(INTERVAL)
            }else{
                return true
            }
        }
        return false
    }

    /**
     * 在一段时间内尝试加锁，运行代码
     * 如果有多个线程尝试同一个锁，各个线程几乎随机获得锁。
     * 如果超时，抛出超时异常
     * @param lockName
     * @param timeOutInSeconds
     * @param closure
     * @return
     */
    def runInLock(String lockName,Integer timeOutInSeconds,Closure<Object> closure){
        final int INTERVAL = 200        //尝试询问锁的时间间隔 ms
        final int MAX = 1000 * timeOutInSeconds
        int usedTime = 0
        boolean isOk
        while(usedTime <= MAX){
            isOk = getAndsetLock(lockName)
            if(!isOk){
                usedTime+=INTERVAL
                TimeUnit.MILLISECONDS.sleep(INTERVAL)
            }else{
                try{
                    return closure.call()
                }finally {
                    removeLock(lockName)
                }
            }
        }
        throw new TimeoutException("faild to get lock for ${lockName} in ${timeOutInSeconds}s.")
    }

    /**
     * 该方法是为解决分布式部署时，由于各个服务器本地时间的误差，多台服务器的同一定时任务可能多次执行的问题。
     * 占用 lockName 的锁，持续timeoutInSeconds，并运行closure.
     * 第一个占用锁的线程执行closure，其他服务器线程在持续时间内拿不到锁，跳过执行。
     * @param lockName  锁名
     * @param timeoutInSeconds   时间需要大于各个服务器最大时钟误差，否则可能不能保证执行一次。时间不能过大，不能超过定时任务间隔。
     * @param closure
     */
    def runOnce(String lockName,Integer timeoutInSeconds,Closure<Object> closure){
        lockName = "onceLock"+lockName
        if(getAndsetLock(lockName)){
            //此处依赖于redis 的过期机制，而非sleep timeoutInSeconds 时间，当前执行线程不宜阻塞
            //但是可以用异步执行的方式避免，该处未做此处理
            redisTemplate.expire("lock"+lockName,timeoutInSeconds,TimeUnit.SECONDS)
            return closure.call()
        }
    }

}
