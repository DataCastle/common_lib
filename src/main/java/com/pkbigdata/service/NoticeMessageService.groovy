package com.pkbigdata.service

/**
 * Created by Administrator on 2017/7/25.
 */
interface NoticeMessageService extends  BaseService{

    /**
     *发送邮件
     * @param toEmails 目的邮箱地址，用英文逗号分隔
     * @param title 邮件主题
     * @param content 邮件内容
     * @param url  邮件服务地址
     * @return
     */
    def sendEmail(String toEmails,String title,String content,String url)



    /**
     * 发送营销短信
     * @param phone 必须有 +86
     * @param content
     * @param url 接口路径
     * @return
     */
    def sendMarketingSms(String phone,String content,String url)

    /**
     * 发送通知短信
     * @param phone 用户电话号码，+86可有可无
     * @param vCode 验证码
     * @param expTime 过期时间，单位是分钟
     * @param url 接口路径
     * @return
     */
    def sendNoticeSms(String phone ,String vCode,String expTime,String url)

}