package com.pkbigdata.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class HttpUtils {

    private static Logger logger= LoggerFactory.getLogger(HttpUtils.class);

    /**
     * 获取链接返回的json数据
     * @param url 地址
     * @param method 方法
     * @param ParamMap 参数
     * @param HederMap header
     * @return
     */
    public static Map getResponse(String url,String method,Map ParamMap,Map HederMap){
        if ("post".equalsIgnoreCase(method)){

            return JsonToMap.jsonToMap(post(url,ParamMap,HederMap));

        }else if ("get".equalsIgnoreCase(method)){

            return JsonToMap.jsonToMap(get(url,ParamMap));
        }else{
            return null;
        }
    }
    /**
     * 获取链接返回的json数据
     * @param url 地址
     * @param method 方法
     * @param ParamMap 参数
     * @return
     */
    public static Map getResponse(String url,String method,Map ParamMap){
        if ("post".equalsIgnoreCase(method)){

            return JsonToMap.jsonToMap(post(url,ParamMap));

        }else if ("get".equalsIgnoreCase(method)){

            return JsonToMap.jsonToMap(get(url,ParamMap));
        }else{
            return null;
        }
    }

    /**
     * get访问接口
     * @param url
     * @param map
     * @return
     */
    public static String get(String url, Map<String,Object> map) {
        if (!map.isEmpty()){
            url += "?";
        }
        for (String string:map.keySet()){
            url += string+"="+map.get(string)+"&";
        }
        String content=get(url);

        return content;
    }




    /**
     * get访问接口
     * @param url
     * @return
     */
    public static String get(String url) {
        CloseableHttpClient httpclient = HttpClients.createSystem();
        try {
            HttpGet httpget = new HttpGet(url);
            httpget.setHeader("Content-Type", "application/json");
            logger.info("executing request " + httpget.getURI());
            // 执行get请求.
            CloseableHttpResponse response = httpclient.execute(httpget);
            logger.info(response.getStatusLine().getStatusCode()+"");
            try {
                if (HttpStatus.SC_OK==response.getStatusLine().getStatusCode() ){
                    // 获取响应实体
                    HttpEntity entity = response.getEntity();
                    logger.info("--------------------------------------");
                    // 打印响应状态
                    logger.info(response.getStatusLine().getStatusCode()+"");
                    if (entity != null) {
                        String content = EntityUtils.toString(entity, "UTF-8");
                        return content;
                    }else {
                        logger.info("return is null");
                    }
                    logger.info("------------------------------------");
                }else {
                    logger.error("未能获取到响应");
                }

            } finally {
                response.close();
            }
        } catch (ClientProtocolException e) {
            logger.error(e.getMessage(), e);
        }  catch (IOException e) {
            logger.error(e.getMessage(), e);
        } finally {
            // 关闭连接,释放资源
            try {
                httpclient.close();
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return null;
    }

    /**
     * post访问接口
     * @param url
     * @param map
     * @return
     */
    public static String post(String url, Map<String,Object> map) {
        // 创建默认的httpClient实例.
        CloseableHttpClient httpclient = HttpClients.createSystem();
        // 创建httppost
        HttpPost httppost = new HttpPost(url);
        // 创建参数队列
        List<NameValuePair> formparams = new ArrayList<NameValuePair>();
        for (String string:map.keySet()){
            formparams.add(new BasicNameValuePair(string, map.get(string).toString()));
        }
        UrlEncodedFormEntity uefEntity;

        try {
            uefEntity = new UrlEncodedFormEntity(formparams, "UTF-8");
            httppost.setEntity(uefEntity);
            httppost.setHeader("X-Auth-Token", "2B9198BAA9EB5932F3EDDB2D50C6E94D");
            httppost.setHeader("Authorization","");
            CloseableHttpResponse response = httpclient.execute(httppost);
            try {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    String content = EntityUtils.toString(entity, "UTF-8");
                    System.out.println(content);
                    return content;
                }
            } finally {
                response.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭连接,释放资源
            try {
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * post方式请求,body区域传json
     * @param url
     * @param JsonBody
     * @param headerMap
     * @return
     */
    public static String post(String url,String JsonBody,Map<String,String> headerMap){
        // 创建默认的httpClient实例.
        CloseableHttpClient httpclient = HttpClients.createSystem();
        // 创建httppost
        HttpPost httppost = new HttpPost(url);
        for (String string:headerMap.keySet()){
            httppost.setHeader(string,headerMap.get(string));
        }
        try {
            httppost.setEntity(new StringEntity(JsonBody,"UTF-8"));
            try (CloseableHttpResponse response = httpclient.execute(httppost)) {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    String content = EntityUtils.toString(entity, "UTF-8");
                    System.out.println(content);
                    return content;
                }
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭连接,释放资源
            try {
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public static String post(String url, Map<String,Object> paramMap,Map<String,String> headerMap) {
        // 创建默认的httpClient实例.
        CloseableHttpClient httpclient = HttpClients.createSystem();
        // 创建httppost
        HttpPost httppost = new HttpPost(url);
        // 创建参数队列
        List<NameValuePair> formparams = new ArrayList<NameValuePair>();
        for (String string:paramMap.keySet()){
            formparams.add(new BasicNameValuePair(string, paramMap.get(string).toString()));
        }
        UrlEncodedFormEntity uefEntity;
        for (String string:headerMap.keySet()){
            httppost.setHeader(string,headerMap.get(string));
        }

        try {
            StringEntity stringEntity = new StringEntity(JsonToMap.beanToJson(paramMap));
            uefEntity = new UrlEncodedFormEntity(formparams, "UTF-8");
            httppost.setEntity(uefEntity);
            httppost.setEntity(stringEntity);
            CloseableHttpResponse response = httpclient.execute(httppost);

            try {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    String content = EntityUtils.toString(entity, "UTF-8");
                    System.out.println(content);
                    return content;
                }
            } finally {
                response.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭连接,释放资源
            try {
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}