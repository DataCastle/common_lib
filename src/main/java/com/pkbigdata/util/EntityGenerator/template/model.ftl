<#-- TODO 改为实际使用的包名 -->
package com.pkbigdata.entity.train ;
import javax.persistence.*;

/**
 * ${comment!""}
 * @desc	使用Entity生成器生成.
 * @date 	${.now?string("yyyy/MM/dd")}
 */
@Entity
@Table(name = "${tableName}" ,catalog = "${schemaName}")
public class ${className} implements java.io.Serializable {
    public ${className}(){}

    private Integer id;
	<#list columns as column>
	<#assign autograph = "private " + column.propertyType + " " + column.propertyName + " ;">
	<#if column.propertyName != "id">
	${autograph}<#if (column.columnComment?? && column.columnComment?length > 0)><#if (autograph?length < 4)>${"\t\t\t\t\t\t\t\t\t\t"}<#elseif (autograph?length < 8)>${"\t\t\t\t\t\t\t\t\t"}<#elseif (autograph?length < 12)>${"\t\t\t\t\t\t\t\t"}<#elseif (autograph?length < 16)>${"\t\t\t\t\t\t\t"}<#elseif (autograph?length < 20)>${"\t\t\t\t\t\t"}<#elseif (autograph?length < 24)>${"\t\t\t\t\t"}<#elseif (autograph?length < 28)>${"\t\t\t\t"}<#elseif (autograph?length < 32)>${"\t\t\t"}<#elseif (autograph?length < 36)>${"\t\t"}<#else>${"\t"}</#if>// ${column.columnComment}</#if>
	</#if>
	</#list>

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	<#list columns as column>
	<#if column.propertyName != "id">
	public void set${column.propertyName?cap_first} (${column.propertyType} ${column.propertyName}){
		this.${column.propertyName} = ${column.propertyName} ;
	}

	@Column(name = "${column.columnName}"<#if column.columnSize != 0> ,length = ${column.columnSize?c}</#if><#if column.isNullable == "NO"> ,nullable = false</#if><#if column.columnKey == "UNI"> ,unique = true</#if>)
	public ${column.propertyType} get${column.propertyName?cap_first} (){
		return this.${column.propertyName} ;
	}

	</#if>
	</#list>
}