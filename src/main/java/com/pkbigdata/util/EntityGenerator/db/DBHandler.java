package com.pkbigdata.util.EntityGenerator.db;

import com.pkbigdata.util.EntityGenerator.consts.Consts;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by wd on 2017/4/12.
 */
public class DBHandler {
    private static Connection conn ;

    public static final Connection createConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver") ;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection("jdbc:mysql://" + Consts.DB_HOST + ":" + Consts.DB_PORT + "/" + Consts.DB_NAME, Consts.DB_USER ,Consts.DB_PASS) ;
           // if(conn!=null) System.out.println("数据库连接成功。。。");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn ;
    }

    public static final void close() {
        if(conn != null) {
            try {
                conn.close() ;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
