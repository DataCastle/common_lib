package com.pkbigdata.util.EntityGenerator.consts;

/**
 * Created by wd on 2017/4/12.
 */
public class Consts {

    public static final String DB_NAME = "bbs" ;		// 数据库名称
    public static final String DB_HOST = "192.168.1.102" ;	// 数据库HOST
    public static final int DB_PORT = 3306 ;					// 数据库端口
    public static final String DB_USER="wsc2014" ;					// 用户名
    public static final String DB_PASS="Wsc2014123456789" ;	// 密码

    public static final String DB_TABLE_PREFIX = "bbs" ;		// 表前缀

    public static final String TARGET_DIR = "D:/Temps/codes2/" ;	// 生成代码存放目录
}
