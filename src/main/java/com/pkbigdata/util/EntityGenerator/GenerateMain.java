package com.pkbigdata.util.EntityGenerator;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import com.pkbigdata.util.EntityGenerator.consts.Consts;
import com.pkbigdata.util.EntityGenerator.db.AnalysisDB;
import com.pkbigdata.util.EntityGenerator.model.TableMeta;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.lang.StringUtils;


/**
 * Created by wd on 2017/4/12.
 * 使用步骤:
 * 1、配置Consts 数据库文件
 * 2、修改模版文件的 package等信息
 * 3、可能需要更改main 函数中的模版文件路径和生成路径
 *
 * 依赖apache common, freemaker.template， mysql-connector
 */

public class GenerateMain {
    public static void main(String[] args) throws IOException, TemplateException {
        GenerateMain.generate();
    }

    public static void generate() throws IOException, TemplateException {

        List<TableMeta> tableList ;
        Writer out = null ;
        String targetDir = Consts.TARGET_DIR ;  //文件生成路径

        tableList = AnalysisDB.readDB() ;
        AnalysisDB.readTables(tableList) ;
        // 输出到文件
        File dir = new File(targetDir) ;
        if(!dir.isDirectory()) {
            dir.mkdirs() ;
        }
        //com.pkbigdata.util.EntityGenerator.model
        Configuration cfg = new Configuration();
        String path=GenerateMain.class.getResource("/").getPath();

        String tPath=path.replaceAll("target/classes/","src/main/java/com/pkbigdata/util/EntityGenerator/template");

        cfg.setDirectoryForTemplateLoading(new File(tPath)) ;
        cfg.setObjectWrapper(new DefaultObjectWrapper());
        Template tpl = cfg.getTemplate("model.ftl") ;
        if(tableList != null) {
            for(TableMeta tm : tableList) {
                if(StringUtils.isBlank(tm.getClassName()))continue ;
                out = new FileWriter(new File(targetDir + tm.getClassName() + ".java")) ;
                tpl.process(tm, out) ;
                System.out.println("===文件 " + tm.getClassName() + ".java" + " 生成成功===");
            }
        }

        out.flush() ;
        out.close() ;

    }
}
