package com.pkbigdata.util.EntityGenerator.model;

import com.pkbigdata.util.EntityGenerator.consts.Consts;
import org.apache.commons.lang.StringUtils;

import java.util.List;



/**
 * Created by wd on 2017/4/12.
 */
public class TableMeta {
    private String schemaName ;
    private String tableName ;
    private String comment ;
    private List<ColumnMeta> columns ;

    public String getTableName() {
        return tableName;
    }
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
    public List<ColumnMeta> getColumns() {
        return columns;
    }
    public void setColumns(List<ColumnMeta> columns) {
        this.columns = columns;
    }
    public String getClassName() {
        if(tableName == null) return "" ;
        StringBuffer className = new StringBuffer() ;
        if(StringUtils.startsWith(tableName, Consts.DB_TABLE_PREFIX)) {
            className.append(Consts.DB_TABLE_PREFIX.substring(0,1).toUpperCase()+Consts.DB_TABLE_PREFIX.substring(1));
            String newName = StringUtils.substring(tableName, Consts.DB_TABLE_PREFIX.length()) ;
            String [] names = StringUtils.split(newName.toLowerCase(), "_") ;
            for(int i = 0 ,len = names.length ; i < len ; i ++) {
                className.append(names[i].substring(0, 1).toUpperCase() + names[i].substring(1)) ;
            }
        } else {
            System.out.println("==不支持的表前缀==");
        }
        return className.toString() ;
    }
    public String getSchemaName() {
        return schemaName;
    }
    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }
}
