package com.pkbigdata.util

import org.springframework.util.StringUtils

/**
 * 用于将 特殊符号隔开的分割成list 或者set<Integer>
 */
class ParamUtils {
    
    static List<Integer> spilt(String content,String reg){
        List<Integer> list = new ArrayList<>()
        if(StringUtils.isEmpty(content)){
            return list
        }
        if(StringUtils.isEmpty(reg)){
            return list
        }
        String[] strings = content.split(reg)
        strings.each {
            if(it!=null && !StringUtils.isEmpty(it.trim())){
                Integer a = Integer.valueOf(it.trim())
                list.add(a)
            }
        }
        return list
    }
    static Set<Integer> spiltToSet(String content,String reg){
        List<Integer> list = spilt(content,reg)
        Set<Integer> set = new LinkedHashSet<>(list)
        return  set
    }

}
