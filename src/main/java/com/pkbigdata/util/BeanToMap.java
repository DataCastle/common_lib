package com.pkbigdata.util;


import com.pkbigdata.entity.DcTeam;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhaorenjie on 2015/4/17.
 * 对list创建索引
 */
public class BeanToMap {

    private static final Logger logger = LoggerFactory.getLogger(BeanToMap.class);
    /**
     * 将list<map>转为map,用其中一个key作为索引
     * @param key
     * @param list
     * @return
     */
    public static Map<String,Map> listToMap(String key ,List<Map> list){
        Map<String,Map> result= new HashMap<String,Map>();
        for (int i = 0; i <list.size() ; i++) {
            result.put(list.get(i).get(key)+"",list.get(i));
        }
        return result;
    }

    /**
     * 用T的某一个属性作为key生成索引
     *
     * @param filed
     * @param list
     * @param <T>
     * @return
     */
    public static <T> Map<Integer, T> toMap(String filed, List<T> list) {

        Map<Integer, T> map = new HashMap<Integer, T>();
        String getMethodName = "get"
                + filed.substring(0, 1).toUpperCase()
                + filed.substring(1);
        Class tCls = null;
        if (list.size() > 0) {
            tCls = list.get(0).getClass();
        } else {
            return map;
        }
        for (T t : list) {
            try {
                Method getMethod = tCls.getMethod(getMethodName,
                        new Class[]{});
                Integer value = (Integer) getMethod.invoke(t, new Object[]{});
                map.put(value, t);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return map;

    }

    /**
     * 将一个 JavaBean 对象转化为一个  Map
     * @param bean 要转化的JavaBean 对象
     * @return 转化出来的  Map 对象
     * @throws IntrospectionException 如果分析类属性失败
     * @throws IllegalAccessException 如果实例化 JavaBean 失败
     * @throws InvocationTargetException 如果调用属性的 setter 方法失败
     */
    public static Map convertBean(Object bean)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException {
        Class type = bean.getClass();
        Map returnMap = new HashMap();
        BeanInfo beanInfo = Introspector.getBeanInfo(type);

        PropertyDescriptor[] propertyDescriptors =  beanInfo.getPropertyDescriptors();
        for (int i = 0; i< propertyDescriptors.length; i++) {
            PropertyDescriptor descriptor = propertyDescriptors[i];
            String propertyName = descriptor.getName();
            if (!propertyName.equals("class")) {
                Method readMethod = descriptor.getReadMethod();
                Object result = readMethod.invoke(bean, new Object[0]);
                if (result != null) {
                    returnMap.put(propertyName, result);
                } else {
                    returnMap.put(propertyName, "");
                }
            }
        }
        return returnMap;
    }

    /**
     * 将实体列表转化为map列表
     * @param beans 实体列表
     * @return
     * @throws IllegalAccessException
     * @throws IntrospectionException
     * @throws InvocationTargetException
     */
    public static List<Map> convertBeans(List<Object> beans) throws IllegalAccessException, IntrospectionException, InvocationTargetException {
        List<Map> maps = new ArrayList<Map>();
        if (beans==null){
            return maps;
        }
        for (int i = 0; i < beans.size(); i++) {
            Map map = convertBean(beans.get(i));
            maps.add(map);
        }
        return maps;
    }
    /**
     * 将实体列表转化为map列表
     * @param beans 实体列表
     * @return
     * @throws IllegalAccessException
     * @throws IntrospectionException
     * @throws InvocationTargetException
     */
    public static List<Map> convertBeansByTeam(List<DcTeam> beans) throws IllegalAccessException, IntrospectionException, InvocationTargetException {
        List<Map> maps = new ArrayList<Map>();
        if (beans==null){
            return maps;
        }
        for (int i = 0; i < beans.size(); i++) {
            Map map = convertBean(beans.get(i));
            maps.add(map);
        }
        return maps;
    }

    /**
     *  sql 表的map 转为 bean   满足： sql 字段用下划线隔开，且都为小写  如：  user_id    create_time    order_id 这种
     * @param sourceMap
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T sqlResultmapToBean(Map<String,Object> sourceMap , Class<T> clazz){
        //转化为 实体驼峰字段
        Map<String,Object> entityFieldMap = new HashMap<>();
        for(Map.Entry<String,Object> entry:sourceMap.entrySet()){
            String sqlKey = entry.getKey();
            String entityKey = convertToEntityField(sqlKey);
            entityFieldMap.put(entityKey,entry.getValue());
        }
        try {
            T result = clazz.newInstance();
            BeanInfo beanInfo = Introspector.getBeanInfo(clazz);
            PropertyDescriptor[] propertyDescriptors =  beanInfo.getPropertyDescriptors();
            for (int i = 0; i< propertyDescriptors.length; i++) {
                PropertyDescriptor descriptor = propertyDescriptors[i];
                String propertyName = descriptor.getName();
                if (!propertyName.equals("class")) {
                    Method setter = descriptor.getWriteMethod();
                    Object value = entityFieldMap.get(propertyName);
                    setter.invoke(result,value);
                }
            }
            return result;
        } catch (Exception e) {
            logger.warn("convertTobean faild,map={},class={}",sourceMap,clazz.getName());
            throw new RuntimeException(e);

        }
    }

    /**
     * 转化sql 下划线   如 time_create  ->  timeCreate
     * @param sqlField
     * @return
     */
    private static String convertToEntityField(String sqlField){
        StringBuilder sb = new StringBuilder();
        String[] strings = sqlField.split("_");
        for(String it:strings){
            if(it.length()>1){
                sb.append(it.substring(0,1).toUpperCase()).append(it.substring(1));
            }else{
                sb.append(it.toUpperCase());
            }
        }
        return sb.substring(0,1).toLowerCase()+sb.substring(1);
    }

}
