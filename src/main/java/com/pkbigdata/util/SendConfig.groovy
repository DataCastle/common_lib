package com.pkbigdata.util;

/**
 * Created by WillChiang on 2015/8/25.
 */
public class SendConfig {

    /**
     * 每次从数据库中查询条数
     */
    public static int querynum = 200;
    public static int start = 0;

    //public static String[] fromEmails = {"datacastle@hirebigdata.cn","wei.jiang@hirebigdata.cn"};

    /**
     *datacastle1@hirebigdata.cn~datacastle8@hirebigdata.cn
     bigdata1@hirebigdata.cn~bigdata8@hirebigdata.cn
     pkbigdata1@hirebigdata.cn~pkbigdata8@hirebigdata.cn.
     */

    /**
     * 发送邮箱池
     */
    public static String[] fromEmails = [
//            "service@service.pkbigdata.com",
//            "datacastle@service.pkbigdata.com",
//            "service@datacastle.pkbigdata.com",
//            "news@service.pkbigdata.com",
            "datacastle@servicepush.datacastle.cn",
            "service@servicepush.datacastle.cn"
            ];

    public static String getDCEmailTemple(String host ,String userName, String content,String email){
        if (userName==null||"".equals(userName)){
            return content;
        }

        String HOST ="http://www.pkbigdata.com";

        String modelMail = """

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title></title>
</head>

<body style="padding:0px;margin: 0px;font-family: '微软雅黑';color:#333;">
<div style="width: 600px;margin:auto;overflow: hidden;background: #FBFBFB;">
<!--头-->
<div style="width: 100%;height: 80px;background: #02A8F4;text-align: center;border-radius: 12px;">
<img src="http://third.datacastle.cn/master.third.source/img/headerlogo.png" style="position:relative;top:16px;" />
</div>

<!--内容-->
<div style="width:100%;">
<br />
<div style="color:#02A8F4;font-size: 16px;position: relative;left: 30px;">Dear <span style="text-decoration: underline;">${userName}</span> :</div>
</div>
        ${content}
<!--尾-->
<footer style="width: 600px;height: 80px;margin: 0px auto;border-radius: 12px;overflow: hidden;">
<a style="width: 600px;height: 80px;display:inline-block;background: url(http://third.datacastle.cn/master.third.source/img/train_emial.png) no-repeat;" href="http://class.pkbigdata.com/" target="_blank"></a>
</footer>
<div class="bottom">
不再接收DataCastle邮件？<a href="http://www.pkbigdata.com/admin/common/unsubscribeEmail.html?email=${email}">取消订阅</a>
 </div>
</div>

</body>

</html>
                            """

        return modelMail;
    }

}
