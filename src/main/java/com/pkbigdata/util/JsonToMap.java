package com.pkbigdata.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.format.DataFormatMatcher;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * Created by zhaorenjie on 2015/4/27.
 */
public class JsonToMap {

    /**
     * json字符串转map
     * @param json json
     * @return
     */
    public static Map jsonToMap(String json){
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> maps = null;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        objectMapper.setDateFormat(df);
        try {
            maps = objectMapper.readValue(json, Map.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return maps;
    }

    /**
     * json字符串转List<map></map>
     * @param json json
     * @return
     */
    public static Map[] jsonToListMap(String json){
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object>[] maps = null;
        try {
            maps = objectMapper.readValue(json, Map[].class);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return maps;
    }

    public static String beanToJson(Object o){
        return JSON.toJSONString(o, SerializerFeature.WriteMapNullValue);
    }

    public static String beanToJsonWithDataFormat(Object o){
        return JSON.toJSONString(o, SerializerFeature.WriteMapNullValue,SerializerFeature.WriteDateUseDateFormat);
    }


}
