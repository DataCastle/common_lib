package com.pkbigdata.util

/**
 * Created by jay on 2017/5/4.
 * 随机码，激活码，状态码，上课码，优惠码生成工具
 */
class CodeGenerator {
    public static final String BaseCode = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM"
    private static  banList = ['0','1','l','I','O','SB','sb','Sb','sB']
    private static  getCourseCodeClosure={
        def valueCode = getRandCode(7)
        def checkCode = getCheckCode(valueCode)
        return valueCode+checkCode
    }
    static def getCouponCodeClosure = {
        def valueCode = getRandCode(6)
        def checkCode = getCheckCode(valueCode)
        return valueCode+checkCode
    }

    /**
     * 官方上课码 ，8位，7位随机码，1位校验码
     * @return
     */
    static String getCourseCode(){
        genFilteredCode (getCourseCodeClosure)
    }
    /**
     * 优惠码 ，7位，6位随机码，1位校验码
     * @return
     */
    static String getCouponCode(){
        genFilteredCode (getCouponCodeClosure)
    }


    /**
     * 验证码是不是符合规则的验证码
     * 默认最后一位是验证位
     * @param code
     * @return
     */
    static Boolean CheckCode(String code){
        if (code[code.length()-1].equals(getCheckCode(code[0..code.length()-2]))){
            return true
        }
        return false
    }

    /**
     * 生成校验位
     * @param valueCode
     * @return
     */
    private static String getCheckCode(String valueCode) {
        def reverseCode = valueCode.reverse()
        long sum = 0L
        valueCode.eachWithIndex { String code, int i ->
            sum += code.toCharacter()*reverseCode.charAt(i)
        }
        return BaseCode[(sum%BaseCode.length()).toInteger()]
    }

    /**
     * 生成随机字符串
     * @param length 字符串长度
     * @return
     */
    private static String getRandCode(int length){
        StringBuffer valueCode = new StringBuffer("")
        (0..length-1).each {
            valueCode.append BaseCode[(int)(Math.random()*BaseCode.length())]
        }
        return valueCode
    }


    def static genFilteredCode(Closure closure){
        while(true){
            def code = closure.call()
            if(filter(code)){
                return code

            }
        }
    }

    private static boolean filter(String code){
        for(String it:banList){
            if(code.contains(it)) return false
        }
        return true
    }

//    public static void main(String[] args) {
//        for(it in 0..10){
//            def code = getCourseCode()
//            println ">>>${code}>>${CheckCode(code)}"
//        }
//    }
}
