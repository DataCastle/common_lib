package com.pkbigdata.util;

/**
 * 邮件主题常量
 * Created by WillChiang on 2015/7/2.
 */
public class EmailSubject {
    public static final String RESET_PWD = "DC账号重置密码";//参赛者忘记密码找回发送的邮件主题
    public static final String UPDATE_EMAIL = "DC新邮箱账号激活";
}
