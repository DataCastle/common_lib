package com.pkbigdata.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 文件格式转换
 * Created by Administrator on 2016/2/17.
 */
public class FileFormatConversion {

    public static int fileCount = 0;
    /*
    public static String sourceFileRoot = "D:\\test"; // 将要转换文件所在的根目录

    public String sourceCharset; // 源文件编码

    public String targetCharset; // 目标文件编码
    */

    public static FileFormatConversion getInstance(){
        return  new FileFormatConversion();
    }

//    public static void main(String[] args){
//        File orgi = new File("C:\\Users\\Administrator\\Desktop\\qqqqq.txt");
//
//       /* File newfile = FileFormatConversion.getInstance().getUtf8_bomToUTF8(orgi);
//        System.out.println(newfile);*/
//
//        /*File file = FileFormatConversion.getInstance().getAnsiToUTF8(orgi);
//        System.out.println(file);*/
//
//        File file = null;
//        try {
//            file = FileFormatConversion.getInstance().utf8ToUtf8(orgi);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        System.out.println(file);
//
//    }

    public File getUtf8_bomToUTF8(File orgi){
        File newfile = null;
        try {
            newfile = FileFormatConversion.getInstance().utf8_bomToUTF8(orgi,"utf8","utf8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newfile;
    }
    public File getAnsiToUTF8(File orgi){
        File newfile = null;
        try {
            newfile = FileFormatConversion.getInstance().convert(orgi);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newfile;
    }

    /**
     * utf8_bomToUTF8
     * @param file
     * @param sourceCharset
     * @param targetCharset
     * @return
     * @throws IOException
     */
    public File utf8_bomToUTF8(File file,String sourceCharset ,String targetCharset) throws IOException {

        List<String> nameList = new ArrayList<String>();

        File targetFile =null;

        // 如果是文件则进行编码转换，写入覆盖原文件

        if (file.isFile()) {

            // 只处理.java结尾的代码文件

            /*if (file.getPath().indexOf(".java") == -1) {



            }*/

            InputStreamReader isr = new InputStreamReader(new FileInputStream(file), sourceCharset);

            BufferedReader br = new BufferedReader(isr);

            StringBuffer sb = new StringBuffer();

            String line = null;

            while ((line = br.readLine()) != null) {
                nameList.add(line);

                // 注意写入换行符
                System.out.println(line);
                sb.append(line + "\n");

            }

            String tmpStr = nameList.get(0);
            //System.out.println(tmpStr + "----len=" + tmpStr.length());
            String tmpStr2 = new String(tmpStr.substring(0, 1));
            //System.out.println(tmpStr2 + "----hex=" + strtoHex(tmpStr2));


            br.close();

            isr.close();

            //文件存入
            targetFile = new File(file.getPath());

            OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(targetFile), targetCharset);

            BufferedWriter bw = new BufferedWriter(osw);

            // 以字符串的形式一次性写入
            if(strtoHex(tmpStr2).equals("0xfeff")){
                bw.write(sb.toString().substring(1));
            }else {
                bw.write(sb.toString());
            }
            bw.close();

            osw.close();

            //System.out.println("Deal:" + targetFile.getPath());

            fileCount++;

        } else {

            for (File subFile : file.listFiles()) {

                utf8_bomToUTF8(subFile, sourceCharset, targetCharset);

            }

        }
    return targetFile;
    }
    public static String strtoHex(String s) {
        String str = "";
        for (int i = 0; i < s.length(); i++) {
            int ch = (int) s.charAt(i);
            String s4 = Integer.toHexString(ch);
            str = str + s4;
        }
        return "0x" + str;// 0x表示十六进制
    }

    /**
     * ansitoutf8
     * @param src
     * @return
     * @throws IOException
     */
    public File convert(File src) throws IOException {
        File dest = new File(src.getPath().split(src.getName())[0] + src.getName().substring(0,src.getName().lastIndexOf("."))+"(附件)."+src.getName().substring(src.getName().lastIndexOf(".")+1));
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(src), "GBK"));
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dest), "utf-8"));
        String line = null;
        while((line = br.readLine()) != null) {
            System.out.println(line);
            writer.write(line+ "\n");
        }
        writer.flush();
        writer.close();
        br.close();

        return dest;
    }

    public static String convert(String text) throws UnsupportedEncodingException {
        return new String(text.getBytes("GBK"));
    }

    /**
     * utf8toutf8
     * @param src
     * @return
     * @throws IOException
     */
    public File utf8ToUtf8(File src) throws IOException {
        File dest = new File(src.getPath().split(src.getName())[0] + src.getName().substring(0,src.getName().lastIndexOf("."))+"(附件)."+src.getName().substring(src.getName().lastIndexOf(".")+1));
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(src), "utf8"));
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dest), "utf8"));
        String line = null;
        while((line = br.readLine()) != null) {
            System.out.println(line);
            writer.write(line+ "\n");
        }
        writer.flush();
        writer.close();
        br.close();

        return dest;
    }


}
