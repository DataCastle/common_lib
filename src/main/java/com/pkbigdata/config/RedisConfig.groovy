package com.pkbigdata.config

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.PropertyAccessor
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Value
import org.springframework.cache.CacheManager
import org.springframework.cache.annotation.CachingConfigurerSupport
import org.springframework.cache.annotation.EnableCaching
import org.springframework.cache.interceptor.CacheResolver
import org.springframework.cache.interceptor.KeyGenerator
import org.springframework.cache.interceptor.SimpleCacheResolver
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.cache.RedisCacheManager
import org.springframework.data.redis.connection.RedisConnectionFactory
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer

/**
 * Created by jay on 2016/5/3.
 */
@Configuration
@EnableCaching
class RedisConfig extends CachingConfigurerSupport{
    @Value('${cachePrefix}')
    String cachePrefix

    /**
     * key的生成策略
     * @return
     */
    @Bean
    @Override
    public KeyGenerator keyGenerator() {
        return [
                generate:{ target, method, params->
                        StringBuilder sb = new StringBuilder();
                        sb.append(target.getClass().getName());
                        sb.append(method.getName());
                        for (Object obj : params) {
                            sb.append(obj.toString());
                        }
                        if(!cachePrefix)
                            cachePrefix=""
                        return cachePrefix+sb.toString();
                }
        ] as KeyGenerator
    }

    /**
     * 缓存默认的过期时间
     * @param redisTemplate
     * @return
     */
    @Bean
    public CacheManager cacheManager(RedisTemplate redisTemplate) {
        RedisCacheManager redisCacheManager = new RedisCacheManager(redisTemplate)
        redisCacheManager.setDefaultExpiration(30*60)
        redisCacheManager.setExpires([
                (cachePrefix+'com.datacastle.groovy.service.impl.CommonServiceImplgetTop3Cmpt'):60*60,
                (cachePrefix+ 'com.datacastle.groovy.service.impl.CommonServiceImplcountTeamAndCommitAndUser'):30*60,
                (cachePrefix+'com.datacastle.groovy.service.impl.CommonServiceImplgetTopBanner'):60*60
        ])
        return new RedisCacheManager(redisTemplate);
    }

    /**
     * 序列化策略
     * @param factory
     * @return
     */
    @Bean
    public StringRedisTemplate redisTemplate(RedisConnectionFactory factory) {
        final StringRedisTemplate template = new StringRedisTemplate(factory);
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        template.setValueSerializer(jackson2JsonRedisSerializer);
        template.setHashValueSerializer(jackson2JsonRedisSerializer);
        template.afterPropertiesSet();
        return template;
    }

    /**
     * 缓存过期时间调整，可以定制时长
     * @param redisTemplate
     * @return
     */
    @Bean("cacheResolverForLong")
    public CacheResolver cacheResolver(RedisTemplate redisTemplate){
        RedisCacheManager redisCacheManager = new RedisCacheManager(redisTemplate)
        redisCacheManager.setDefaultExpiration(23*60*60)
        new SimpleCacheResolver(redisCacheManager)
    }

}
