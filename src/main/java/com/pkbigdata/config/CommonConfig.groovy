package com.pkbigdata.config

import com.google.code.kaptcha.impl.DefaultKaptcha
import com.google.code.kaptcha.util.Config
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.ResourceBundleMessageSource
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean
import org.springframework.web.filter.CharacterEncodingFilter
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor

/**
 * Created by jay on 2017/4/27.
 */
@Configuration
class CommonConfig {

    @Bean
    CharacterEncodingFilter EncodingFilter() {
        CharacterEncodingFilter characterEncodingFilter =new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);
        return characterEncodingFilter;
    }

    /**
     * AbstractApplicationContext.initMessageSource
     * 国际化bean必须定义成MESSAGE_SOURCE_BEAN_NAME
     * @return
     */
    @Bean(name = "messageSource")
    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
        resourceBundleMessageSource.setBasenames("i18n/message");
        resourceBundleMessageSource.setUseCodeAsDefaultMessage(true);
        resourceBundleMessageSource.setDefaultEncoding("UTF-8");
        return resourceBundleMessageSource;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        lci.setIgnoreInvalidLocale(true)
        return lci;
    }

    @Bean(name = "captchaProducer")
    DefaultKaptcha captchaProducer(){
        DefaultKaptcha defaultKaptcha= new DefaultKaptcha()
        Properties properties = new Properties()
        properties.with {
            setProperty("kaptcha.image.width","100")
            setProperty("kaptcha.image.height","40")
            setProperty("kaptcha.textproducer.char.string","0123456789")
            setProperty("kaptcha.textproducer.char.length","4")
            setProperty("kaptcha.border","no")
            setProperty("kaptcha.border.color","105,179,90")
            setProperty("kaptcha.border.thickness","1")
            setProperty("kaptcha.textproducer.font.size","40")
            setProperty("kaptcha.textproducer.font.names","宋体,楷体,微软雅黑")
            setProperty("kaptcha.noise.impl","com.google.code.kaptcha.impl.NoNoise")
            setProperty("kaptcha.textproducer.char.space","3")
            setProperty("kaptcha.background.clear.to","48,173,205")
            setProperty("kaptcha.obscurificator.impl","com.google.code.kaptcha.impl.ShadowGimpy")
        }
        defaultKaptcha.setConfig(new Config(properties))
        return defaultKaptcha
    }


}
