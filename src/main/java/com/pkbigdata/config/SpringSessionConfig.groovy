package com.pkbigdata.config

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.session.data.redis.config.ConfigureRedisAction

/**
 * Created by jay on 2017/2/8 0008.
 */
@Configuration
@Profile("prod")
class SpringSessionConfig {
    private static final Logger logger = LoggerFactory.getLogger(getClass())

    //注意只有在云服务上才需要启用这个配置，用来关闭spring session设置redis config的操作
    @Bean
    public static ConfigureRedisAction configureRedisAction() {
        logger.info("关闭redis config操作")
        return ConfigureRedisAction.NO_OP;
    }
}
