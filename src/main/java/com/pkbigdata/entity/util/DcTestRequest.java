package com.pkbigdata.entity.util;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * DcTestRequest entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "dc_test_request", catalog = "dc_util")

public class DcTestRequest implements java.io.Serializable {

	// Fields

	private Integer id;
	private String projectName;
	private String url;
	private String header;
	private String param;
	private String method;

	// Constructors

	/** default constructor */
	public DcTestRequest() {
	}

	/** minimal constructor */
	public DcTestRequest(String projectName, String url) {
		this.projectName = projectName;
		this.url = url;
	}

	/** full constructor */
	public DcTestRequest(String projectName, String url, String header, String param, String method) {
		this.projectName = projectName;
		this.url = url;
		this.header = header;
		this.param = param;
		this.method = method;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "projectName", nullable = false)

	public String getProjectName() {
		return this.projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	@Column(name = "url", nullable = false, length = 1000)

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "header", length = 2000)

	public String getHeader() {
		return this.header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	@Column(name = "param", length = 1000)

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Column(name = "method")

	public String getMethod() {
		return this.method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

}