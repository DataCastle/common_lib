package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 上课码-用户关联表
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/28
 */
@Entity
@Table(name = "dc_train_code_user_relation" ,catalog = "dc_train")
public class DcTrainCodeUserRelation implements java.io.Serializable {
    public DcTrainCodeUserRelation(){}

    private Integer id;
	private Integer courseCodeId ;			// 上课码id
	private Integer userId ;				// 用户id
	private java.sql.Timestamp createTime ;
	private java.sql.Timestamp useTime ;
	private String useOrderId ;				// 使用上课码的订单号
	private Integer useFlag ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setCourseCodeId (Integer courseCodeId){
		this.courseCodeId = courseCodeId ;
	}

	@Column(name = "course_code_id" ,length = 13 ,nullable = false)
	public Integer getCourseCodeId (){
		return this.courseCodeId ;
	}

	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setUseTime (java.sql.Timestamp useTime){
		this.useTime = useTime ;
	}

	@Column(name = "use_time")
	public java.sql.Timestamp getUseTime (){
		return this.useTime ;
	}

	public void setUseOrderId (String useOrderId){
		this.useOrderId = useOrderId ;
	}

	@Column(name = "use_order_id" ,length = 100)
	public String getUseOrderId (){
		return this.useOrderId ;
	}

	public void setUseFlag (Integer useFlag){
		this.useFlag = useFlag ;
	}

	@Column(name = "use_flag" ,length = 1 ,nullable = false)
	public Integer getUseFlag (){
		return this.useFlag ;
	}

}