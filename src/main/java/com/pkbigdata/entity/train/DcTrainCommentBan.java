package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 用户被禁言记录表
 * @desc	使用Entity生成器生成.
 * @date 	2017/09/06
 */
@Entity
@Table(name = "dc_train_comment_ban" ,catalog = "dc_train")
public class DcTrainCommentBan implements java.io.Serializable {
    public DcTrainCommentBan(){}

    private Integer id;
	private Integer userId ;
	private Integer commentId ;				// 因为哪一条评论被禁
	private java.sql.Timestamp createTime ;
	private Integer courseId ;				// comment对应的课程的id
	private Integer type ;					// 类型  全局或当前
	private Integer length ;				// 禁言天数（天）
	private java.sql.Timestamp endTime ;	// 结束禁言时间
	private String reason ;					// 被禁言原因
	private Integer active ;				// 该条是否有效1禁言有效  0解除禁言

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setCommentId (Integer commentId){
		this.commentId = commentId ;
	}

	@Column(name = "comment_id" ,length = 13 ,nullable = false)
	public Integer getCommentId (){
		return this.commentId ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13)
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setType (Integer type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 1 ,nullable = false)
	public Integer getType (){
		return this.type ;
	}

	public void setLength (Integer length){
		this.length = length ;
	}

	@Column(name = "length" ,length = 7 ,nullable = false)
	public Integer getLength (){
		return this.length ;
	}

	public void setEndTime (java.sql.Timestamp endTime){
		this.endTime = endTime ;
	}

	@Column(name = "end_time" ,nullable = false)
	public java.sql.Timestamp getEndTime (){
		return this.endTime ;
	}

	public void setReason (String reason){
		this.reason = reason ;
	}

	@Column(name = "reason" ,length = 500)
	public String getReason (){
		return this.reason ;
	}

	public void setActive (Integer active){
		this.active = active ;
	}

	@Column(name = "active" ,length = 1 ,nullable = false)
	public Integer getActive (){
		return this.active ;
	}

}