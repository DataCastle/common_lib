package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 用户习题全部答对记录状态
 * @desc	使用Entity生成器生成.
 * @date 	2017/07/21
 */
@Entity
@Table(name = "dc_train_user_exercise_status" ,catalog = "dc_train")
public class DcTrainUserExerciseStatus implements java.io.Serializable {
    public DcTrainUserExerciseStatus(){}

    private Integer id;
	private Integer userId ;
	private Integer classId ;
	private Integer num ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 11 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setClassId (Integer classId){
		this.classId = classId ;
	}

	@Column(name = "class_id" ,length = 11 ,nullable = false)
	public Integer getClassId (){
		return this.classId ;
	}

	public void setNum (Integer num){
		this.num = num ;
	}

	@Column(name = "num" ,length = 11 ,nullable = false)
	public Integer getNum (){
		return this.num ;
	}

}