package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/05/24
 */
@Entity
@Table(name = "dc_train_third_pay_rate" ,catalog = "dc_train")
public class DcTrainThirdPayRate implements java.io.Serializable {
    public DcTrainThirdPayRate(){}

    private Integer id;
	private String name ;					// 支付名称
	private Integer goodsType ;				// 商品类型
	private String goodsTypeInfo ;			// 商品类型
	private Float rate ;					// 税率

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setName (String name){
		this.name = name ;
	}

	@Column(name = "name" ,length = 255 ,nullable = false)
	public String getName (){
		return this.name ;
	}

	public void setGoodsType (Integer goodsType){
		this.goodsType = goodsType ;
	}

	@Column(name = "goods_type" ,length = 1 ,nullable = false)
	public Integer getGoodsType (){
		return this.goodsType ;
	}

	public void setGoodsTypeInfo (String goodsTypeInfo){
		this.goodsTypeInfo = goodsTypeInfo ;
	}

	@Column(name = "goods_type_info" ,length = 255 ,nullable = false)
	public String getGoodsTypeInfo (){
		return this.goodsTypeInfo ;
	}

	public void setRate (Float rate){
		this.rate = rate ;
	}

	@Column(name = "rate" ,nullable = false)
	public Float getRate (){
		return this.rate ;
	}

}