package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 课时表(最小单位,直接对应到某个视频)
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/18
 */
@Entity
@Table(name = "dc_train_class" ,catalog = "dc_train")
public class DcTrainClass implements java.io.Serializable {
    public DcTrainClass(){}

    private Integer id;
	private String name ;					// 课时名称(视频名称)
	private String img ;					// 视频图
	private Integer courseItemId ;			// 关联子课程id(课程目录id)
	private String introduction;//课时简介
	private Integer teacherId ;				// 主讲导师id
	private Integer free;//是否免费
	private Integer createBy ;
	private java.sql.Timestamp createTime ;
	private Integer updateBy ;
	private java.sql.Timestamp updateTime ;
	private Integer sort ;
	private Integer delFlag ;
	private Integer videoLength ;			// 播放时长，单位秒

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setName (String name){
		this.name = name ;
	}

	@Column(name = "name" ,length = 255 ,nullable = false)
	public String getName (){
		return this.name ;
	}

	public void setImg (String img){
		this.img = img ;
	}

	@Column(name = "img" ,length = 500)
	public String getImg (){
		return this.img ;
	}

	public void setCourseItemId (Integer courseItemId){
		this.courseItemId = courseItemId ;
	}

	@Column(name = "course_item_id" ,length = 13 ,nullable = false)
	public Integer getCourseItemId (){
		return this.courseItemId ;
	}

	public void setTeacherId (Integer teacherId){
		this.teacherId = teacherId ;
	}

	@Column(name = "teacher_id" ,length = 13 ,nullable = false)
	public Integer getTeacherId (){
		return this.teacherId ;
	}

	@Column(name = "free",length = 1,nullable = false)
	public Integer getFree() {
		return free;
	}

	public void setFree(Integer free) {
		this.free = free;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setUpdateBy (Integer updateBy){
		this.updateBy = updateBy ;
	}

	@Column(name = "update_by" ,length = 13)
	public Integer getUpdateBy (){
		return this.updateBy ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setSort (Integer sort){
		this.sort = sort ;
	}

	@Column(name = "sort" ,length = 2)
	public Integer getSort (){
		return this.sort ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

	public void setVideoLength (Integer videoLength){
		this.videoLength = videoLength ;
	}

	@Column(name = "video_length" ,length = 5 ,nullable = false)
	public Integer getVideoLength (){
		return this.videoLength ;
	}

	@Column(name = "introduction",length = 30)
	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
}