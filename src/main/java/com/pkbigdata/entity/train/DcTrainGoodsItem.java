package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 商品-课程体系关联表
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/18
 */
@Entity
@Table(name = "dc_train_goods_item" ,catalog = "dc_train")
public class DcTrainGoodsItem implements java.io.Serializable {
    public DcTrainGoodsItem(){}

    private Integer id;
	private String goodsId ;				// 商品ID
	private Integer productId ;				// 产品ID
	private Integer type ;					// 产品类型
	private java.sql.Timestamp createTime ;	// 创建时间

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setGoodsId (String goodsId){
		this.goodsId = goodsId ;
	}

	@Column(name = "goods_id" ,length = 100 ,nullable = false)
	public String getGoodsId (){
		return this.goodsId ;
	}

	public void setProductId (Integer productId){
		this.productId = productId ;
	}

	@Column(name = "product_id" ,length = 13 ,nullable = false)
	public Integer getProductId (){
		return this.productId ;
	}

	public void setType (Integer type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 1)
	public Integer getType (){
		return this.type ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time")
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

}