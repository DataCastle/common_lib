package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 购买课程-能够观看的class的子表
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/19
 */
@Entity
@Table(name = "dc_train_course_purchased_class" ,catalog = "dc_train")
public class DcTrainCoursePurchasedClass implements java.io.Serializable {
    public DcTrainCoursePurchasedClass(){}

    private Integer id;
	private Integer purchasedId ;			// 购买id
	private Integer classId ;				// 课时id

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setPurchasedId (Integer purchasedId){
		this.purchasedId = purchasedId ;
	}

	@Column(name = "purchased_id" ,length = 13 ,nullable = false)
	public Integer getPurchasedId (){
		return this.purchasedId ;
	}

	public void setClassId (Integer classId){
		this.classId = classId ;
	}

	@Column(name = "class_id" ,length = 13 ,nullable = false)
	public Integer getClassId (){
		return this.classId ;
	}

}