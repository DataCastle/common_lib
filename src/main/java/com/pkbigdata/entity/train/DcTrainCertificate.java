package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 认证证书记录
 * @desc	使用Entity生成器生成.
 * @date 	2017/09/19
 */
@Entity
@Table(name = "dc_train_certificate" ,catalog = "dc_train")
public class DcTrainCertificate implements java.io.Serializable {
    public DcTrainCertificate(){}

    private Integer id;
	private String name ;					// 证书名称
	private String certificateId ;			// 证书编号
	private Integer userId ;				// 用户id
	private Integer courseId ;				// 课程id
	private String level ;					// 级别
	private Float score ;					// 分数
	private java.sql.Timestamp firstStudyTime ;	// 第一次学习的时间
	private java.sql.Timestamp createTime ;	// 创建时间
	private Integer isPdf ;					// 是否已经生成了pdf
	private String pdfUrl ;					// pdf 的路径
	private String type ;					// 证书类型
	private String courseName ;				// 课程名称

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setName (String name){
		this.name = name ;
	}

	@Column(name = "name" ,length = 255 ,nullable = false)
	public String getName (){
		return this.name ;
	}

	public void setCertificateId (String certificateId){
		this.certificateId = certificateId ;
	}

	@Column(name = "certificate_id" ,length = 12 ,nullable = false ,unique = true)
	public String getCertificateId (){
		return this.certificateId ;
	}

	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 11 ,nullable = false)
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setLevel (String level){
		this.level = level ;
	}

	@Column(name = "level" ,length = 10)
	public String getLevel (){
		return this.level ;
	}

	public void setScore (Float score){
		this.score = score ;
	}

	@Column(name = "score")
	public Float getScore (){
		return this.score ;
	}

	public void setFirstStudyTime (java.sql.Timestamp firstStudyTime){
		this.firstStudyTime = firstStudyTime ;
	}

	@Column(name = "first_study_time" ,nullable = false)
	public java.sql.Timestamp getFirstStudyTime (){
		return this.firstStudyTime ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time")
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setIsPdf (Integer isPdf){
		this.isPdf = isPdf ;
	}

	@Column(name = "is_pdf" ,length = 1 ,nullable = false)
	public Integer getIsPdf (){
		return this.isPdf ;
	}

	public void setPdfUrl (String pdfUrl){
		this.pdfUrl = pdfUrl ;
	}

	@Column(name = "pdf_url" ,length = 255)
	public String getPdfUrl (){
		return this.pdfUrl ;
	}

	public void setType (String type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 10 ,nullable = false)
	public String getType (){
		return this.type ;
	}

	public void setCourseName (String courseName){
		this.courseName = courseName ;
	}

	@Column(name = "course_name" ,length = 255 ,nullable = false)
	public String getCourseName (){
		return this.courseName ;
	}

}