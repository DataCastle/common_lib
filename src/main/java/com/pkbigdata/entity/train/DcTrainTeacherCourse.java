package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 导师和课程关联表
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/18
 */
@Entity
@Table(name = "dc_train_teacher_course" ,catalog = "dc_train")
public class DcTrainTeacherCourse implements java.io.Serializable {
    public DcTrainTeacherCourse(){}

    private Integer id;
	private Integer teacherId ;				// 导师id
	private Integer courseId ;				// 课程id
	private  Float rate;//提成率
	private Float ratio;//提成比例
	private Integer sort;//导师排列顺序

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setTeacherId (Integer teacherId){
		this.teacherId = teacherId ;
	}

	@Column(name = "teacher_id" ,length = 13 ,nullable = false)
	public Integer getTeacherId (){
		return this.teacherId ;
	}

	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13 ,nullable = false)
	public Integer getCourseId (){
		return this.courseId ;
	}

	@Column(name="rate")
	public Float getRate() {
		return rate;
	}

	public void setRate(Float rate) {
		this.rate = rate;
	}

	@Column(name = "ratio")
	public Float getRatio() {
		return ratio;
	}

	public void setRatio(Float ratio) {
		this.ratio = ratio;
	}

	@Column(name = "sort",length = 1,nullable = false)
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
}