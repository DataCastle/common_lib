package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * PayPal 订单表
 * @desc	使用Entity生成器生成.
 * @date 	2017/08/22
 */
@Entity
@Table(name = "dc_train_order_paypal" ,catalog = "dc_train")
public class DcTrainOrderPaypal implements java.io.Serializable {
    public DcTrainOrderPaypal(){}

    private Integer id;
	private String paypalId ;				// paypal Payment Id
	private String orderId ;				// dc的订单号
	private String firstName ;
	private String lastName ;
	private String userEmail ;
	private String payerId ;				// 用户paypalId
	private String countryCode ;			// 用户paypal国家代号
	private String saleId ;					// sale的id
	private java.math.BigDecimal transactionFee ;	// 扣费 USD
	private java.math.BigDecimal total ;	// 订单总钱 USD
	private java.math.BigDecimal usdRate ;	// 当时美元汇率
	private Integer status ;				// 0未支付 1 已支付

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setPaypalId (String paypalId){
		this.paypalId = paypalId ;
	}

	@Column(name = "paypal_id" ,length = 100 ,nullable = false)
	public String getPaypalId (){
		return this.paypalId ;
	}

	public void setOrderId (String orderId){
		this.orderId = orderId ;
	}

	@Column(name = "order_id" ,length = 100 ,nullable = false ,unique = true)
	public String getOrderId (){
		return this.orderId ;
	}

	public void setFirstName (String firstName){
		this.firstName = firstName ;
	}

	@Column(name = "first_name" ,length = 100)
	public String getFirstName (){
		return this.firstName ;
	}

	public void setLastName (String lastName){
		this.lastName = lastName ;
	}

	@Column(name = "last_name" ,length = 100)
	public String getLastName (){
		return this.lastName ;
	}

	public void setUserEmail (String userEmail){
		this.userEmail = userEmail ;
	}

	@Column(name = "user_email" ,length = 255)
	public String getUserEmail (){
		return this.userEmail ;
	}

	public void setPayerId (String payerId){
		this.payerId = payerId ;
	}

	@Column(name = "payer_id" ,length = 255)
	public String getPayerId (){
		return this.payerId ;
	}

	public void setCountryCode (String countryCode){
		this.countryCode = countryCode ;
	}

	@Column(name = "country_code" ,length = 50)
	public String getCountryCode (){
		return this.countryCode ;
	}

	public void setSaleId (String saleId){
		this.saleId = saleId ;
	}

	@Column(name = "sale_id" ,length = 100)
	public String getSaleId (){
		return this.saleId ;
	}

	public void setTransactionFee (java.math.BigDecimal transactionFee){
		this.transactionFee = transactionFee ;
	}

	@Column(name = "transaction_fee")
	public java.math.BigDecimal getTransactionFee (){
		return this.transactionFee ;
	}

	public void setTotal (java.math.BigDecimal total){
		this.total = total ;
	}

	@Column(name = "total" ,nullable = false)
	public java.math.BigDecimal getTotal (){
		return this.total ;
	}

	public void setUsdRate (java.math.BigDecimal usdRate){
		this.usdRate = usdRate ;
	}

	@Column(name = "usd_rate" ,nullable = false)
	public java.math.BigDecimal getUsdRate (){
		return this.usdRate ;
	}

	public void setStatus (Integer status){
		this.status = status ;
	}

	@Column(name = "status" ,length = 1 ,nullable = false)
	public Integer getStatus (){
		return this.status ;
	}

}