package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 子课程(即为课程的目录)
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/18
 */
@Entity
@Table(name = "dc_train_course_item" ,catalog = "dc_train")
public class DcTrainCourseItem implements java.io.Serializable {
    public DcTrainCourseItem(){}

    private Integer id;
	private String name ;					// 子课程名称
	private String level ;					// 等级(初级,中级,高级)
	private String img ;					// 目录列表图
	private Integer teacherId ;			// 主讲导师id
	private Integer courseNum ;			// 总共多少课时
	private String introduction ;			// 该目录课程简介
	private Float price ;					// 成本价
	private Float originalPrice ;			// 原价
	private java.sql.Timestamp createTime ;
	private Integer createBy ;
	private java.sql.Timestamp updateTime ;
	private Integer updateBy ;
	private Integer delFlag ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setName (String name){
		this.name = name ;
	}

	@Column(name = "name" ,length = 255 ,nullable = false)
	public String getName (){
		return this.name ;
	}

	public void setLevel (String level){
		this.level = level ;
	}

	@Column(name = "level" ,length = 255)
	public String getLevel (){
		return this.level ;
	}

	public void setImg (String img){
		this.img = img ;
	}

	@Column(name = "img" ,length = 500 )
	public String getImg (){
		return this.img ;
	}

	public void setTeacherId (Integer teacherId){
		this.teacherId = teacherId ;
	}

	@Column(name = "teacher_id" ,length = 13 )
	public Integer getTeacherId (){
		return this.teacherId ;
	}

	public void setCourseNum (Integer courseNum){
		this.courseNum = courseNum ;
	}

	@Column(name = "course_num" ,length = 11 )
	public Integer getCourseNum (){
		return this.courseNum ;
	}

	public void setIntroduction (String introduction){
		this.introduction = introduction ;
	}

	@Column(name = "introduction" ,length = 1000 )
	public String getIntroduction (){
		return this.introduction ;
	}

	public void setPrice (Float price){
		this.price = price ;
	}

	@Column(name = "price")
	public Float getPrice (){
		return this.price ;
	}

	public void setOriginalPrice (Float originalPrice){
		this.originalPrice = originalPrice ;
	}

	@Column(name = "original_price")
	public Float getOriginalPrice (){
		return this.originalPrice ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setUpdateBy (Integer updateBy){
		this.updateBy = updateBy ;
	}

	@Column(name = "update_by" ,length = 13)
	public Integer getUpdateBy (){
		return this.updateBy ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

}