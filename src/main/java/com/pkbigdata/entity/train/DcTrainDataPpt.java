package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 课时ppt表格
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/25
 */
@Entity
@Table(name = "dc_train_data_ppt" ,catalog = "dc_train")
public class DcTrainDataPpt implements java.io.Serializable {
    public DcTrainDataPpt(){}

    private Integer id;
	private Integer fileId ;				// 文件下载路径id
	private String img ;					// 封面图片路径
	private String title ;					// 标题
	private Integer courseId ;				// 课程体系id
	private  Integer courseItemId;//子课程id
	private Integer classId;//课时id
	private Integer type;//1.课程2.子课程3.课时
	private Integer delFlag ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setFileId (Integer fileId){
		this.fileId = fileId ;
	}

	@Column(name = "file_id" ,length = 13 ,nullable = false)
	public Integer getFileId (){
		return this.fileId ;
	}

	public void setImg (String img){
		this.img = img ;
	}

	@Column(name = "img" ,length = 255)
	public String getImg (){
		return this.img ;
	}

	public void setTitle (String title){
		this.title = title ;
	}

	@Column(name = "title" ,length = 255)
	public String getTitle (){
		return this.title ;
	}

	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13 )
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

	@Column(name="course_item_id")
	public Integer getCourseItemId() {
		return courseItemId;
	}

	public void setCourseItemId(Integer courseItemId) {
		this.courseItemId = courseItemId;
	}

	@Column(name="class_id")
	public Integer getClassId() {
		return classId;
	}

	public void setClassId(Integer classId) {
		this.classId = classId;
	}

	@Column(name="type")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
}