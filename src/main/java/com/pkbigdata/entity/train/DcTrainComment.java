package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 课程体系评论表
 * @desc	使用Entity生成器生成.
 * @date 	2017/09/06
 */
@Entity
@Table(name = "dc_train_comment" ,catalog = "dc_train")
public class DcTrainComment implements java.io.Serializable {
    public DcTrainComment(){}

    private Integer id;
	private String title ;					// 评论标题
	private String content ;				// 评论内容
	private Integer userId ;				// 评论人
	private Integer courseId ;				// 课程id
	private Integer goodNum ;				// 点赞数
	private String treePath ;				// 评论树，root-id2-id3-id4
	private Integer replytoUserId ;
	private Integer badNum ;				// 差评数
	private Integer replyNum ;				// 回复数-每次更新按照treePath进行维护
	private java.sql.Timestamp createTime ;	// 评论时间
	private Integer topSort ;				// 置顶排序
	private Integer delFlag ;				// 用户的删除标记
	private Integer isRead ;				// 管理员是否已读
	private Integer isReply ;
	private Integer isTop ;					// 是否置顶
	private Integer adminDelFlag ;			// 管理员是否删除

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setTitle (String title){
		this.title = title ;
	}

	@Column(name = "title" ,length = 255)
	public String getTitle (){
		return this.title ;
	}

	public void setContent (String content){
		this.content = content ;
	}

	@Column(name = "content" ,length = 1000 ,nullable = false)
	public String getContent (){
		return this.content ;
	}

	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13 ,nullable = false)
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setGoodNum (Integer goodNum){
		this.goodNum = goodNum ;
	}

	@Column(name = "good_num" ,length = 11)
	public Integer getGoodNum (){
		return this.goodNum ;
	}

	public void setTreePath (String treePath){
		this.treePath = treePath ;
	}

	@Column(name = "tree_path" ,length = 700)
	public String getTreePath (){
		return this.treePath ;
	}

	public void setReplytoUserId (Integer replytoUserId){
		this.replytoUserId = replytoUserId ;
	}

	@Column(name = "replyto_user_id" ,length = 13)
	public Integer getReplytoUserId (){
		return this.replytoUserId ;
	}

	public void setBadNum (Integer badNum){
		this.badNum = badNum ;
	}

	@Column(name = "bad_num" ,length = 11)
	public Integer getBadNum (){
		return this.badNum ;
	}

	public void setReplyNum (Integer replyNum){
		this.replyNum = replyNum ;
	}

	@Column(name = "reply_num" ,length = 11)
	public Integer getReplyNum (){
		return this.replyNum ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setTopSort (Integer topSort){
		this.topSort = topSort ;
	}

	@Column(name = "top_sort" ,length = 2)
	public Integer getTopSort (){
		return this.topSort ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

	public void setIsRead (Integer isRead){
		this.isRead = isRead ;
	}

	@Column(name = "is_read" ,length = 1)
	public Integer getIsRead (){
		return this.isRead ;
	}

	public void setIsReply (Integer isReply){
		this.isReply = isReply ;
	}

	@Column(name = "is_reply" ,length = 1)
	public Integer getIsReply (){
		return this.isReply ;
	}

	public void setIsTop (Integer isTop){
		this.isTop = isTop ;
	}

	@Column(name = "is_top" ,length = 1)
	public Integer getIsTop (){
		return this.isTop ;
	}

	public void setAdminDelFlag (Integer adminDelFlag){
		this.adminDelFlag = adminDelFlag ;
	}

	@Column(name = "admin_del_flag" ,length = 1)
	public Integer getAdminDelFlag (){
		return this.adminDelFlag ;
	}

}