package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 业务流水号表
 * @desc	使用Entity生成器生成.
 * @date 	2017/05/11
 */
@Entity
@Table(name = "dc_train_business_serial" ,catalog = "dc_train")
public class DcTrainBusinessSerial implements java.io.Serializable {
    public DcTrainBusinessSerial(){}

    private Integer id;
	private String businessId ;				// 业务流水号
	private String orderId ;				// 订单号
	private Integer userId ;				// 用户id
	private Integer type ;					// 业务类型
	private String typeInfo ;				// 业务类型说明
	private java.sql.Timestamp createTime ;	// 操作时间
	private Integer delFlag ;				// 是否删除

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setBusinessId (String businessId){
		this.businessId = businessId ;
	}

	@Column(name = "business_id" ,length = 100  ,unique = true)
	public String getBusinessId (){
		return this.businessId ;
	}

	public void setOrderId (String orderId){
		this.orderId = orderId ;
	}

	@Column(name = "order_id" ,length = 100 ,nullable = false)
	public String getOrderId (){
		return this.orderId ;
	}

	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setType (Integer type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 2 ,nullable = false)
	public Integer getType (){
		return this.type ;
	}

	public void setTypeInfo (String typeInfo){
		this.typeInfo = typeInfo ;
	}

	@Column(name = "type_info" ,length = 10 ,nullable = false)
	public String getTypeInfo (){
		return this.typeInfo ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

}