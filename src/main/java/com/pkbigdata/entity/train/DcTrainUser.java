package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 用户表
 * @desc	使用Entity生成器生成.
 * @date 	2017/06/14
 */
@Entity
@Table(name = "dc_train_user" ,catalog = "dc_train")
public class DcTrainUser implements java.io.Serializable {
    public DcTrainUser(){}

    private Integer id;
	private Integer uid ;
	private String nickName ;				// 昵称
	private java.sql.Timestamp createTime ;
	private java.sql.Timestamp updateTime ;
	private Integer updateBy ;
	private String promotionSource ;
	private String remarks ;
	private Integer delFlag ;
	private String img ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUid (Integer uid){
		this.uid = uid ;
	}

	@Column(name = "uid" ,length = 13 ,nullable = false)
	public Integer getUid (){
		return this.uid ;
	}

	public void setNickName (String nickName){
		this.nickName = nickName ;
	}

	@Column(name = "nick_name" ,length = 255 ,unique = true)
	public String getNickName (){
		return this.nickName ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setUpdateBy (Integer updateBy){
		this.updateBy = updateBy ;
	}

	@Column(name = "update_by" ,length = 13)
	public Integer getUpdateBy (){
		return this.updateBy ;
	}

	public void setPromotionSource (String promotionSource){
		this.promotionSource = promotionSource ;
	}

	@Column(name = "promotion_source" ,length = 100)
	public String getPromotionSource (){
		return this.promotionSource ;
	}

	public void setRemarks (String remarks){
		this.remarks = remarks ;
	}

	@Column(name = "remarks" ,length = 255)
	public String getRemarks (){
		return this.remarks ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

	public void setImg (String img){
		this.img = img ;
	}

	@Column(name = "img" ,length = 1000)
	public String getImg (){
		return this.img ;
	}

}