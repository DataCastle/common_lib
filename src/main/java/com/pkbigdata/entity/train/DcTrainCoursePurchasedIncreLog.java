package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 记录用户延长权限时长的表
 * @desc	使用Entity生成器生成.
 * @date 	2017/08/11
 */
@Entity
@Table(name = "dc_train_course_purchased_incre_log" ,catalog = "dc_train")
public class DcTrainCoursePurchasedIncreLog implements java.io.Serializable {
    public DcTrainCoursePurchasedIncreLog(){}

    private Integer id;
	private Integer purchasedId ;			// purchased的id
	private java.sql.Timestamp createTime ;	// 本条记录创建时间
	private java.sql.Timestamp oldEndTime ;	// 之前的权限结束时间
	private Integer score ;					// 使用的积分数
	private Integer day ;					// 兑换的天数

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setPurchasedId (Integer purchasedId){
		this.purchasedId = purchasedId ;
	}

	@Column(name = "purchased_id" ,length = 13 ,nullable = false ,unique = true)
	public Integer getPurchasedId (){
		return this.purchasedId ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setOldEndTime (java.sql.Timestamp oldEndTime){
		this.oldEndTime = oldEndTime ;
	}

	@Column(name = "old_end_time" ,nullable = false)
	public java.sql.Timestamp getOldEndTime (){
		return this.oldEndTime ;
	}

	public void setScore (Integer score){
		this.score = score ;
	}

	@Column(name = "score" ,length = 6 ,nullable = false)
	public Integer getScore (){
		return this.score ;
	}

	public void setDay (Integer day){
		this.day = day ;
	}

	@Column(name = "day" ,length = 6 ,nullable = false)
	public Integer getDay (){
		return this.day ;
	}

}