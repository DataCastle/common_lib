package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 渠道销售列表
 * @desc	使用Entity生成器生成.
 * @date 	2017/07/19
 */
@Entity
@Table(name = "dc_train_channel_sold" ,catalog = "dc_train")
public class DcTrainChannelSold implements java.io.Serializable {
    public DcTrainChannelSold(){}

    private Integer id;
	private Integer channelId ;				// 渠道ID
	private String content ;				// 第三方信息.。例如手机号
	private String goodsId ;				// 码商品ID
	private String thirdId ;				// 第三方ID
	private Integer codeId ;				// 上课码ID
	private String thirdUser ;				// 第三方用户名
	private java.sql.Timestamp time ;		// 添加时间
	private Integer status;//通知状态 1,成功0未通知成功

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setChannelId (Integer channelId){
		this.channelId = channelId ;
	}

	@Column(name = "channel_id" ,length = 16)
	public Integer getChannelId (){
		return this.channelId ;
	}

	public void setContent (String content){
		this.content = content ;
	}

	@Column(name = "content" ,length = 255)
	public String getContent (){
		return this.content ;
	}

	public void setGoodsId (String goodsId){
		this.goodsId = goodsId ;
	}

	@Column(name = "goods_id" ,length = 255)
	public String getGoodsId (){
		return this.goodsId ;
	}

	public void setThirdId (String thirdId){
		this.thirdId = thirdId ;
	}

	@Column(name = "third_id" ,length = 255)
	public String getThirdId (){
		return this.thirdId ;
	}

	public void setCodeId (Integer codeId){
		this.codeId = codeId ;
	}

	@Column(name = "code_id" ,length = 16)
	public Integer getCodeId (){
		return this.codeId ;
	}

	public void setThirdUser (String thirdUser){
		this.thirdUser = thirdUser ;
	}

	@Column(name = "third_user" ,length = 255)
	public String getThirdUser (){
		return this.thirdUser ;
	}

	public void setTime (java.sql.Timestamp time){
		this.time = time ;
	}

	@Column(name = "time")
	public java.sql.Timestamp getTime (){
		return this.time ;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}