package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 订单基本信息表
 * @desc	使用Entity生成器生成.
 * @date 	2017/05/26
 */
@Entity
@Table(name = "dc_train_order" ,catalog = "dc_train")
public class DcTrainOrder implements java.io.Serializable {
    public DcTrainOrder(){}

    private Integer id;
	private String thirdOrderId ;			// 第三方业务流水号
	private String businessId ;				// 业务流水号
	private String tranSerialId ;			// 交易流水号
	private java.sql.Timestamp orderTime ;	// 订单时间
	private String statusInfo ;				// 状态描述
	private Integer status ;				// 订单状态(已支付,未支付,已申请,申请取消,退款中,已退款),退款成功..
	private Float originalPrice ;			// 原价
	private Float orderPrice ;				// 需要付款价
	private Integer buyNumber ;				// 购买数量
	private Integer userId ;
	private String goodsId ;				// 商品编号
	private String payTypeInfo ;			// 支付方式描述()
	private Integer payType ;				// 支付方式(第三方支付,上课码)
	private String payWay ;					// 支付渠道(微信,支付宝,自有)
	private String payAccNum ;				// 付款账号
	private Float payMoney ;				// 支付金额
	private String activityIds ;
	private String couponIds ;
	private Float couponMoney ;				// 优惠券减少的价格
	private Float activityMoney ;			// 活动减少的价格
	private java.sql.Timestamp payTime ;
	private java.sql.Timestamp updateTime ;
	private String remarks ;				// 备注
	private Integer delFlag ;
	private String orderId ;				// 订单号
	private String userName ;				// 用户姓名
	private String userContact ;			// 用户联系方式
	private String userEmail ;				// 用户邮箱
	private Float thirdRate ;				// 第三方费率
	private Integer courseCodeId ;			// 上课码表id
	private Integer channelId ;				// 渠道id

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setThirdOrderId (String thirdOrderId){
		this.thirdOrderId = thirdOrderId ;
	}

	@Column(name = "third_order_id" ,length = 100)
	public String getThirdOrderId (){
		return this.thirdOrderId ;
	}

	public void setBusinessId (String businessId){
		this.businessId = businessId ;
	}

	@Column(name = "business_id" ,length = 100)
	public String getBusinessId (){
		return this.businessId ;
	}

	public void setTranSerialId (String tranSerialId){
		this.tranSerialId = tranSerialId ;
	}

	@Column(name = "tran_serial_id" ,length = 100)
	public String getTranSerialId (){
		return this.tranSerialId ;
	}

	public void setOrderTime (java.sql.Timestamp orderTime){
		this.orderTime = orderTime ;
	}

	@Column(name = "order_time" ,nullable = false)
	public java.sql.Timestamp getOrderTime (){
		return this.orderTime ;
	}

	public void setStatusInfo (String statusInfo){
		this.statusInfo = statusInfo ;
	}

	@Column(name = "status_info" ,length = 255)
	public String getStatusInfo (){
		return this.statusInfo ;
	}

	public void setStatus (Integer status){
		this.status = status ;
	}

	@Column(name = "status" ,length = 1 ,nullable = false)
	public Integer getStatus (){
		return this.status ;
	}

	public void setOriginalPrice (Float originalPrice){
		this.originalPrice = originalPrice ;
	}

	@Column(name = "original_price" ,nullable = false)
	public Float getOriginalPrice (){
		return this.originalPrice ;
	}

	public void setOrderPrice (Float orderPrice){
		this.orderPrice = orderPrice ;
	}

	@Column(name = "order_price" ,nullable = false)
	public Float getOrderPrice (){
		return this.orderPrice ;
	}

	public void setBuyNumber (Integer buyNumber){
		this.buyNumber = buyNumber ;
	}

	@Column(name = "buy_number" ,length = 4 ,nullable = false)
	public Integer getBuyNumber (){
		return this.buyNumber ;
	}

	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setGoodsId (String goodsId){
		this.goodsId = goodsId ;
	}

	@Column(name = "goods_id" ,length = 100 ,nullable = false)
	public String getGoodsId (){
		return this.goodsId ;
	}

	public void setPayTypeInfo (String payTypeInfo){
		this.payTypeInfo = payTypeInfo ;
	}

	@Column(name = "pay_type_info" ,length = 255)
	public String getPayTypeInfo (){
		return this.payTypeInfo ;
	}

	public void setPayType (Integer payType){
		this.payType = payType ;
	}

	@Column(name = "pay_type" ,length = 2)
	public Integer getPayType (){
		return this.payType ;
	}

	public void setPayWay (String payWay){
		this.payWay = payWay ;
	}

	@Column(name = "pay_way" ,length = 255)
	public String getPayWay (){
		return this.payWay ;
	}

	public void setPayAccNum (String payAccNum){
		this.payAccNum = payAccNum ;
	}

	@Column(name = "pay_acc_num" ,length = 255)
	public String getPayAccNum (){
		return this.payAccNum ;
	}

	public void setPayMoney (Float payMoney){
		this.payMoney = payMoney ;
	}

	@Column(name = "pay_money")
	public Float getPayMoney (){
		return this.payMoney ;
	}

	public void setActivityIds (String activityIds){
		this.activityIds = activityIds ;
	}

	@Column(name = "activity_ids" ,length = 200)
	public String getActivityIds (){
		return this.activityIds ;
	}

	public void setCouponIds (String couponIds){
		this.couponIds = couponIds ;
	}

	@Column(name = "coupon_ids" ,length = 200)
	public String getCouponIds (){
		return this.couponIds ;
	}

	public void setCouponMoney (Float couponMoney){
		this.couponMoney = couponMoney ;
	}

	@Column(name = "coupon_money")
	public Float getCouponMoney (){
		return this.couponMoney ;
	}

	public void setActivityMoney (Float activityMoney){
		this.activityMoney = activityMoney ;
	}

	@Column(name = "activity_money")
	public Float getActivityMoney (){
		return this.activityMoney ;
	}

	public void setPayTime (java.sql.Timestamp payTime){
		this.payTime = payTime ;
	}

	@Column(name = "pay_time")
	public java.sql.Timestamp getPayTime (){
		return this.payTime ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setRemarks (String remarks){
		this.remarks = remarks ;
	}

	@Column(name = "remarks" ,length = 255)
	public String getRemarks (){
		return this.remarks ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

	public void setOrderId (String orderId){
		this.orderId = orderId ;
	}

	@Column(name = "order_id" ,length = 100 ,nullable = false ,unique = true)
	public String getOrderId (){
		return this.orderId ;
	}

	public void setUserName (String userName){
		this.userName = userName ;
	}

	@Column(name = "user_name" ,length = 255 ,nullable = false)
	public String getUserName (){
		return this.userName ;
	}

	public void setUserContact (String userContact){
		this.userContact = userContact ;
	}

	@Column(name = "user_contact" ,length = 255 ,nullable = false)
	public String getUserContact (){
		return this.userContact ;
	}

	public void setUserEmail (String userEmail){
		this.userEmail = userEmail ;
	}

	@Column(name = "user_email" ,length = 255 ,nullable = false)
	public String getUserEmail (){
		return this.userEmail ;
	}

	public void setThirdRate (Float thirdRate){
		this.thirdRate = thirdRate ;
	}

	@Column(name = "third_rate")
	public Float getThirdRate (){
		return this.thirdRate ;
	}

	public void setCourseCodeId (Integer courseCodeId){
		this.courseCodeId = courseCodeId ;
	}

	@Column(name = "course_code_id" ,length = 13)
	public Integer getCourseCodeId (){
		return this.courseCodeId ;
	}

	public void setChannelId (Integer channelId){
		this.channelId = channelId ;
	}

	@Column(name = "channel_id" ,length = 13 ,nullable = false)
	public Integer getChannelId (){
		return this.channelId ;
	}

}