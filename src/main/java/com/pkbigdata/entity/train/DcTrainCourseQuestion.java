package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 课程常见问题表
 * @desc	使用Entity生成器生成.
 * @date 	2017/08/31
 */
@Entity
@Table(name = "dc_train_course_question" ,catalog = "dc_train")
public class DcTrainCourseQuestion implements java.io.Serializable {
    public DcTrainCourseQuestion(){}

    private Integer id;
	private String question ;				// 问题
	private String answer ;					// 答案
	private Integer courseId ;				// 课程id
	private java.sql.Timestamp createTime ;
	private Integer createBy ;
	private Integer updateBy ;
	private java.sql.Timestamp updateTime ;
	private Integer order ;					// 排序，越小越前面
	private String remarks ;				// 描述
	private Integer type ;					// 技术型，还是普通型问答
	private Integer delFlag ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setQuestion (String question){
		this.question = question ;
	}

	@Column(name = "question" ,length = 500 ,nullable = false)
	public String getQuestion (){
		return this.question ;
	}

	public void setAnswer (String answer){
		this.answer = answer ;
	}

	@Column(name = "answer" ,length = 500 ,nullable = false)
	public String getAnswer (){
		return this.answer ;
	}

	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13)
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

	public void setUpdateBy (Integer updateBy){
		this.updateBy = updateBy ;
	}

	@Column(name = "update_by" ,length = 13)
	public Integer getUpdateBy (){
		return this.updateBy ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setOrder (Integer order){
		this.order = order ;
	}

	@Column(name = "\"order\"" ,length = 5)
	public Integer getOrder (){
		return this.order ;
	}

	public void setRemarks (String remarks){
		this.remarks = remarks ;
	}

	@Column(name = "remarks" ,length = 255)
	public String getRemarks (){
		return this.remarks ;
	}

	public void setType (Integer type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 1)
	public Integer getType (){
		return this.type ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

}