package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/05/27
 */
@Entity
@Table(name = "dc_train_code_goods_relation" ,catalog = "dc_train")
public class DcTrainCodeGoodsRelation implements java.io.Serializable {
    public DcTrainCodeGoodsRelation(){}

    private Integer id;
	private Integer courseCodeId ;			// 上课码id
	private String goodsId ;				// 码商品id
	private java.sql.Timestamp createTime ;	// 创建时间
	private Integer createBy ;				// 创建者

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setCourseCodeId (Integer courseCodeId){
		this.courseCodeId = courseCodeId ;
	}

	@Column(name = "course_code_id" ,length = 13 ,nullable = false)
	public Integer getCourseCodeId (){
		return this.courseCodeId ;
	}

	public void setGoodsId (String goodsId){
		this.goodsId = goodsId ;
	}

	@Column(name = "goods_id" ,length = 100 ,nullable = false)
	public String getGoodsId (){
		return this.goodsId ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

}