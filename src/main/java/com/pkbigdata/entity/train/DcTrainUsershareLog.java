package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 记录用户分享（转发）的表格
 * @desc	使用Entity生成器生成.
 * @date 	2017/06/16
 */
@Entity
@Table(name = "dc_train_usershare_log" ,catalog = "dc_train")
public class DcTrainUsershareLog implements java.io.Serializable {
    public DcTrainUsershareLog(){}

    private Integer id;
	private Integer classId ;				// 课时id
	private Integer courseId ;				// 所属于的课程id
	private String shareTypeName ;			// 分享类型说明
	private Integer shareType ;				// 分享类型编号
	private String userName ;				// 用户名
	private Integer userId ;				// 用户id
	private String refer ;
	private String ip ;
	private String userAgent ;				// 浏览器UA
	private java.sql.Timestamp createTime ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setClassId (Integer classId){
		this.classId = classId ;
	}

	@Column(name = "class_id" ,length = 13)
	public Integer getClassId (){
		return this.classId ;
	}

	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13 ,nullable = false)
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setShareTypeName (String shareTypeName){
		this.shareTypeName = shareTypeName ;
	}

	@Column(name = "share_type_name" ,length = 20 ,nullable = false)
	public String getShareTypeName (){
		return this.shareTypeName ;
	}

	public void setShareType (Integer shareType){
		this.shareType = shareType ;
	}

	@Column(name = "share_type" ,length = 1 ,nullable = false)
	public Integer getShareType (){
		return this.shareType ;
	}

	public void setUserName (String userName){
		this.userName = userName ;
	}

	@Column(name = "user_name" ,length = 255)
	public String getUserName (){
		return this.userName ;
	}

	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setRefer (String refer){
		this.refer = refer ;
	}

	@Column(name = "refer" ,length = 1000)
	public String getRefer (){
		return this.refer ;
	}

	public void setIp (String ip){
		this.ip = ip ;
	}

	@Column(name = "ip" ,length = 200)
	public String getIp (){
		return this.ip ;
	}

	public void setUserAgent (String userAgent){
		this.userAgent = userAgent ;
	}

	@Column(name = "user_agent" ,length = 255)
	public String getUserAgent (){
		return this.userAgent ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

}