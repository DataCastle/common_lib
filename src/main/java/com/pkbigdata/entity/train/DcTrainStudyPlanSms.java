package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 用户学习计划 发送短信设置，有数据的则会发短信。
 * @desc	使用Entity生成器生成.
 * @date 	2017/07/19
 */
@Entity
@Table(name = "dc_train_study_plan_sms" ,catalog = "dc_train")
public class DcTrainStudyPlanSms implements java.io.Serializable {
    public DcTrainStudyPlanSms(){}

    private Integer id;
	private Integer userId ;				// 用户id
	private Integer courseId ;				// 课程体系id
	private java.sql.Timestamp createTime ;	// 创建时间

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13 ,nullable = false)
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

}