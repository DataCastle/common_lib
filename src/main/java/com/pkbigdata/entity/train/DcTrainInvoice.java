package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/08/25
 */
@Entity
@Table(name = "dc_train_invoice" ,catalog = "dc_train")
public class DcTrainInvoice implements java.io.Serializable {
    public DcTrainInvoice(){}

    private Integer id;
	private String orderId ;				// 订单ID
	private Integer userId ;				// 用户
	private java.sql.Timestamp createTime ;	// 申请发票的时间
	private Integer fileId ;				// 电子发票的文件ID
	private Integer status ;				// 是否已经发送邮箱
	private Integer titleId;				// 发票titleID
	private String  courierNum;            //快递单号(纸质发票可能才有)
	private String shippingAddress;//收货人电话
	private String shippingPhone;//收货人地址
	private Integer type;//发票类型 (1.电子发票 2.纸质发票)
	private Boolean del;					//删除标志

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setOrderId (String orderId){
		this.orderId = orderId ;
	}

	@Column(name = "order_id" ,length = 11)
	public String getOrderId (){
		return this.orderId ;
	}

	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 11)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time")
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setFileId (Integer fileId){
		this.fileId = fileId ;
	}

	@Column(name = "file_id" ,length = 11)
	public Integer getFileId (){
		return this.fileId ;
	}

	public void setStatus (Integer status){
		this.status = status ;
	}

	@Column(name = "status" ,length = 1)
	public Integer getStatus (){
		return this.status ;
	}

	@Column(name = "title_id")
	public Integer getTitleId() {
		return titleId;
	}

	public void setTitleId(Integer titleId) {
		this.titleId = titleId;
	}

	@Column(name = "del")
	public Boolean getDel() {
		return del;
	}

	public void setDel(Boolean del) {
		this.del = del;
	}

	@Column(name = "shipping_phone",length = 255)
	public String getShippingPhone() {
		return shippingPhone;
	}

	public void setShippingPhone(String shippingPhone) {
		this.shippingPhone = shippingPhone;
	}

	@Column(name = "courier_num")
	public String getCourierNum() {
		return courierNum;
	}

	public void setCourierNum(String courierNum) {
		this.courierNum = courierNum;
	}

	@Column(name = "type")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name="shipping_address",length = 500)
	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
}