package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/05/11
 */
@Entity
@Table(name = "dc_train_video" ,catalog = "dc_train")
public class DcTrainVideo implements java.io.Serializable {
    public DcTrainVideo(){}

    private Integer id;
	private String name ;					// 视频名称
	private Integer duration;//播放时长
	private Integer fileId ;				// 视频文件路径id
	private String pixel ;					// 1080P|720P|480P
	private Integer relationId;//关联1080P视频的id
	private Integer classId ;				// 课时id
	private String courseName; //课程体系名称
	private java.sql.Timestamp createTime ;
	private Integer createBy ;
	private Integer updateBy ;
	private java.sql.Timestamp updateTime ;
	private Integer delFlag ;
	private Integer subtitle;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setName (String name){
		this.name = name ;
	}

	@Column(name = "name" ,length = 255 ,nullable = false)
	public String getName (){
		return this.name ;
	}

	@Column(name = "course_name" ,length = 255)
	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public void setFileId (Integer fileId){
		this.fileId = fileId ;
	}

	@Column(name = "file_id" ,length = 13 ,nullable = false)
	public Integer getFileId (){
		return this.fileId ;
	}

	public void setPixel (String pixel){
		this.pixel = pixel ;
	}

	@Column(name = "pixel" ,length = 255 ,nullable = false)
	public String getPixel (){
		return this.pixel ;
	}

	public void setClassId (Integer classId){
		this.classId = classId ;
	}

	@Column(name = "class_id" ,length = 13)
	public Integer getClassId (){
		return this.classId ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

	public void setUpdateBy (Integer updateBy){
		this.updateBy = updateBy ;
	}

	@Column(name = "update_by" ,length = 13)
	public Integer getUpdateBy (){
		return this.updateBy ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

	@Column(name = "duration")
	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	@Column(name = "relation_id")
	public Integer getRelationId() {
		return relationId;
	}

	public void setRelationId(Integer relationId) {
		this.relationId = relationId;
	}

	@Column(name = "subtitle")
	public Integer getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(Integer subtitle) {
		this.subtitle = subtitle;
	}
}