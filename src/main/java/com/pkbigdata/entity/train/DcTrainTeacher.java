package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 讲师(导师)表
 * @desc	使用Entity生成器生成.
 * @date 	2017/08/31
 */
@Entity
@Table(name = "dc_train_teacher" ,catalog = "dc_train")
public class DcTrainTeacher implements java.io.Serializable {
    public DcTrainTeacher(){}

    private Integer id;
	private String avatarImg ;				// 导师头像
	private String bustPhotos ;				// 半身照
	private String name ;					// 导师姓名
	private String teacherCode ;			// 导师编号
	private String title ;					// 导师title
	private String shortIntroduction ;
	private String introduction ;			// 导师简介
	private Integer courseNumber ;			// 课程数量
	private Integer top ;					// 是否置顶首页
	private Integer createBy ;
	private java.sql.Timestamp createTime ;
	private Integer updateBy ;
	private java.sql.Timestamp updateTime ;
	private String remarks ;
	private Integer onlineStatus ;			// 上线1  下线0   上下线状态
	private Integer delFlag ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setAvatarImg (String avatarImg){
		this.avatarImg = avatarImg ;
	}

	@Column(name = "avatar_img" ,length = 255 ,nullable = false)
	public String getAvatarImg (){
		return this.avatarImg ;
	}

	public void setBustPhotos (String bustPhotos){
		this.bustPhotos = bustPhotos ;
	}

	@Column(name = "bust_photos" ,length = 500 ,nullable = false)
	public String getBustPhotos (){
		return this.bustPhotos ;
	}

	public void setName (String name){
		this.name = name ;
	}

	@Column(name = "name" ,length = 255 ,nullable = false)
	public String getName (){
		return this.name ;
	}

	public void setTeacherCode (String teacherCode){
		this.teacherCode = teacherCode ;
	}

	@Column(name = "teacher_code" ,length = 255)
	public String getTeacherCode (){
		return this.teacherCode ;
	}

	public void setTitle (String title){
		this.title = title ;
	}

	@Column(name = "title" ,length = 50 ,nullable = false)
	public String getTitle (){
		return this.title ;
	}

	public void setShortIntroduction (String shortIntroduction){
		this.shortIntroduction = shortIntroduction ;
	}

	@Column(name = "short_introduction" ,length = 128 ,nullable = false)
	public String getShortIntroduction (){
		return this.shortIntroduction ;
	}

	public void setIntroduction (String introduction){
		this.introduction = introduction ;
	}

	@Column(name = "introduction" ,length = 1000 ,nullable = false)
	public String getIntroduction (){
		return this.introduction ;
	}

	public void setCourseNumber (Integer courseNumber){
		this.courseNumber = courseNumber ;
	}

	@Column(name = "course_number" ,length = 11 ,nullable = false)
	public Integer getCourseNumber (){
		return this.courseNumber ;
	}

	public void setTop (Integer top){
		this.top = top ;
	}

	@Column(name = "top" ,length = 1 ,nullable = false)
	public Integer getTop (){
		return this.top ;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setUpdateBy (Integer updateBy){
		this.updateBy = updateBy ;
	}

	@Column(name = "update_by" ,length = 13)
	public Integer getUpdateBy (){
		return this.updateBy ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setRemarks (String remarks){
		this.remarks = remarks ;
	}

	@Column(name = "remarks" ,length = 255)
	public String getRemarks (){
		return this.remarks ;
	}

	public void setOnlineStatus (Integer onlineStatus){
		this.onlineStatus = onlineStatus ;
	}

	@Column(name = "online_status" ,length = 1)
	public Integer getOnlineStatus (){
		return this.onlineStatus ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

}