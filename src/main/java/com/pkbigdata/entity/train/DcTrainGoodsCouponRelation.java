package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/05/27
 */
@Entity
@Table(name = "dc_train_goods_coupon_relation" ,catalog = "dc_train")
public class DcTrainGoodsCouponRelation implements java.io.Serializable {
    public DcTrainGoodsCouponRelation(){}

    private Integer id;
	private String goodsId ;				// 商品id
	private Integer couponId ;				// 优惠券id
	private Integer type ;					// 优惠券作用的种类，和goods_item中的type对应
	private java.sql.Timestamp createTime ;	// 创建时间
	private Integer createBy ;				// 创建人

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setGoodsId (String goodsId){
		this.goodsId = goodsId ;
	}

	@Column(name = "goods_id" ,length = 100 ,nullable = false)
	public String getGoodsId (){
		return this.goodsId ;
	}

	public void setCouponId (Integer couponId){
		this.couponId = couponId ;
	}

	@Column(name = "coupon_id" ,length = 11 ,nullable = false)
	public Integer getCouponId (){
		return this.couponId ;
	}

	public void setType (Integer type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 1 ,nullable = false)
	public Integer getType (){
		return this.type ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

}