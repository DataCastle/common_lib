package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 权限表
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/12
 */
@Entity
@Table(name = "dc_train_permission" ,catalog = "dc_train")
public class DcTrainPermission implements java.io.Serializable {
    public DcTrainPermission(){}

    private int id;
	private String name ;			// 权限名称
	private String url ;				// 权限路径
	private String methodName;//权限对应的方法名
	private String methodPath;//权限对应的包路径

	@Id
    @Column(name = "id")
    @GeneratedValue
    public int getId() {
         return id;
    }

    public void setId(int id) {
        this.id = id;
    }
	public void setName (String name){
		this.name = name ;
	}

	@Column(name = "name" ,length = 255)
	public String getName (){
		return this.name ;
	}

	public void setUrl (String url){
		this.url = url ;
	}

	@Column(name = "url" ,length = 255)
	public String getUrl (){
		return this.url ;
	}

	@Column(name = "method_name" ,length = 255)
	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	@Column(name = "method_path" ,length = 255)
	public String getMethodPath() {
		return methodPath;
	}

	public void setMethodPath(String methodPath) {
		this.methodPath = methodPath;
	}
}