package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 用户学习时段统计
 * @desc	使用Entity生成器生成.
 * @date 	2017/07/18
 */
@Entity
@Table(name = "dc_train_play_record_log" ,catalog = "dc_train")
public class DcTrainPlayRecordLog implements java.io.Serializable {
    public DcTrainPlayRecordLog(){}

    private Integer id;
	private Integer userId ;				// 用户id
	private Integer classId ;				// 课时id
	private java.sql.Timestamp createTime ;	// 观看时间（用于时间段统计）
	private java.sql.Timestamp endTime ;	// 最后一次看视频的时间

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 11 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setClassId (Integer classId){
		this.classId = classId ;
	}

	@Column(name = "class_id" ,length = 11 ,nullable = false)
	public Integer getClassId (){
		return this.classId ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setEndTime (java.sql.Timestamp endTime){
		this.endTime = endTime ;
	}

	@Column(name = "end_time")
	public java.sql.Timestamp getEndTime (){
		return this.endTime ;
	}

}