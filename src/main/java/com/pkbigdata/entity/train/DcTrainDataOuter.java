package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 课时/课程体系 外部资料表
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/25
 */
@Entity
@Table(name = "dc_train_data_outer" ,catalog = "dc_train")
public class DcTrainDataOuter implements java.io.Serializable {
    public DcTrainDataOuter(){}

    private Integer id;
	private String title ;					// 标题
	private String content ;				// 描述内容
	private Integer fileId ;				// 文件id
	private Integer classId ;				// 课时id
	private Integer courseItemId;//子课程id
	private Integer courseId ;				// 课程体系id
	private Integer type ;					// 1.课程体系 2.子课程 3.课时
	private Integer delFlag ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setTitle (String title){
		this.title = title ;
	}

	@Column(name = "title" ,length = 255 ,nullable = false)
	public String getTitle (){
		return this.title ;
	}

	public void setContent (String content){
		this.content = content ;
	}

	@Column(name = "content" ,length = 1000 ,nullable = false)
	public String getContent (){
		return this.content ;
	}

	public void setFileId (Integer fileId){
		this.fileId = fileId ;
	}

	@Column(name = "file_id" ,length = 13 ,nullable = false)
	public Integer getFileId (){
		return this.fileId ;
	}

	public void setClassId (Integer classId){
		this.classId = classId ;
	}

	@Column(name = "class_id" ,length = 13)
	public Integer getClassId (){
		return this.classId ;
	}

	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13)
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setType (Integer type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 1 ,nullable = false)
	public Integer getType (){
		return this.type ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

	@Column(name = "course_item_id")
	public Integer getCourseItemId() {
		return courseItemId;
	}

	public void setCourseItemId(Integer courseItemId) {
		this.courseItemId = courseItemId;
	}
}