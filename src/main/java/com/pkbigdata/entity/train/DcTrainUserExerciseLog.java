package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 用户答题日志表
 * @desc	使用Entity生成器生成.
 * @date 	2017/07/20
 */
@Entity
@Table(name = "dc_train_user_exercise_log" ,catalog = "dc_train")
public class DcTrainUserExerciseLog implements java.io.Serializable {
    public DcTrainUserExerciseLog(){}

    private Integer id;
	private Integer userId ;				// 用户id
	private Integer exerciseId ;			// 题目id
	private String answer ;					// 用户当时提交的答案
	private Integer status ;				// 状态，跳过、答对、答错
	private java.sql.Timestamp createTime ;	// 更新时间

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setExerciseId (Integer exerciseId){
		this.exerciseId = exerciseId ;
	}

	@Column(name = "exercise_id" ,length = 13 ,nullable = false)
	public Integer getExerciseId (){
		return this.exerciseId ;
	}

	public void setAnswer (String answer){
		this.answer = answer ;
	}

	@Column(name = "answer" ,length = 1000)
	public String getAnswer (){
		return this.answer ;
	}

	public void setStatus (Integer status){
		this.status = status ;
	}

	@Column(name = "status" ,length = 1 ,nullable = false)
	public Integer getStatus (){
		return this.status ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

}