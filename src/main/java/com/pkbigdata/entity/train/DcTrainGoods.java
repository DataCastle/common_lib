package com.pkbigdata.entity.train;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * 商品表(所有的销售对象)
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/18
 */
@Entity
@Table(name = "dc_train_goods" ,catalog = "dc_train")
public class DcTrainGoods implements java.io.Serializable {
    public DcTrainGoods(){}

	/**
	 * 目前有两种商品：
	 * 1、每个课程体系对应一个商品
	 * 2、几个课程体系打包成一个商品
	 */
    private String id;
	private String name ;
	private String imgUrl ;					// 商品图片
	private Integer saleVolume ;			// 销售量
	private Integer number ;				// 库存
	private java.sql.Timestamp startTime ;	// 上架时间
	private java.sql.Timestamp endTime ;	// 下架时间
	private Float price ;					// 成本价格-计价单位之和
	private Float originalPrice ;			// 原价
	private String type ;					// 商品类型（单品、套餐）
	private Integer status ;				// 商品状态 （0.未上架1.已上架2.已下架）
	private Integer createBy ;
	private java.sql.Timestamp createTime ;
	private Integer updateBy ;
	private java.sql.Timestamp updateTime ;
	private String remarks ;
	private Integer delFlag ;
	private String info ;					// 商品描述
	private Integer expireDay ;				// 商品使用期限，单位天
	private Integer isAllowActivity ;  	//是否允许活动生效    1为允许
	private Integer isAllowCoupon ;		//是否允许使用优惠券  1 为允许
	private Integer taskStartId;
	private Integer taskEndId;
	private String goodsCode;//商品编号

	@Id
    @Column(name = "id")
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
    public String getId() {
         return id;
    }

    public void setId(String id) {
        this.id = id;
    }
	public void setName (String name){
		this.name = name ;
	}

	@Column(name = "name" ,length = 255 ,nullable = false)
	public String getName (){
		return this.name ;
	}

	public void setNumber (Integer number){
		this.number = number ;
	}

	@Column(name = "number" ,length = 255)
	public Integer getNumber (){
		return this.number ;
	}
	public void setImgUrl (String imgUrl){
		this.imgUrl = imgUrl ;
	}
	public void setSaleVolume (Integer saleVolume){
		this.saleVolume = saleVolume ;
	}

	@Column(name = "sale_volume" ,length = 255)
	public Integer getSaleVolume (){
		return this.saleVolume ;
	}
	@Column(name = "img_url" ,length = 500 )
	public String getImgUrl (){
		return this.imgUrl ;
	}
	public void setStartTime (java.sql.Timestamp startTime){
		this.startTime = startTime ;
	}

	@Column(name = "start_time")
	public java.sql.Timestamp getStartTime (){
		return this.startTime ;
	}

	public void setEndTime (java.sql.Timestamp endTime){
		this.endTime = endTime ;
	}

	@Column(name = "end_time")
	public java.sql.Timestamp getEndTime (){
		return this.endTime ;
	}

	public void setPrice (Float price){
		this.price = price ;
	}

	@Column(name = "price" )
	public Float getPrice (){
		return this.price ;
	}

	public void setOriginalPrice (Float originalPrice){
		this.originalPrice = originalPrice ;
	}

	@Column(name = "original_price" ,nullable = false)
	public Float getOriginalPrice (){
		return this.originalPrice ;
	}

	public void setType (String type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 100 ,nullable = false)
	public String getType (){
		return this.type ;
	}

	public void setStatus (Integer status){
		this.status = status ;
	}

	@Column(name = "status" ,length = 1 ,nullable = false)
	public Integer getStatus (){
		return this.status ;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setUpdateBy (Integer updateBy){
		this.updateBy = updateBy ;
	}

	@Column(name = "update_by" ,length = 13)
	public Integer getUpdateBy (){
		return this.updateBy ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setRemarks (String remarks){
		this.remarks = remarks ;
	}

	@Column(name = "remarks" ,length = 255)
	public String getRemarks (){
		return this.remarks ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

	public void setInfo (String info){
		this.info = info ;
	}

	@Column(name = "info" ,length = 500)
	public String getInfo (){
		return this.info ;
	}

	public void setExpireDay (Integer expireDay){
		this.expireDay = expireDay ;
	}

	@Column(name = "expire_day" ,length = 13,nullable = false)
	public Integer getExpireDay (){
		return this.expireDay ;
	}
	public void setIsAllowActivity (Integer isAllowActivity){
		this.isAllowActivity = isAllowActivity ;
	}

	@Column(name = "is_allow_activity" ,length = 1 ,nullable = false)
	public Integer getIsAllowActivity (){
		return this.isAllowActivity ;
	}

	public void setIsAllowCoupon (Integer isAllowCoupon){
		this.isAllowCoupon = isAllowCoupon ;
	}

	@Column(name = "is_allow_coupon" ,length = 1 ,nullable = false)
	public Integer getIsAllowCoupon (){
		return this.isAllowCoupon ;
	}

	@Column(name = "task_start_id")
	public Integer getTaskStartId() {
		return taskStartId;
	}

	public void setTaskStartId(Integer taskStartId) {
		this.taskStartId = taskStartId;
	}

	@Column(name = "task_end_id")
	public Integer getTaskEndId() {
		return taskEndId;
	}

	public void setTaskEndId(Integer taskEndId) {
		this.taskEndId = taskEndId;
	}

	@Column(name = "goods_code")
	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}
}