package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 文件表
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/26
 */
@Entity
@Table(name = "dc_train_file" ,catalog = "dc_train")
public class DcTrainFile implements java.io.Serializable {
    public DcTrainFile(){}

    private Integer id;
	private String name ;					// 文件名称
	private Integer type ;					// 文件类型
	private String path ;					// 文件路径
	private String info ;					// 文件信息
	private java.sql.Timestamp createTime ;	// 创建时间
	private Integer createBy ;				// 创建人

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setName (String name){
		this.name = name ;
	}

	@Column(name = "name" ,length = 255)
	public String getName (){
		return this.name ;
	}

	public void setType (Integer type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 1 ,nullable = false)
	public Integer getType (){
		return this.type ;
	}

	public void setPath (String path){
		this.path = path ;
	}

	@Column(name = "path" ,length = 255 ,nullable = false)
	public String getPath (){
		return this.path ;
	}

	public void setInfo (String info){
		this.info = info ;
	}

	@Column(name = "info" ,length = 255)
	public String getInfo (){
		return this.info ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

}