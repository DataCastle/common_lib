package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 商品折价优惠券
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/28
 */
@Entity
@Table(name = "dc_train_goods_coupon" ,catalog = "dc_train")
public class DcTrainGoodsCoupon implements java.io.Serializable {
    public DcTrainGoodsCoupon(){}

	/**
	 * type 用来计算优惠券优惠信息
	 * usePolicy用来规定优惠券的使用方式（是否是新手券、是否是多人使用、是否是单人使用）
	 */
    private Integer id;
	private String code ;					// 优惠码
	private Integer channelId;//销售渠道id
	private String comingWay ;				// 销售渠道
	private Float discount ;
	private Float money ;
	private java.sql.Timestamp createTime ;	// 优惠码类型产生时间
	private java.sql.Timestamp effectTime ;	// 优惠码生效时间
	private java.sql.Timestamp expireTime ;	// 优惠码失效时间
	private String usePolicy ;				// 使用策略-表示是否可以多人使用，是否是新手券，是否是码兑换券  枚举： T_GoodsCouponUsePolicy
	private String type ;					// 卷类型：折扣卷，现金券

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setCode (String code){
		this.code = code ;
	}

	@Column(name = "code" ,length = 255)
	public String getCode (){
		return this.code ;
	}

	public void setComingWay (String comingWay){
		this.comingWay = comingWay ;
	}

	@Column(name = "coming_way" ,length = 255)
	public String getComingWay (){
		return this.comingWay ;
	}

	public void setDiscount (Float discount){
		this.discount = discount ;
	}

	@Column(name = "discount")
	public Float getDiscount (){
		return this.discount ;
	}

	public void setMoney (Float money){
		this.money = money ;
	}

	@Column(name = "money")
	public Float getMoney (){
		return this.money ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setEffectTime (java.sql.Timestamp effectTime){
		this.effectTime = effectTime ;
	}

	@Column(name = "effect_time" ,nullable = false)
	public java.sql.Timestamp getEffectTime (){
		return this.effectTime ;
	}

	public void setExpireTime (java.sql.Timestamp expireTime){
		this.expireTime = expireTime ;
	}

	@Column(name = "expire_time" ,nullable = false)
	public java.sql.Timestamp getExpireTime (){
		return this.expireTime ;
	}

	public void setUsePolicy (String usePolicy){
		this.usePolicy = usePolicy ;
	}

	@Column(name = "use_policy" ,length = 255 ,nullable = false)
	public String getUsePolicy (){
		return this.usePolicy ;
	}

	public void setType (String type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 255 ,nullable = false)
	public String getType (){
		return this.type ;
	}

	@Column(name="channel_id")
	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
}