package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 用户答题状态表（最新状态）
 * @desc	使用Entity生成器生成.
 * @date 	2017/10/11
 */
@Entity
@Table(name = "dc_train_user_exercise" ,catalog = "dc_train")
public class DcTrainUserExercise implements java.io.Serializable {
    public DcTrainUserExercise(){}

    private Integer id;
	private Integer userId ;				// 用户id
	private Integer exerciseId ;			// 题目id
	private Integer status ;				// 状态，跳过、答对、答错
	private Integer latestLogId ;			// 最近一次答题记录的id
	private java.sql.Timestamp updateTime ;	// 更新时间

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setExerciseId (Integer exerciseId){
		this.exerciseId = exerciseId ;
	}

	@Column(name = "exercise_id" ,length = 13 ,nullable = false)
	public Integer getExerciseId (){
		return this.exerciseId ;
	}

	public void setStatus (Integer status){
		this.status = status ;
	}

	@Column(name = "status" ,length = 1 ,nullable = false)
	public Integer getStatus (){
		return this.status ;
	}

	public void setLatestLogId (Integer latestLogId){
		this.latestLogId = latestLogId ;
	}

	@Column(name = "latest_log_id" ,length = 13)
	public Integer getLatestLogId (){
		return this.latestLogId ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time" ,nullable = false)
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

}