package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 上课码表
 * @desc	使用Entity生成器生成.
 * @date 	2017/05/23
 */
@Entity
@Table(name = "dc_train_course_code" ,catalog = "dc_train")
public class DcTrainCourseCode implements java.io.Serializable {
    public DcTrainCourseCode(){}

    private Integer id;
	private String code ;					// 上课码
	private Integer channelId ;				// 渠道id
	private Integer time ;					// 从使用开始，免费多少天。try码需要填写
	private Float price ;					// 售价
	private Integer createBy ;
	private java.sql.Timestamp createTime ;
	private Integer updateBy ;
	private java.sql.Timestamp updateTime ;
	private java.sql.Timestamp startTime ;	// 可使用的开始时间
	private java.sql.Timestamp expireTime ;	// 到期时间
	private Integer classNumber ;			// 0 为课程体系所有的课时， 5 为课程体系的前5个课时
	private String type ;					// 种类，商品上课码，通用上课码等

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

	public void setCode (String code){
		this.code = code ;
	}

	@Column(name = "code" ,length = 255 ,nullable = false)
	public String getCode (){
		return this.code ;
	}

	public void setChannelId (Integer channelId){
		this.channelId = channelId ;
	}

	@Column(name = "channel_id" ,length = 11)
	public Integer getChannelId (){
		return this.channelId ;
	}

	public void setTime (Integer time){
		this.time = time ;
	}

	@Column(name = "time" ,length = 11)
	public Integer getTime (){
		return this.time ;
	}

	public void setPrice (Float price){
		this.price = price ;
	}

	@Column(name = "price")
	public Float getPrice (){
		return this.price ;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setUpdateBy (Integer updateBy){
		this.updateBy = updateBy ;
	}

	@Column(name = "update_by" ,length = 13)
	public Integer getUpdateBy (){
		return this.updateBy ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setStartTime (java.sql.Timestamp startTime){
		this.startTime = startTime ;
	}

	@Column(name = "start_time")
	public java.sql.Timestamp getStartTime (){
		return this.startTime ;
	}

	public void setExpireTime (java.sql.Timestamp expireTime){
		this.expireTime = expireTime ;
	}

	@Column(name = "expire_time" ,nullable = false)
	public java.sql.Timestamp getExpireTime (){
		return this.expireTime ;
	}

	public void setClassNumber (Integer classNumber){
		this.classNumber = classNumber ;
	}

	@Column(name = "class_number" ,length = 2 ,nullable = false)
	public Integer getClassNumber (){
		return this.classNumber ;
	}

	public void setType (String type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 100 ,nullable = false)
	public String getType (){
		return this.type ;
	}

}