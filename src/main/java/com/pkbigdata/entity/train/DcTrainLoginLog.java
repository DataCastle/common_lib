package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 登录记录表
 * @desc	使用Entity生成器生成.
 * @date 	2017/07/13
 */
@Entity
@Table(name = "dc_train_login_log" ,catalog = "dc_train")
public class DcTrainLoginLog implements java.io.Serializable {
    public DcTrainLoginLog(){}

    private Integer id;
	private String userName ;
	private Integer userId ;
	private String ip ;
	private java.sql.Timestamp loginTime ;
	private String browser ;
	private String area ;
	private String portType ;
	private java.sql.Timestamp loginOutTime ;	// 退出时间
	private Integer onlineTime ;			// 登录时长

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserName (String userName){
		this.userName = userName ;
	}

	@Column(name = "user_name" ,length = 50)
	public String getUserName (){
		return this.userName ;
	}

	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 11)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setIp (String ip){
		this.ip = ip ;
	}

	@Column(name = "ip" ,length = 500)
	public String getIp (){
		return this.ip ;
	}

	public void setLoginTime (java.sql.Timestamp loginTime){
		this.loginTime = loginTime ;
	}

	@Column(name = "login_time")
	public java.sql.Timestamp getLoginTime (){
		return this.loginTime ;
	}

	public void setBrowser (String browser){
		this.browser = browser ;
	}

	@Column(name = "browser" ,length = 500)
	public String getBrowser (){
		return this.browser ;
	}

	public void setArea (String area){
		this.area = area ;
	}

	@Column(name = "area" ,length = 200)
	public String getArea (){
		return this.area ;
	}

	public void setPortType (String portType){
		this.portType = portType ;
	}

	@Column(name = "port_type" ,length = 50)
	public String getPortType (){
		return this.portType ;
	}

	public void setLoginOutTime (java.sql.Timestamp loginOutTime){
		this.loginOutTime = loginOutTime ;
	}

	@Column(name = "login_out_time")
	public java.sql.Timestamp getLoginOutTime (){
		return this.loginOutTime ;
	}

	public void setOnlineTime (Integer onlineTime){
		this.onlineTime = onlineTime ;
	}

	@Column(name = "online_time" ,length = 13)
	public Integer getOnlineTime (){
		return this.onlineTime ;
	}

}