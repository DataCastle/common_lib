package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/26
 */
@Entity
@Table(name = "dc_train_cmpt_course_item" ,catalog = "dc_train")
public class DcTrainCmptCourseItem implements java.io.Serializable {
    public DcTrainCmptCourseItem(){}

    private Integer id;
	private Integer courseItemId ;			// 子课程id
	private Integer trainCmptId ;			// 训练赛id

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setCourseItemId (Integer courseItemId){
		this.courseItemId = courseItemId ;
	}

	@Column(name = "course_item_id" ,length = 13 ,nullable = false)
	public Integer getCourseItemId (){
		return this.courseItemId ;
	}

	public void setTrainCmptId (Integer trainCmptId){
		this.trainCmptId = trainCmptId ;
	}

	@Column(name = "train_cmpt_id" ,length = 13 ,nullable = false)
	public Integer getTrainCmptId (){
		return this.trainCmptId ;
	}

}