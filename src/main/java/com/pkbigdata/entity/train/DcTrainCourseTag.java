package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 子课程标签表
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/18
 */
@Entity
@Table(name = "dc_train_course_tag" ,catalog = "dc_train")
public class DcTrainCourseTag implements java.io.Serializable {
    public DcTrainCourseTag(){}

    private Integer id;
	private String name ;					// 标签名
	private Integer courseItemId ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setName (String name){
		this.name = name ;
	}

	@Column(name = "name" ,length = 255 ,nullable = false)
	public String getName (){
		return this.name ;
	}

	public void setCourseItemId (Integer courseItemId){
		this.courseItemId = courseItemId ;
	}

	@Column(name = "course_item_id" ,length = 13 ,nullable = false)
	public Integer getCourseItemId (){
		return this.courseItemId ;
	}

}