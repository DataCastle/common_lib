package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 课程和课时收藏表
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/18
 */
@Entity
@Table(name = "dc_train_collect" ,catalog = "dc_train")
public class DcTrainCollect implements java.io.Serializable {
    public DcTrainCollect(){}

    private Integer id;
	private Integer courseId ;				// 课程id
	private Integer classId ;				// 课时id
	private Integer userId ;				// 收藏人id
	private String type ;					// 收藏类型    课程体系course  课时class
	private Integer createBy ;
	private java.sql.Timestamp createTime ;
	private Integer updateBy ;
	private java.sql.Timestamp updateTime ;
	private String remarks ;				// 收藏备注
	private Integer delFlag ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13)
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setClassId (Integer classId){
		this.classId = classId ;
	}

	@Column(name = "class_id" ,length = 13)
	public Integer getClassId (){
		return this.classId ;
	}

	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setType (String type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 255 ,nullable = false)
	public String getType (){
		return this.type ;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setUpdateBy (Integer updateBy){
		this.updateBy = updateBy ;
	}

	@Column(name = "update_by" ,length = 13)
	public Integer getUpdateBy (){
		return this.updateBy ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setRemarks (String remarks){
		this.remarks = remarks ;
	}

	@Column(name = "remarks" ,length = 255)
	public String getRemarks (){
		return this.remarks ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

}