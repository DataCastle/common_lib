package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 课程重要标记表
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/24
 */
@Entity
@Table(name = "dc_train_class_important" ,catalog = "dc_train")
public class DcTrainClassImportant implements java.io.Serializable {
    public DcTrainClassImportant(){}

    private Integer id;
	private Integer userId ;				// 用户id
	private Integer courseId ;				// 课程体系id
	private Integer classId ;				// 课时id
	private Integer type;//类型（1.课时，2.课程体系）
	private Integer status ;				// 0.不重要   1.重要
	private java.sql.Timestamp createTime ;	// 标记时间
	private java.sql.Timestamp updateTime ;	// 取消标记时间

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13)
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setClassId (Integer classId){
		this.classId = classId ;
	}

	@Column(name = "class_id" ,length = 13 )
	public Integer getClassId (){
		return this.classId ;
	}

	public void setStatus (Integer status){
		this.status = status ;
	}

	@Column(name = "type" ,length = 1 ,nullable = false)
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "status" ,length = 1 ,nullable = false)
	public Integer getStatus (){
		return this.status ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

}