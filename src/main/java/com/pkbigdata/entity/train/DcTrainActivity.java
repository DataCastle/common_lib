package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 商品限时打折活动
 * @desc	使用Entity生成器生成.
 * @date 	2017/05/02
 */
@Entity
@Table(name = "dc_train_activity" ,catalog = "dc_train")
public class DcTrainActivity implements java.io.Serializable {
    public DcTrainActivity(){}

    private Integer id;
	private String goodsId ;				// 商品id
	private Float discount ;				// 优惠率
	private Float money ;					// 优惠金额(RMB:元) 列1.如果原价900 优惠后的价格800 优惠金额为100
	private Integer type;                  //活动类型(0.优惠 1.折扣 2.免费)
	private java.sql.Timestamp startTime ;	// 开始时间
	private java.sql.Timestamp endTime ;	// 结束时间
	private Integer status ;				// 是否上线,1 上线  0 未上线
	private Integer createBy ;
	private java.sql.Timestamp createTime ;
	private Integer updateBy ;
	private java.sql.Timestamp updateTime ;
	private Integer isAllowCoupon ;			// 是否允许使用优惠券
	private Integer delFlag ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setGoodsId (String goodsId){
		this.goodsId = goodsId ;
	}

	@Column(name = "goods_id" ,length = 100 ,nullable = false)
	public String getGoodsId (){
		return this.goodsId ;
	}

	public void setDiscount (Float discount){
		this.discount = discount ;
	}

	@Column(name = "discount")
	public Float getDiscount (){
		return this.discount ;
	}

	public void setMoney (Float money){
		this.money = money ;
	}

	@Column(name = "money" )
	public Float getMoney (){
		return this.money ;
	}

	@Column(name="type",length = 1,nullable = false)
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public void setStartTime (java.sql.Timestamp startTime){
		this.startTime = startTime ;
	}

	@Column(name = "start_time" ,nullable = false)
	public java.sql.Timestamp getStartTime (){
		return this.startTime ;
	}

	public void setEndTime (java.sql.Timestamp endTime){
		this.endTime = endTime ;
	}

	@Column(name = "end_time" )
	public java.sql.Timestamp getEndTime (){
		return this.endTime ;
	}

	public void setStatus (Integer status){
		this.status = status ;
	}

	@Column(name = "status" ,length = 1 ,nullable = false)
	public Integer getStatus (){
		return this.status ;
	}


	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setUpdateBy (Integer updateBy){
		this.updateBy = updateBy ;
	}

	@Column(name = "update_by" ,length = 13)
	public Integer getUpdateBy (){
		return this.updateBy ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setIsAllowCoupon (Integer isAllowCoupon){
		this.isAllowCoupon = isAllowCoupon ;
	}

	@Column(name = "is_allow_coupon" ,length = 1 ,nullable = false)
	public Integer getIsAllowCoupon (){
		return this.isAllowCoupon ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

}