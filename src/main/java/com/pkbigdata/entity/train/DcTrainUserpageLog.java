package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 统计IP PV表
 * @desc	使用Entity生成器生成.
 * @date 	2017/06/14
 */
@Entity
@Table(name = "dc_train_userpage_log" ,catalog = "dc_train")
public class DcTrainUserpageLog implements java.io.Serializable {
    public DcTrainUserpageLog(){}

    private Integer id;
	private String ip ;
	private Integer userId ;
	private String username ;
	private String url ;					// 路径
	private String content ;				// 操作内容
	private String userAgent ;
	private String browser ;				// 浏览器
	private String source ;					// 渠道
	private String sourceName ;
	private String refer ;
	private String sessionId ;
	private java.sql.Timestamp createTime ;
	private String uvCookie ;				// 用户cookie

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setIp (String ip){
		this.ip = ip ;
	}

	@Column(name = "ip" ,length = 255 ,nullable = false)
	public String getIp (){
		return this.ip ;
	}

	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 11)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setUsername (String username){
		this.username = username ;
	}

	@Column(name = "username" ,length = 50)
	public String getUsername (){
		return this.username ;
	}

	public void setUrl (String url){
		this.url = url ;
	}

	@Column(name = "url" ,length = 10000 ,nullable = false)
	public String getUrl (){
		return this.url ;
	}

	public void setContent (String content){
		this.content = content ;
	}

	@Column(name = "content" ,length = 255)
	public String getContent (){
		return this.content ;
	}

	public void setUserAgent (String userAgent){
		this.userAgent = userAgent ;
	}

	@Column(name = "user_agent" ,length = 1000)
	public String getUserAgent (){
		return this.userAgent ;
	}

	public void setBrowser (String browser){
		this.browser = browser ;
	}

	@Column(name = "browser" ,length = 255)
	public String getBrowser (){
		return this.browser ;
	}

	public void setSource (String source){
		this.source = source ;
	}

	@Column(name = "source" ,length = 255)
	public String getSource (){
		return this.source ;
	}

	public void setSourceName (String sourceName){
		this.sourceName = sourceName ;
	}

	@Column(name = "source_name" ,length = 255)
	public String getSourceName (){
		return this.sourceName ;
	}

	public void setRefer (String refer){
		this.refer = refer ;
	}

	@Column(name = "refer" ,length = 1000)
	public String getRefer (){
		return this.refer ;
	}

	public void setSessionId (String sessionId){
		this.sessionId = sessionId ;
	}

	@Column(name = "session_id" ,length = 255 ,nullable = false)
	public String getSessionId (){
		return this.sessionId ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setUvCookie (String uvCookie){
		this.uvCookie = uvCookie ;
	}

	@Column(name = "uv_cookie" ,length = 50)
	public String getUvCookie (){
		return this.uvCookie ;
	}

}