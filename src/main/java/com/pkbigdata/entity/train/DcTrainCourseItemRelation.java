package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 课程体系子课程关联表
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/18
 */
@Entity
@Table(name = "dc_train_course_item_relation" ,catalog = "dc_train")
public class DcTrainCourseItemRelation implements java.io.Serializable {
    public DcTrainCourseItemRelation(){}

    private Integer id;
	private Integer courseId ;				// 课程体系id
	private Integer courseItemId ;			// 子课程ID
	private Integer order ;					// 课程目录排序
	private java.sql.Timestamp createTime ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13 ,nullable = false)
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setCourseItemId (Integer courseItemId){
		this.courseItemId = courseItemId ;
	}

	@Column(name = "course_item_id" ,length = 13 ,nullable = false)
	public Integer getCourseItemId (){
		return this.courseItemId ;
	}

	public void setOrder (Integer order){
		this.order = order ;
	}

	@Column(name = "\"order\"" ,length = 5)
	public Integer getOrder (){
		return this.order ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time")
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

}