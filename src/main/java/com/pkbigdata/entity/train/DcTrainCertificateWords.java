package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 用户课程证书字段排版
 * @desc	使用Entity生成器生成.
 * @date 	2017/09/20
 */
@Entity
@Table(name = "dc_train_certificate_words" ,catalog = "dc_train")
public class DcTrainCertificateWords implements java.io.Serializable {
    public DcTrainCertificateWords(){}

    private Integer id;
	private Integer courseId ;
	private String courseName ;
	private String eng1 ;
	private String eng2 ;
	private String eng3 ;
	private String eng4 ;
	private String eng5 ;
	private java.sql.Timestamp createTime ;
	private String type ;					// 类型

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13 ,nullable = false)
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setCourseName (String courseName){
		this.courseName = courseName ;
	}

	@Column(name = "course_name" ,length = 255 ,nullable = false)
	public String getCourseName (){
		return this.courseName ;
	}

	public void setEng1 (String eng1){
		this.eng1 = eng1 ;
	}

	@Column(name = "eng1" ,length = 255)
	public String getEng1 (){
		return this.eng1 ;
	}

	public void setEng2 (String eng2){
		this.eng2 = eng2 ;
	}

	@Column(name = "eng2" ,length = 255)
	public String getEng2 (){
		return this.eng2 ;
	}

	public void setEng3 (String eng3){
		this.eng3 = eng3 ;
	}

	@Column(name = "eng3" ,length = 255)
	public String getEng3 (){
		return this.eng3 ;
	}

	public void setEng4 (String eng4){
		this.eng4 = eng4 ;
	}

	@Column(name = "eng4" ,length = 255)
	public String getEng4 (){
		return this.eng4 ;
	}

	public void setEng5 (String eng5){
		this.eng5 = eng5 ;
	}

	@Column(name = "eng5" ,length = 255)
	public String getEng5 (){
		return this.eng5 ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setType (String type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 10 ,nullable = false)
	public String getType (){
		return this.type ;
	}

}