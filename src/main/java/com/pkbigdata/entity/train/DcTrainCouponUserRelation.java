package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 优惠券-用户关系表
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/28
 */
@Entity
@Table(name = "dc_train_coupon_user_relation" ,catalog = "dc_train")
public class DcTrainCouponUserRelation implements java.io.Serializable {
    public DcTrainCouponUserRelation(){}

    private Integer id;
	private Integer couponId ;				// 优惠券id
	private Integer userId ;				// 用户id
	private java.sql.Timestamp createTime ;	// 用户获得优惠码时间
	private java.sql.Timestamp useTime ;	// 用户使用时间
	private String useOrderId ;			// 使用该优惠码的订单
	private Integer useFlag ;				// 0 为未使用

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setCouponId (Integer couponId){
		this.couponId = couponId ;
	}

	@Column(name = "coupon_id" ,length = 13 ,nullable = false)
	public Integer getCouponId (){
		return this.couponId ;
	}

	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setUseTime (java.sql.Timestamp useTime){
		this.useTime = useTime ;
	}

	@Column(name = "use_time")
	public java.sql.Timestamp getUseTime (){
		return this.useTime ;
	}

	public void setUseOrderId (String useOrderId){
		this.useOrderId = useOrderId ;
	}

	@Column(name = "use_order_id" ,length = 100)
	public String getUseOrderId (){
		return this.useOrderId ;
	}

	public void setUseFlag (Integer useFlag){
		this.useFlag = useFlag ;
	}

	@Column(name = "use_flag" ,length = 1 ,nullable = false)
	public Integer getUseFlag (){
		return this.useFlag ;
	}

}