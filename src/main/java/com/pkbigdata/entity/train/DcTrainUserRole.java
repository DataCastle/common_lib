package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 角色用户关联表
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/18
 */
@Entity
@Table(name = "dc_train_user_role" ,catalog = "dc_train")
public class DcTrainUserRole implements java.io.Serializable {
    public DcTrainUserRole(){}

    private Integer id;
	private Integer userId ;				// 培训用户id
	private Integer roleId ;				// 角色id

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setRoleId (Integer roleId){
		this.roleId = roleId ;
	}

	@Column(name = "role_id" ,length = 11 ,nullable = false)
	public Integer getRoleId (){
		return this.roleId ;
	}

}