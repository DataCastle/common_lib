package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 发票抬头表
 * @desc	使用Entity生成器生成.
 * @date 	2017/09/05
 */
@Entity
@Table(name = "dc_train_invoice_title" ,catalog = "dc_train")
public class DcTrainInvoiceTitle implements java.io.Serializable {
    public DcTrainInvoiceTitle(){}

    private Integer id;
	private Integer userId ;				// 用户id
	private String invoiceTitle ;			// 发票抬头
	private String invoiceNo ;				// 税收机构码
	private Integer type;                  //发票类型(1.电子发票.2纸质发票)
	private String invoiceType ;			// 发票类型，企业或者个人
	private String invoiceAddress ;			// 单位地址
	private String invoicePhone ;			// 单位电话
	private String bank ;					// 开户行
	private String bankNo ;					// 开户行账号
	private Boolean Comment;				//是否备注详细课程


	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 16)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setInvoiceTitle (String invoiceTitle){
		this.invoiceTitle = invoiceTitle ;
	}

	@Column(name = "invoice_title" ,length = 255)
	public String getInvoiceTitle (){
		return this.invoiceTitle ;
	}

	public void setInvoiceNo (String invoiceNo){
		this.invoiceNo = invoiceNo ;
	}

	@Column(name = "invoice_no" ,length = 255)
	public String getInvoiceNo (){
		return this.invoiceNo ;
	}

	@Column(name="type",length = 1)
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public void setInvoiceType (String invoiceType){
		this.invoiceType = invoiceType ;
	}

	@Column(name = "invoice_type" ,length = 10)
	public String getInvoiceType (){
		return this.invoiceType ;
	}

	public void setInvoiceAddress (String invoiceAddress){
		this.invoiceAddress = invoiceAddress ;
	}

	@Column(name = "invoice_address" ,length = 500)
	public String getInvoiceAddress (){
		return this.invoiceAddress ;
	}

	public void setInvoicePhone (String invoicePhone){
		this.invoicePhone = invoicePhone ;
	}

	@Column(name = "invoice_phone" ,length = 255)
	public String getInvoicePhone (){
		return this.invoicePhone ;
	}

	public void setBank (String bank){
		this.bank = bank ;
	}

	@Column(name = "bank" ,length = 255)
	public String getBank (){
		return this.bank ;
	}

	public void setBankNo (String bankNo){
		this.bankNo = bankNo ;
	}

	@Column(name = "bank_no" ,length = 255)
	public String getBankNo (){
		return this.bankNo ;
	}

	@Column(name = "comment")
	public Boolean getComment() {
		return Comment;
	}

	public void setComment(Boolean comment) {
		Comment = comment;
	}


}