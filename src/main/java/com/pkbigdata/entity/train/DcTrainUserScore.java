package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 用户积分表
 * @desc	使用Entity生成器生成.
 * @date 	2017/07/20
 */
@Entity
@Table(name = "dc_train_user_score" ,catalog = "dc_train")
public class DcTrainUserScore implements java.io.Serializable {
    public DcTrainUserScore(){}

    private Integer id;
	private Integer userId ;				// 用户id
	private Integer totalScore ;			// 总积分
	private Integer exerciseScore ;			// 答题积分

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setTotalScore (Integer totalScore){
		this.totalScore = totalScore ;
	}

	@Column(name = "total_score" ,length = 10 ,nullable = false)
	public Integer getTotalScore (){
		return this.totalScore ;
	}

	public void setExerciseScore (Integer exerciseScore){
		this.exerciseScore = exerciseScore ;
	}

	@Column(name = "exercise_score" ,length = 10 ,nullable = false)
	public Integer getExerciseScore (){
		return this.exerciseScore ;
	}

}