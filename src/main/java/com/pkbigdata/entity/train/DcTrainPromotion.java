package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 推广渠道
 * @desc	使用Entity生成器生成.
 * @date 	2017/06/15
 */
@Entity
@Table(name = "dc_train_promotion" ,catalog = "dc_train")
public class DcTrainPromotion implements java.io.Serializable {
    public DcTrainPromotion(){}

    private Integer id;
	private String name ;					// 推广渠道名称
	private String goodsUrl ;				// 推广页面链接
	private String channelUrl ;				// 推广渠道链接
	private String paramName ;				// 渠道在url 拼的 key 的名字
	private Integer isCoupon ;				// 是否优惠
	private Integer couponId ;				// 优惠码表id
	private Integer createBy ;
	private java.sql.Timestamp createTime ;
	private Integer updateBy ;
	private java.sql.Timestamp updateTime ;
	private Integer delFlag ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setName (String name){
		this.name = name ;
	}

	@Column(name = "name" ,length = 255 ,nullable = false)
	public String getName (){
		return this.name ;
	}

	public void setGoodsUrl (String goodsUrl){
		this.goodsUrl = goodsUrl ;
	}

	@Column(name = "goods_url" ,length = 500 ,nullable = false)
	public String getGoodsUrl (){
		return this.goodsUrl ;
	}

	public void setChannelUrl (String channelUrl){
		this.channelUrl = channelUrl ;
	}

	@Column(name = "channel_url" ,length = 500 ,nullable = false)
	public String getChannelUrl (){
		return this.channelUrl ;
	}

	public void setParamName (String paramName){
		this.paramName = paramName ;
	}

	@Column(name = "param_name" ,length = 25)
	public String getParamName (){
		return this.paramName ;
	}

	public void setIsCoupon (Integer isCoupon){
		this.isCoupon = isCoupon ;
	}

	@Column(name = "is_coupon" ,length = 1 ,nullable = false)
	public Integer getIsCoupon (){
		return this.isCoupon ;
	}

	public void setCouponId (Integer couponId){
		this.couponId = couponId ;
	}

	@Column(name = "coupon_id" ,length = 13)
	public Integer getCouponId (){
		return this.couponId ;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setUpdateBy (Integer updateBy){
		this.updateBy = updateBy ;
	}

	@Column(name = "update_by" ,length = 13)
	public Integer getUpdateBy (){
		return this.updateBy ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

}