package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 记录 ppt关联的ppt 转存到七牛以后的7牛的路径，用于分片下载失败后的下载修复。
 * @desc	使用Entity生成器生成.
 * @date 	2017/06/28
 */
@Entity
@Table(name = "dc_train_data_ppt_qiniu" ,catalog = "dc_train")
public class DcTrainDataPptQiniu implements java.io.Serializable {
    public DcTrainDataPptQiniu(){}

    private Integer id;
	private Integer fileId ;				// 七牛的url 转存的文件的id
	private String qiniuUrl ;				// 7牛的url

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setFileId (Integer fileId){
		this.fileId = fileId ;
	}

	@Column(name = "file_id" ,length = 13 ,nullable = false)
	public Integer getFileId (){
		return this.fileId ;
	}

	public void setQiniuUrl (String qiniuUrl){
		this.qiniuUrl = qiniuUrl ;
	}

	@Column(name = "qiniu_url" ,length = 255 ,nullable = false)
	public String getQiniuUrl (){
		return this.qiniuUrl ;
	}

}