package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 管理员删除评论记录
 * @desc	使用Entity生成器生成.
 * @date 	2017/09/05
 */
@Entity
@Table(name = "dc_train_comment_admin_del_log" ,catalog = "dc_train")
public class DcTrainCommentAdminDelLog implements java.io.Serializable {
    public DcTrainCommentAdminDelLog(){}

    private Integer id;
	private Integer commentId ;				// 被删除的评论id
	private String reason ;					// 删除原因
	private java.sql.Timestamp createTime ;	// 创建时间

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setCommentId (Integer commentId){
		this.commentId = commentId ;
	}

	@Column(name = "comment_id" ,length = 13 ,nullable = false)
	public Integer getCommentId (){
		return this.commentId ;
	}

	public void setReason (String reason){
		this.reason = reason ;
	}

	@Column(name = "reason" ,length = 500)
	public String getReason (){
		return this.reason ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

}