package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 交易流水表
 * @desc	使用Entity生成器生成.
 * @date 	2017/05/11
 */
@Entity
@Table(name = "dc_train_trading_serial" ,catalog = "dc_train")
public class DcTrainTradingSerial implements java.io.Serializable {
    public DcTrainTradingSerial(){}

    private Integer id;
	private String tradingId ;
	private String thirdOrderId ;			// 第三方订单号
	private Integer payWay ;				// 字符方式
	private String payWayInfo ;				// 支付方式描述
	private java.sql.Timestamp endTime ;	// 完成时间
	private String orderId ;				// 订单ID
	private Float payMoney ;				// 流水金额
	private String thirdUserId ;			// 支付渠道的ID
	private String thirdUserName ;			// 支付渠道用户名
	private Integer type ;					// 流水类型，退款或付款成功
	private String typeInfo ;				// type描述

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setTradingId (String tradingId){
		this.tradingId = tradingId ;
	}

	@Column(name = "trading_id" ,length = 100  ,unique = true)
	public String getTradingId (){
		return this.tradingId ;
	}

	public void setThirdOrderId (String thirdOrderId){
		this.thirdOrderId = thirdOrderId ;
	}

	@Column(name = "third_order_id" ,length = 100)
	public String getThirdOrderId (){
		return this.thirdOrderId ;
	}

	public void setPayWay (Integer payWay){
		this.payWay = payWay ;
	}

	@Column(name = "pay_way" ,length = 1)
	public Integer getPayWay (){
		return this.payWay ;
	}

	public void setPayWayInfo (String payWayInfo){
		this.payWayInfo = payWayInfo ;
	}

	@Column(name = "pay_way_info" ,length = 255)
	public String getPayWayInfo (){
		return this.payWayInfo ;
	}

	public void setEndTime (java.sql.Timestamp endTime){
		this.endTime = endTime ;
	}

	@Column(name = "end_time")
	public java.sql.Timestamp getEndTime (){
		return this.endTime ;
	}

	public void setOrderId (String orderId){
		this.orderId = orderId ;
	}

	@Column(name = "order_id" ,length = 200 ,nullable = false)
	public String getOrderId (){
		return this.orderId ;
	}

	public void setPayMoney (Float payMoney){
		this.payMoney = payMoney ;
	}

	@Column(name = "pay_money")
	public Float getPayMoney (){
		return this.payMoney ;
	}

	public void setThirdUserId (String thirdUserId){
		this.thirdUserId = thirdUserId ;
	}

	@Column(name = "third_user_id" ,length = 200)
	public String getThirdUserId (){
		return this.thirdUserId ;
	}

	public void setThirdUserName (String thirdUserName){
		this.thirdUserName = thirdUserName ;
	}

	@Column(name = "third_user_name" ,length = 200)
	public String getThirdUserName (){
		return this.thirdUserName ;
	}

	public void setType (Integer type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 1)
	public Integer getType (){
		return this.type ;
	}

	public void setTypeInfo (String typeInfo){
		this.typeInfo = typeInfo ;
	}

	@Column(name = "type_info" ,length = 255)
	public String getTypeInfo (){
		return this.typeInfo ;
	}

}