package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 培训banner图表
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/18
 */
@Entity
@Table(name = "dc_train_banner" ,catalog = "dc_train")
public class DcTrainBanner implements java.io.Serializable {
    public DcTrainBanner(){}

    private Integer id;
	private String imgUrl ;					// 图片路径
	private String jumpUrl ;				// 跳转链接路径
	private String type ;					// 类型
	private Integer sort ;					// 排序
	private Integer status ;
	private java.sql.Timestamp createTime ;
	private Integer createBy ;
	private java.sql.Timestamp updateTime ;
	private Integer updateBy ;
	private String remarks ;
	private Integer delFlag ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setImgUrl (String imgUrl){
		this.imgUrl = imgUrl ;
	}

	@Column(name = "img_url" ,length = 500 ,nullable = false)
	public String getImgUrl (){
		return this.imgUrl ;
	}

	public void setJumpUrl (String jumpUrl){
		this.jumpUrl = jumpUrl ;
	}

	@Column(name = "jump_url" ,length = 500)
	public String getJumpUrl (){
		return this.jumpUrl ;
	}

	public void setType (String type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 255 ,nullable = false)
	public String getType (){
		return this.type ;
	}

	public void setSort (Integer sort){
		this.sort = sort ;
	}

	@Column(name = "sort" ,length = 1 ,nullable = false)
	public Integer getSort (){
		return this.sort ;
	}

	public void setStatus (Integer status){
		this.status = status ;
	}

	@Column(name = "status" ,length = 1 ,nullable = false)
	public Integer getStatus (){
		return this.status ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setUpdateBy (Integer updateBy){
		this.updateBy = updateBy ;
	}

	@Column(name = "update_by")
	public Integer getUpdateBy (){
		return this.updateBy ;
	}

	public void setRemarks (String remarks){
		this.remarks = remarks ;
	}

	@Column(name = "remarks" ,length = 255)
	public String getRemarks (){
		return this.remarks ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

}