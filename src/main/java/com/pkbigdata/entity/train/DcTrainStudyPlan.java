package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/07/18
 */
@Entity
@Table(name = "dc_train_study_plan" ,catalog = "dc_train")
public class DcTrainStudyPlan implements java.io.Serializable {
    public DcTrainStudyPlan(){}

    private Integer id;
	private Integer courseId ;				// 课程id
	private Integer userId ;
	private String planName ;				// 计划顺序
	private String planDate ;				// 计划日期
	private java.sql.Time startTime ;		// 开始时间
	private java.sql.Time endTime ;			// 结束时间
	private Integer minutes ;				// 分钟
	private String startClass ;				// 开始章节
	private String endClass ;				// 结束章节
	private Integer classNum ;				// 章节数
	private String planTarget ;				// 学习目标
	private java.sql.Timestamp createTime ;
	private Integer taskId;//定时任务id

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13 ,nullable = false)
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setPlanName (String planName){
		this.planName = planName ;
	}

	@Column(name = "plan_name" ,length = 20 ,nullable = false)
	public String getPlanName (){
		return this.planName ;
	}

	public void setPlanDate (String planDate){
		this.planDate = planDate ;
	}

	@Column(name = "plan_date" ,length = 20 ,nullable = false)
	public String getPlanDate (){
		return this.planDate ;
	}

	public void setStartTime (java.sql.Time startTime){
		this.startTime = startTime ;
	}

	@Column(name = "start_time" ,nullable = false)
	public java.sql.Time getStartTime (){
		return this.startTime ;
	}

	public void setEndTime (java.sql.Time endTime){
		this.endTime = endTime ;
	}

	@Column(name = "end_time" ,nullable = false)
	public java.sql.Time getEndTime (){
		return this.endTime ;
	}

	public void setMinutes (Integer minutes){
		this.minutes = minutes ;
	}

	@Column(name = "minutes" ,length = 5 ,nullable = false)
	public Integer getMinutes (){
		return this.minutes ;
	}

	public void setStartClass (String startClass){
		this.startClass = startClass ;
	}

	@Column(name = "start_class" ,length = 20 ,nullable = false)
	public String getStartClass (){
		return this.startClass ;
	}

	public void setEndClass (String endClass){
		this.endClass = endClass ;
	}

	@Column(name = "end_class" ,length = 20 ,nullable = false)
	public String getEndClass (){
		return this.endClass ;
	}

	public void setClassNum (Integer classNum){
		this.classNum = classNum ;
	}

	@Column(name = "class_num" ,length = 5 ,nullable = false)
	public Integer getClassNum (){
		return this.classNum ;
	}

	public void setPlanTarget (String planTarget){
		this.planTarget = planTarget ;
	}

	@Column(name = "plan_target" ,length = 500)
	public String getPlanTarget (){
		return this.planTarget ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	@Column(name = "task_id")
	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
}