package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/06/28
 */
@Entity
@Table(name = "dc_train_data_ppt_jpg" ,catalog = "dc_train")
public class DcTrainDataPptJpg implements java.io.Serializable {
    public DcTrainDataPptJpg(){}

    private Integer id;
	private Integer pptId ;					// 关联的pptId
	private Integer pptQiniuId ;			// 需要转化的ppt_qiniu表的id
	private String url ;					// 服务器存储路径
	private java.sql.Timestamp createTime ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setPptId (Integer pptId){
		this.pptId = pptId ;
	}

	@Column(name = "ppt_id" ,length = 11 ,nullable = false)
	public Integer getPptId (){
		return this.pptId ;
	}

	public void setPptQiniuId (Integer pptQiniuId){
		this.pptQiniuId = pptQiniuId ;
	}

	@Column(name = "ppt_qiniu_id" ,length = 13 ,nullable = false)
	public Integer getPptQiniuId (){
		return this.pptQiniuId ;
	}

	public void setUrl (String url){
		this.url = url ;
	}

	@Column(name = "url" ,length = 255 ,nullable = false)
	public String getUrl (){
		return this.url ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

}