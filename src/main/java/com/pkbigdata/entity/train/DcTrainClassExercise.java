package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 课时题目表
 * @desc	使用Entity生成器生成.
 * @date 	2017/07/26
 */
@Entity
@Table(name = "dc_train_class_exercise" ,catalog = "dc_train")
public class DcTrainClassExercise implements java.io.Serializable {
    public DcTrainClassExercise(){}

    private Integer id;
	private Integer courseId ;				// 课程体系id
	private Integer courseItemId ;			// 章节id
	private String imageFilePath ;			// 图片id
	private Integer classId ;				// 课时id
	private String content ;				// 题目内容
	private String selections ;				// 各个选项，各个选项用特殊符号隔开
	private String answer ;					// 答案
	private String tip ;					// 参考思路
	private Integer halfCodeId ;			// 编程题 的 部分代码
	private Integer dataFileId ;			// 参考资料的 fileId
	private Integer type ;					// 类型 ，编程 单选 多选
	private Integer score ;					// 积分
	private Integer canSkip ;				// 是否可以跳过 0不可以 1 可以
	private Integer status ;				// 是否上线

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13 ,nullable = false)
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setCourseItemId (Integer courseItemId){
		this.courseItemId = courseItemId ;
	}

	@Column(name = "course_item_id" ,length = 13 ,nullable = false)
	public Integer getCourseItemId (){
		return this.courseItemId ;
	}

	public void setImageFilePath (String imageFilePath){
		this.imageFilePath = imageFilePath ;
	}

	@Column(name = "image_file_path" ,length = 500)
	public String getImageFilePath (){
		return this.imageFilePath ;
	}

	public void setClassId (Integer classId){
		this.classId = classId ;
	}

	@Column(name = "class_id" ,length = 13 ,nullable = false)
	public Integer getClassId (){
		return this.classId ;
	}

	public void setContent (String content){
		this.content = content ;
	}

	@Column(name = "content" ,length = 1000 ,nullable = false)
	public String getContent (){
		return this.content ;
	}

	public void setSelections (String selections){
		this.selections = selections ;
	}

	@Column(name = "selections" ,length = 1000)
	public String getSelections (){
		return this.selections ;
	}

	public void setAnswer (String answer){
		this.answer = answer ;
	}

	@Column(name = "answer" ,length = 255 ,nullable = false)
	public String getAnswer (){
		return this.answer ;
	}

	public void setTip (String tip){
		this.tip = tip ;
	}

	@Column(name = "tip" ,length = 1000)
	public String getTip (){
		return this.tip ;
	}

	public void setHalfCodeId (Integer halfCodeId){
		this.halfCodeId = halfCodeId ;
	}

	@Column(name = "half_code_id" ,length = 13)
	public Integer getHalfCodeId (){
		return this.halfCodeId ;
	}

	public void setDataFileId (Integer dataFileId){
		this.dataFileId = dataFileId ;
	}

	@Column(name = "data_file_id" ,length = 13)
	public Integer getDataFileId (){
		return this.dataFileId ;
	}

	public void setType (Integer type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 1 ,nullable = false)
	public Integer getType (){
		return this.type ;
	}

	public void setScore (Integer score){
		this.score = score ;
	}

	@Column(name = "score" ,length = 5 ,nullable = false)
	public Integer getScore (){
		return this.score ;
	}

	public void setCanSkip (Integer canSkip){
		this.canSkip = canSkip ;
	}

	@Column(name = "can_skip" ,length = 1)
	public Integer getCanSkip (){
		return this.canSkip ;
	}

	public void setStatus (Integer status){
		this.status = status ;
	}

	@Column(name = "status" ,length = 1 ,nullable = false)
	public Integer getStatus (){
		return this.status ;
	}

}