package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/05/11
 */
@Entity
@Table(name = "dc_train_channel" ,catalog = "dc_train")
public class DcTrainChannel implements java.io.Serializable {
    public DcTrainChannel(){}

    private Integer id;
	private String serialNum;//销售编号
	private String organization ;			// 单位，如：电子科大、四川大学
	private String organizationNum;//单位编号
	private String name ;					// 渠道的名字
	private String type ;					// 一级渠道名称，如：线上、线下
	private String typeNum;//类型编号
	private Float rate ;					// 提成率
	private Integer number ;				// 销售量
	private String area;//区域
	private java.sql.Timestamp time ;		// 签约时间
	private java.sql.Timestamp createTime ;	// 签约时间
	private Integer createBy ;
	private Integer updateBy ;
	private java.sql.Timestamp updateTime ;
	private Integer delFlag ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "serial_num",length = 255)
	public String getSerialNum() {
		return serialNum;
	}

	public void setSerialNum(String serialNum) {
		this.serialNum = serialNum;
	}

	public void setOrganization (String organization){
		this.organization = organization ;
	}

	@Column(name = "organization" ,length = 255 ,nullable = false)
	public String getOrganization (){
		return this.organization ;
	}

	public void setName (String name){
		this.name = name ;
	}

	@Column(name = "name" ,length = 255)
	public String getName (){
		return this.name ;
	}

	public void setType (String type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 255 ,nullable = false)
	public String getType (){
		return this.type ;
	}

	public void setRate (Float rate){
		this.rate = rate ;
	}

	@Column(name = "rate")
	public Float getRate (){
		return this.rate ;
	}

	public void setNumber (Integer number){
		this.number = number ;
	}

	@Column(name = "number" ,length = 11)
	public Integer getNumber (){
		return this.number ;
	}

	public void setTime (java.sql.Timestamp time){
		this.time = time ;
	}

	@Column(name = "area",length = 255,nullable = false)
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	@Column(name = "time")
	public java.sql.Timestamp getTime (){
		return this.time ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

	public void setUpdateBy (Integer updateBy){
		this.updateBy = updateBy ;
	}

	@Column(name = "update_by" ,length = 13)
	public Integer getUpdateBy (){
		return this.updateBy ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

	@Column(name="organization_num")
	public String getOrganizationNum() {
		return organizationNum;
	}

	public void setOrganizationNum(String organizationNum) {
		this.organizationNum = organizationNum;
	}

	@Column(name = "type_num")
	public String getTypeNum() {
		return typeNum;
	}

	public void setTypeNum(String typeNum) {
		this.typeNum = typeNum;
	}
}