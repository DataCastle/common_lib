package com.pkbigdata.entity.train;
import javax.persistence.*;
import java.sql.Date;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/05/15
 */
@Entity
@Table(name = "dc_train_course_cost" ,catalog = "dc_train")
public class DcTrainCourseCost implements java.io.Serializable {
    public DcTrainCourseCost(){}

    private Integer id;
	private Integer courseId ;				// 课程体系id
	private Double siteCost ;				// 场地费
	private Double installationCost ;		// 设备费用
	private Double otherCost ;				// 其他费用
	private String otherRemarks ;			// 其他费用用途
	private Date time;//成本产生日期
	private String url;//修改证明文件
	private String remarks ;				// 描述
	private java.sql.Timestamp createTime ;
	private Integer createBy ;
	private Integer updateBy ;
	private java.sql.Timestamp updateTime ;
	private Integer delFlag ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13 ,nullable = false)
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setSiteCost (Double siteCost){
		this.siteCost = siteCost ;
	}

	@Column(name = "site_cost")
	public Double getSiteCost (){
		return this.siteCost ;
	}

	public void setInstallationCost (Double installationCost){
		this.installationCost = installationCost ;
	}

	@Column(name = "installation_cost")
	public Double getInstallationCost (){
		return this.installationCost ;
	}

	public void setOtherCost (Double otherCost){
		this.otherCost = otherCost ;
	}

	@Column(name = "other_cost")
	public Double getOtherCost (){
		return this.otherCost ;
	}

	public void setOtherRemarks (String otherRemarks){
		this.otherRemarks = otherRemarks ;
	}

	@Column(name = "other_remarks" ,length = 500)
	public String getOtherRemarks (){
		return this.otherRemarks ;
	}

	public void setRemarks (String remarks){
		this.remarks = remarks ;
	}

	@Column(name = "remarks" ,length = 500)
	public String getRemarks (){
		return this.remarks ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

	public void setUpdateBy (Integer updateBy){
		this.updateBy = updateBy ;
	}

	@Column(name = "update_by" ,length = 13)
	public Integer getUpdateBy (){
		return this.updateBy ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

	@Column(name = "time")
	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@Column(name = "url")
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}