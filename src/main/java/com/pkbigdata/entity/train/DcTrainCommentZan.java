package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 用户点赞记录表格
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/18
 */
@Entity
@Table(name = "dc_train_comment_zan" ,catalog = "dc_train")
public class DcTrainCommentZan implements java.io.Serializable {
    public DcTrainCommentZan(){}

    private Integer id;
	private Integer userId ;				// 用户id
	private Integer commentId ;				// 评论id
	private java.sql.Timestamp createTime ;	// 点赞时间
	private Integer delFlag ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setCommentId (Integer commentId){
		this.commentId = commentId ;
	}

	@Column(name = "comment_id" ,length = 13 ,nullable = false)
	public Integer getCommentId (){
		return this.commentId ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

}