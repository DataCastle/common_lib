package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 课程体系(销售的对象基础信息)目前最小销售对象 UE 4.1.6
 * @desc	使用Entity生成器生成.
 * @date 	2017/09/18
 */
@Entity
@Table(name = "dc_train_course" ,catalog = "dc_train")
public class DcTrainCourse implements java.io.Serializable {
    public DcTrainCourse(){}

    private Integer id;
	private String name ;					// 课程名称
	private String slogan ;					// 宣传语-暂时没有展示
	private String introduction ;			// 课程简介
	private String imgList ;				// 列表展示图
	private String imgDetail ;				// 详情展示图
	private String courseGain ;				// 课程收获
	private String weHave ;					// 我们拥有
	private String facePeople ;				// 面向人群，页面文案为”如果你是"
	private String preRequire ;				// 预备知识
	private String studyTip ;				// 温馨提示
	private String courseAdvantage ;		// 课程介绍
	private Integer previewStatus ;			// 普通用户和游客是否可预览
	private java.sql.Timestamp startTime ;	// 开课时间
	private String courseOutline ;			// 学习计划
	private Integer courseNum ;				// 课程数量
	private Integer sizeType ;				// 1.大课 2.小课
	private Integer status ;				// 课程状态
	private Integer createBy ;
	private java.sql.Timestamp createTime ;
	private Integer updateBy ;
	private java.sql.Timestamp updateTime ;
	private Integer delFlag ;
	private String tryUrl ;					// 试看视频路径
	private Integer sort ;					// 排序
	private Integer hasCertificate ;		// 是否有证书

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setName (String name){
		this.name = name ;
	}

	@Column(name = "name" ,length = 190 ,nullable = false ,unique = true)
	public String getName (){
		return this.name ;
	}

	public void setSlogan (String slogan){
		this.slogan = slogan ;
	}

	@Column(name = "slogan" ,length = 20)
	public String getSlogan (){
		return this.slogan ;
	}

	public void setIntroduction (String introduction){
		this.introduction = introduction ;
	}

	@Column(name = "introduction")
	public String getIntroduction (){
		return this.introduction ;
	}

	public void setImgList (String imgList){
		this.imgList = imgList ;
	}

	@Column(name = "img_list" ,length = 500)
	public String getImgList (){
		return this.imgList ;
	}

	public void setImgDetail (String imgDetail){
		this.imgDetail = imgDetail ;
	}

	@Column(name = "img_detail" ,length = 500)
	public String getImgDetail (){
		return this.imgDetail ;
	}

	public void setCourseGain (String courseGain){
		this.courseGain = courseGain ;
	}

	@Column(name = "course_gain")
	public String getCourseGain (){
		return this.courseGain ;
	}

	public void setWeHave (String weHave){
		this.weHave = weHave ;
	}

	@Column(name = "we_have")
	public String getWeHave (){
		return this.weHave ;
	}

	public void setFacePeople (String facePeople){
		this.facePeople = facePeople ;
	}

	@Column(name = "face_people")
	public String getFacePeople (){
		return this.facePeople ;
	}

	public void setPreRequire (String preRequire){
		this.preRequire = preRequire ;
	}

	@Column(name = "pre_require")
	public String getPreRequire (){
		return this.preRequire ;
	}

	public void setStudyTip (String studyTip){
		this.studyTip = studyTip ;
	}

	@Column(name = "study_tip" ,length = 1000)
	public String getStudyTip (){
		return this.studyTip ;
	}

	public void setCourseAdvantage (String courseAdvantage){
		this.courseAdvantage = courseAdvantage ;
	}

	@Column(name = "course_advantage")
	public String getCourseAdvantage (){
		return this.courseAdvantage ;
	}

	public void setPreviewStatus (Integer previewStatus){
		this.previewStatus = previewStatus ;
	}

	@Column(name = "preview_status" ,length = 1)
	public Integer getPreviewStatus (){
		return this.previewStatus ;
	}

	public void setStartTime (java.sql.Timestamp startTime){
		this.startTime = startTime ;
	}

	@Column(name = "start_time")
	public java.sql.Timestamp getStartTime (){
		return this.startTime ;
	}

	public void setCourseOutline (String courseOutline){
		this.courseOutline = courseOutline ;
	}

	@Column(name = "course_outline")
	public String getCourseOutline (){
		return this.courseOutline ;
	}

	public void setCourseNum (Integer courseNum){
		this.courseNum = courseNum ;
	}

	@Column(name = "course_num" ,length = 255)
	public Integer getCourseNum (){
		return this.courseNum ;
	}

	public void setSizeType (Integer sizeType){
		this.sizeType = sizeType ;
	}

	@Column(name = "size_type" ,length = 11)
	public Integer getSizeType (){
		return this.sizeType ;
	}

	public void setStatus (Integer status){
		this.status = status ;
	}

	@Column(name = "status" ,length = 1)
	public Integer getStatus (){
		return this.status ;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13)
	public Integer getCreateBy (){
		return this.createBy ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time")
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setUpdateBy (Integer updateBy){
		this.updateBy = updateBy ;
	}

	@Column(name = "update_by" ,length = 13)
	public Integer getUpdateBy (){
		return this.updateBy ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

	public void setTryUrl (String tryUrl){
		this.tryUrl = tryUrl ;
	}

	@Column(name = "try_url" ,length = 500)
	public String getTryUrl (){
		return this.tryUrl ;
	}

	public void setSort (Integer sort){
		this.sort = sort ;
	}

	@Column(name = "sort" ,length = 10)
	public Integer getSort (){
		return this.sort ;
	}

	public void setHasCertificate (Integer hasCertificate){
		this.hasCertificate = hasCertificate ;
	}

	@Column(name = "has_certificate" ,length = 1)
	public Integer getHasCertificate (){
		return this.hasCertificate ;
	}

}