package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/18
 */
@Entity
@Table(name = "dc_train_role_permission" ,catalog = "dc_train")
public class DcTrainRolePermission implements java.io.Serializable {
    public DcTrainRolePermission(){}

    private int id;
	private Integer roleId ;				// 角色id
	private Integer permissionId ;			// 角色id

	@Id
    @Column(name = "id")
    @GeneratedValue
    public int getId() {
         return id;
    }

    public void setId(int id) {
        this.id = id;
    }
	public void setRoleId (Integer roleId){
		this.roleId = roleId ;
	}

	@Column(name = "role_id" ,length = 13)
	public Integer getRoleId (){
		return this.roleId ;
	}

	public void setPermissionId (Integer permissionId){
		this.permissionId = permissionId ;
	}

	@Column(name = "permission_id" ,length = 13)
	public Integer getPermissionId (){
		return this.permissionId ;
	}

}