package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/08/24
 */
@Entity
@Table(name = "dc_train_acc_log" ,catalog = "dc_train")
public class DcTrainAccLog implements java.io.Serializable {
    public DcTrainAccLog(){}

    private Integer id;
	private String adminName ;					// 结算人名称
	private String imgPath ;				// 凭证文件
	private String remarks ;				// 描述
	private String createBy ;				// 结算人
	private java.sql.Timestamp createTime ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setAdminName (String adminName){
		this.adminName = adminName ;
	}

	@Column(name = "admin_name" ,length = 255)
	public String getAdminName (){
		return this.adminName ;
	}

	public void setImgPath (String imgPath){
		this.imgPath = imgPath ;
	}

	@Column(name = "img_path" ,length = 500)
	public String getImgPath (){
		return this.imgPath ;
	}

	public void setRemarks (String remarks){
		this.remarks = remarks ;
	}

	@Column(name = "remarks" ,length = 500)
	public String getRemarks (){
		return this.remarks ;
	}

	public void setCreateBy (String createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 255 ,nullable = false)
	public String getCreateBy (){
		return this.createBy ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

}