package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/24
 */
@Entity
@Table(name = "dc_train_class_understand" ,catalog = "dc_train")
public class DcTrainClassUnderstand implements java.io.Serializable {
    public DcTrainClassUnderstand(){}

    private Integer id;
	private Integer userId ;				// 用户id
	private Integer classId ;				// 课时id
	private Integer timePosition ;			// 看不懂得位置(秒)
	private java.sql.Timestamp createTime ;	// 创建时间

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setClassId (Integer classId){
		this.classId = classId ;
	}

	@Column(name = "class_id" ,length = 13 ,nullable = false)
	public Integer getClassId (){
		return this.classId ;
	}

	public void setTimePosition (Integer timePosition){
		this.timePosition = timePosition ;
	}

	@Column(name = "time_position" ,length = 13 ,nullable = false)
	public Integer getTimePosition (){
		return this.timePosition ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

}