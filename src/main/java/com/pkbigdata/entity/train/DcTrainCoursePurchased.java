package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 用户已经购买的课程
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/18
 */
@Entity
@Table(name = "dc_train_course_purchased" ,catalog = "dc_train")
public class DcTrainCoursePurchased implements java.io.Serializable {
    public DcTrainCoursePurchased(){}

    private Integer id;
	private Integer userId ;				// 用户id
	private Integer courseId ;				// 课程体系id
	private java.sql.Timestamp startTime ;	// 课程体系购买时间
	private java.sql.Timestamp endTime ;	// 课程体系过期时间
	private String orderId ;				// 订单号

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13 ,nullable = false)
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setStartTime (java.sql.Timestamp startTime){
		this.startTime = startTime ;
	}

	@Column(name = "start_time" ,nullable = false)
	public java.sql.Timestamp getStartTime (){
		return this.startTime ;
	}

	public void setEndTime (java.sql.Timestamp endTime){
		this.endTime = endTime ;
	}

	@Column(name = "end_time" ,nullable = false)
	public java.sql.Timestamp getEndTime (){
		return this.endTime ;
	}

	public void setOrderId (String orderId){
		this.orderId = orderId ;
	}

	@Column(name = "order_id" ,length = 100 ,nullable = false)
	public String getOrderId (){
		return this.orderId ;
	}

}