package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 课时播放记录表
 * @desc	使用Entity生成器生成.
 * @date 	2017/08/01
 */
@Entity
@Table(name = "dc_train_play_record" ,catalog = "dc_train")
public class DcTrainPlayRecord implements java.io.Serializable {
    public DcTrainPlayRecord(){}

    private Integer id;
	private Integer userId ;				// 用户id
	private Integer classId ;				// 课时id
	private Integer timePosition ;			// 播放到的时间位置，秒
	private Integer maxTimePosition ;		// 曾经最大的观看位置
	private Integer status ;				// 是否学过了
	private java.sql.Timestamp createTime ;	// 第一次观看时间
	private java.sql.Timestamp updateTime ;	// 最近观看时间

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setClassId (Integer classId){
		this.classId = classId ;
	}

	@Column(name = "class_id" ,length = 13 ,nullable = false)
	public Integer getClassId (){
		return this.classId ;
	}

	public void setTimePosition (Integer timePosition){
		this.timePosition = timePosition ;
	}

	@Column(name = "time_position" ,length = 5 ,nullable = false)
	public Integer getTimePosition (){
		return this.timePosition ;
	}

	public void setMaxTimePosition (Integer maxTimePosition){
		this.maxTimePosition = maxTimePosition ;
	}

	@Column(name = "max_time_position" ,length = 5)
	public Integer getMaxTimePosition (){
		return this.maxTimePosition ;
	}

	public void setStatus (Integer status){
		this.status = status ;
	}

	@Column(name = "status" ,length = 1 ,nullable = false)
	public Integer getStatus (){
		return this.status ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}




	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

}