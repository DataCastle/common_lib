package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/05/15
 */
@Entity
@Table(name = "dc_train_teacher_cost" ,catalog = "dc_train")
public class DcTrainTeacherCost implements java.io.Serializable {
    public DcTrainTeacherCost(){}

    private Integer id;
	private Integer teacherId ;				// 导师id
	private String teacherName ;			// 导师名称
	private Integer cid ;					// 课程成本费Id
	private Float cost ;					// 费用
	private java.sql.Timestamp createTime ;
	private Integer createBy ;
	private Integer updateBy ;
	private java.sql.Timestamp updateTime ;
	private String remarks ;
	private Integer delFlag ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setTeacherId (Integer teacherId){
		this.teacherId = teacherId ;
	}

	@Column(name = "teacher_id" ,length = 13 ,nullable = false)
	public Integer getTeacherId (){
		return this.teacherId ;
	}

	public void setTeacherName (String teacherName){
		this.teacherName = teacherName ;
	}

	@Column(name = "teacher_name" ,length = 255)
	public String getTeacherName (){
		return this.teacherName ;
	}

	public void setCid (Integer cid){
		this.cid = cid ;
	}

	@Column(name = "cid" ,length = 11 ,nullable = false)
	public Integer getCid (){
		return this.cid ;
	}

	public void setCost (Float cost){
		this.cost = cost ;
	}

	@Column(name = "cost" ,nullable = false)
	public Float getCost (){
		return this.cost ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setCreateBy (Integer createBy){
		this.createBy = createBy ;
	}

	@Column(name = "create_by" ,length = 13 ,nullable = false)
	public Integer getCreateBy (){
		return this.createBy ;
	}

	public void setUpdateBy (Integer updateBy){
		this.updateBy = updateBy ;
	}

	@Column(name = "update_by" ,length = 13)
	public Integer getUpdateBy (){
		return this.updateBy ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time")
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

	public void setRemarks (String remarks){
		this.remarks = remarks ;
	}

	@Column(name = "remarks" ,length = 500)
	public String getRemarks (){
		return this.remarks ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

}