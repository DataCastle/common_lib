package com.pkbigdata.entity.train;
import javax.persistence.*;

/**
 * 课时弹幕表
 * @desc	使用Entity生成器生成.
 * @date 	2017/04/24
 */
@Entity
@Table(name = "dc_train_class_screen" ,catalog = "dc_train")
public class DcTrainClassScreen implements java.io.Serializable {
    public DcTrainClassScreen(){}

    private Integer id;
	private String content ;				// 弹幕内容
	private Integer userId ;				// 评论人
	private Integer classId ;				// 课时ID
	private String treePath ;				// 评论树，root-id2-id3-id4
	private Integer replytoUserId ;
	private Integer timePosition ;//视频播放位置,单位秒
	private java.sql.Timestamp createTime ;	// 评论时间
	private Integer delFlag ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setContent (String content){
		this.content = content ;
	}

	@Column(name = "content" ,length = 1000 ,nullable = false)
	public String getContent (){
		return this.content ;
	}

	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setTreePath (String treePath){
		this.treePath = treePath ;
	}

	@Column(name = "tree_path" ,length = 700)
	public String getTreePath (){
		return this.treePath ;
	}

	public void setReplytoUserId (Integer replytoUserId){
		this.replytoUserId = replytoUserId ;
	}

	@Column(name = "replyto_user_id" ,length = 13)
	public Integer getReplytoUserId (){
		return this.replytoUserId ;
	}

	public void setTimePosition (Integer timePosition){
		this.timePosition = timePosition ;
	}

	@Column(name = "time_position" ,length = 13,nullable = false)
	public Integer getTimePosition (){
		return this.timePosition ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

	@Column(name = "class_id" ,length = 13 ,nullable = false)
	public Integer getClassId() {
		return classId;
	}

	public void setClassId(Integer classId) {
		this.classId = classId;
	}
}