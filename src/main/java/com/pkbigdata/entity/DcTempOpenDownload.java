package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by jay on 2016/11/4 0004.
 */
@Entity
@Table(name = "dc_temp_open_download", catalog = "dc")
public class DcTempOpenDownload implements java.io.Serializable{
    private Integer id;
    private Integer userId;//用户id
    private String type;//状态
    private Timestamp time;//加入时间

    public DcTempOpenDownload(){

    }

    public DcTempOpenDownload(Integer id, Integer userId, String type, Timestamp time) {
        this.id = id;
        this.userId = userId;
        this.type = type;
        this.time = time;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "user_id")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "time")
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
