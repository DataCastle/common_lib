package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ck on 2016/11/29.
 */
@Entity
@Table(name = "dc_usercard_apply", catalog = "dc")
public class DcUsercardApply implements java.io.Serializable{
    private Integer id;
    private Integer userId;//申诉用户id
    private String name;//证件真实姓名
    private String phone;//申诉人手机号
    private String idCardNew;//新身份证号码
    private String idCardOld;//原身份证号码
    private String  headerCardImgNew;//新手持身份证半身照
    private String  faceCardImgNew;//新身份证正面扫描件
    private String  sideCardImgNew;//新身份证反面扫描件
    private String oldName;//原来的姓名
    private String  headerCardImgOld;//原手持身份证半身照
    private String  faceCardImgOld;//原身份证正面扫描件
    private String  sideCardImgOld;//原身份证反面扫描件
    private String content;//内容(原因理由..)
    private Integer type;//申诉类型(1:找回2.更换3.解绑)
    private Boolean status;//申诉状态(null:进行中,0:不同意,1:同意)
    private Timestamp createTime;//申诉时间
    private Timestamp updateTime;//处理时间
    private String auditOpinion;//审核意见
    private Integer cardType;//证件类型(1身份证,2回乡证,3台胞证,4护照)


    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name="user_id")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name="id_card_new")
    public String getIdCardNew() {
        return idCardNew;
    }

    public void setIdCardNew(String idCardNew) {
        this.idCardNew = idCardNew;
    }

    @Column(name="id_card_old")
    public String getIdCardOld() {
        return idCardOld;
    }

    public void setIdCardOld(String idCardOld) {
        this.idCardOld = idCardOld;
    }

    @Column(name="header_card_img_new")
    public String getHeaderCardImgNew() {
        return headerCardImgNew;
    }

    public void setHeaderCardImgNew(String headerCardImgNew) {
        this.headerCardImgNew = headerCardImgNew;
    }

    @Column(name="face_card_img_new")
    public String getFaceCardImgNew() {
        return faceCardImgNew;
    }

    public void setFaceCardImgNew(String faceCardImgNew) {
        this.faceCardImgNew = faceCardImgNew;
    }

    @Column(name="side_card_img_new")
    public String getSideCardImgNew() {
        return sideCardImgNew;
    }

    public void setSideCardImgNew(String sideCardImgNew) {
        this.sideCardImgNew = sideCardImgNew;
    }

    @Column(name="header_card_img_old")
    public String getHeaderCardImgOld() {
        return headerCardImgOld;
    }

    public void setHeaderCardImgOld(String headerCardImgOld) {
        this.headerCardImgOld = headerCardImgOld;
    }

    @Column(name="face_card_img_old")
    public String getFaceCardImgOld() {
        return faceCardImgOld;
    }

    public void setFaceCardImgOld(String faceCardImgOld) {
        this.faceCardImgOld = faceCardImgOld;
    }

    @Column(name="side_card_img_old")
    public String getSideCardImgOld() {
        return sideCardImgOld;
    }

    public void setSideCardImgOld(String sideCardImgOld) {
        this.sideCardImgOld = sideCardImgOld;
    }

    @Column(name="content",length = 500)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name="type")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Column(name="status")
    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Column(name="phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name="create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Column(name="update_time")
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name="audit_opinion")
    public String getAuditOpinion() {
        return auditOpinion;
    }

    public void setAuditOpinion(String auditOpinion) {
        this.auditOpinion = auditOpinion;
    }

    @Column(name="name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "card_type")
    public Integer getCardType() {
        return cardType;
    }

    public void setCardType(Integer cardType) {
        this.cardType = cardType;
    }

    @Column(name="old_name")
    public String getOldName() {
        return oldName;
    }

    public void setOldName(String oldName) {
        this.oldName = oldName;
    }

}
