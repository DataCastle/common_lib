package com.pkbigdata.entity.test;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/05/25
 */
@Entity
@Table(name = "dc_合并3_空" ,catalog = "test")
public class Dc合并3_空 implements java.io.Serializable {
    public Dc合并3_空(){}

    private Integer id;
	private String 大类 ;
	private String 品类 ;
	private String 类别编号 ;
	private String 品种 ;
	private String 标准代号 ;
	private String 更细分类;
	private String 标准名称 ;
	private String 检验项分类 ;
	private String 检验项目 ;
	private String 检验项目备注 ;
	private String 适用分类 ;
	private String 采样标准 ;
	private String 限量 ;
	private String 限量单位 ;
	private String 限量单位备注 ;
	private String 限量标准来源 ;
	private String 检测方法标准 ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void set大类 (String 大类){
		this.大类 = 大类 ;
	}

	@Column(name = "大类" ,length = 255)
	public String get大类 (){
		return this.大类 ;
	}

	@Column(name = "品类" ,length = 255)
	public String get品类() {
		return 品类;
	}

	public void set品类(String 品类) {
		this.品类 = 品类;
	}

	public void set类别编号 (String 类别编号){
		this.类别编号 = 类别编号 ;
	}

	@Column(name = "类别编号" ,length = 255)
	public String get类别编号 (){
		return this.类别编号 ;
	}

	public void set品种 (String 品种){
		this.品种 = 品种 ;
	}

	@Column(name = "品种" ,length = 255)
	public String get品种 (){
		return this.品种 ;
	}

	public void set标准代号 (String 标准代号){
		this.标准代号 = 标准代号 ;
	}

	@Column(name = "标准代号" ,length = 255)
	public String get标准代号 (){
		return this.标准代号 ;
	}

	public void set标准名称 (String 标准名称){
		this.标准名称 = 标准名称 ;
	}

	@Column(name = "标准名称" ,length = 255)
	public String get标准名称 (){
		return this.标准名称 ;
	}

	public void set检验项分类 (String 检验项分类){
		this.检验项分类 = 检验项分类 ;
	}

	@Column(name = "检验项分类" ,length = 255)
	public String get检验项分类 (){
		return this.检验项分类 ;
	}

	public void set检验项目 (String 检验项目){
		this.检验项目 = 检验项目 ;
	}

	@Column(name = "检验项目" ,length = 255)
	public String get检验项目 (){
		return this.检验项目 ;
	}

	public void set检验项目备注 (String 检验项目备注){
		this.检验项目备注 = 检验项目备注 ;
	}

	@Column(name = "检验项目备注" ,length = 255)
	public String get检验项目备注 (){
		return this.检验项目备注 ;
	}

	public void set适用分类 (String 适用分类){
		this.适用分类 = 适用分类 ;
	}

	@Column(name = "适用分类" ,length = 255)
	public String get适用分类 (){
		return this.适用分类 ;
	}

	public void set采样标准 (String 采样标准){
		this.采样标准 = 采样标准 ;
	}

	@Column(name = "采样标准" ,length = 255)
	public String get采样标准 (){
		return this.采样标准 ;
	}

	public void set限量 (String 限量){
		this.限量 = 限量 ;
	}

	@Column(name = "限量" ,length = 255)
	public String get限量 (){
		return this.限量 ;
	}

	public void set限量单位 (String 限量单位){
		this.限量单位 = 限量单位 ;
	}

	@Column(name = "限量单位" ,length = 255)
	public String get限量单位 (){
		return this.限量单位 ;
	}

	public void set限量单位备注 (String 限量单位备注){
		this.限量单位备注 = 限量单位备注 ;
	}

	@Column(name = "限量单位备注" ,length = 255)
	public String get限量单位备注 (){
		return this.限量单位备注 ;
	}

	public void set限量标准来源 (String 限量标准来源){
		this.限量标准来源 = 限量标准来源 ;
	}

	@Column(name = "限量标准来源" ,length = 255)
	public String get限量标准来源 (){
		return this.限量标准来源 ;
	}

	public void set检测方法标准 (String 检测方法标准){
		this.检测方法标准 = 检测方法标准 ;
	}

	@Column(name = "检测方法标准" ,length = 255)
	public String get检测方法标准 (){
		return this.检测方法标准 ;
	}

	@Column(name = "更细分类" ,length = 255)
	public String get更细分类() {
		return 更细分类;
	}

	public void set更细分类(String 更细分类) {
		this.更细分类 = 更细分类;
	}
}