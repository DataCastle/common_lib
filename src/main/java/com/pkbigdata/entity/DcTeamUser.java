package com.pkbigdata.entity;// default package

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * DcTeamUser entity. @author MyEclipse Persistence Tools
 * 团队成员表
 */
@Entity
@Table(name = "dc_team_user", catalog = "dc")
public class DcTeamUser implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer teamId;//团队id
	private Integer userId;//用户id
	private Boolean status;//状态
	private String token;//
	private Integer inviter;//邀请人id
    private Integer cmptId;//竞赛id
    private Timestamp joinTime;//加入时间

    // Constructors

    /** default constructor */
    public DcTeamUser() {
    }

    /** minimal constructor */
    public DcTeamUser(Integer userId, Boolean status) {
        this.userId = userId;
        this.status = status;
    }

    /** full constructor */
    public DcTeamUser(Integer teamId, Integer userId, Boolean status,
                      String token, Integer inviter, Integer cmptId) {
        this.teamId = teamId;
        this.userId = userId;
        this.status = status;
        this.token = token;
        this.inviter = inviter;
        this.cmptId = cmptId;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "team_id")
    public Integer getTeamId() {
        return this.teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    @Column(name = "user_id", nullable = false)
    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "status", nullable = false)
    public Boolean getStatus() {
        return this.status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Column(name = "token", length = 32)
    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Column(name = "inviter")
    public Integer getInviter() {
        return this.inviter;
    }

    public void setInviter(Integer inviter) {
        this.inviter = inviter;
    }

    @Column(name = "cmpt_id")
    public Integer getCmptId() {
        return this.cmptId;
    }

    public void setCmptId(Integer cmptId) {
        this.cmptId = cmptId;
    }

    @Column(name = "join_time")
    public Timestamp getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(Timestamp joinTime) {
        this.joinTime = joinTime;
    }
}