package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * OperationLog entity. @author MyEclipse Persistence Tools
 * 用户操作日志
 */
@Entity
@Table(name = "dc_operation_log", catalog = "dc")
public class DcOperationLog implements java.io.Serializable {

	// Fields

	private Integer id;
	private String ip;//访问者id
	private Integer userId;//用户id
	private String username;//用户名称
	private String url;//访问路径
	private String type;//操作类型
	private String content;//操作内容
	private String userAgent;
	private String browser;//浏览器
	private String suorce;//渠道
	private Integer cmptId;//竞赛id
	private String area;
	private String portType;//访问类型(桌面用户,移动用户)
	private String sessionId;
	private Timestamp createTime;
	private String remarks;

	// Constructors

	/** default constructor */
	public DcOperationLog() {
	}


	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "ip")
	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Column(name = "user_id")
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "username")
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "url")
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "type")
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "content")
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "create_time")
	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Column(name = "remarks")
	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name ="user_agent")
	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	@Column(name ="browser")
	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	@Column(name="suorce")
	public String getSuorce() {
		return suorce;
	}

	public void setSuorce(String suorce) {
		this.suorce = suorce;
	}

    @Column(name="area")
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	@Column(name = "port_type")
	public String getPortType() {
		return portType;
	}

	public void setPortType(String portType) {
		this.portType = portType;
	}


	@Column(name = "session_id")
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Column(name="cmpt_id")
	public Integer getCmptId() {
		return cmptId;
	}

	public void setCmptId(Integer cmptId) {
		this.cmptId = cmptId;
	}
}