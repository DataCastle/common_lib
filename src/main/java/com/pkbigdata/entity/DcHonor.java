package com.pkbigdata.entity;// default package

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * DcHonor entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "dc_honor", catalog = "dc")
public class DcHonor implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;//成就名称
	private Timestamp time;//获奖时间
	private Integer userId;//用户id
	private Integer cmptId;//竞赛id
	private Integer teamId;//队伍id
	private Integer rank;//排名
	private Float socre;//得分
	private String prize;//奖励
	private Timestamp createTime;//创建时间
	private Timestamp updateTime;//修改时间
	private String remark;//描述

	// Constructors

	/** default constructor */
	public DcHonor() {
	}

	/** full constructor */
	public DcHonor(String name, Timestamp time, Integer userId) {
		this.name = name;
		this.time = time;
		this.userId = userId;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", length = 500)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Column(name = "user_id")
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "cmpt_id")
	public Integer getCmptId() {
		return cmptId;
	}

	public void setCmptId(Integer cmptId) {
		this.cmptId = cmptId;
	}

	@Column(name = "team_id")
	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	@Column(name = "rank")
	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}
	@Column(name = "create_time")
	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Column(name = "update_time")
	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	@Column(name = "remark")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name="socre")
	public Float getSocre() {
		return socre;
	}

	public void setSocre(Float socre) {
		this.socre = socre;
	}

	@Column(name="prize")
	public String getPrize() {
		return prize;
	}

	public void setPrize(String prize) {
		this.prize = prize;
	}
}