package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * DcTeam entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "dc_team", catalog = "dc", uniqueConstraints = @UniqueConstraint(columnNames = {
		"name", "cmpt_id" }))
public class DcTeam implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer createId;//创建人ID
	private String name;//队伍名称
	private Integer cmptId;//竞赛id
	private Integer leaderId;//队长id
	private Timestamp createTime;//创建时间
	private Timestamp destroyTime;//解散时间
	private Boolean active;//是否活跃
	private String introduce;//简介
	private String avatar;//头像
	private Integer commitAccount;//总提交次数
	private Timestamp latestCommit;//最后一次提交时间
	private Integer number;//人数
	private Integer code;//云平台提交运行的代码id
	private String workName;//作品名称
	private String yunIp;//分配云id
	private String yunPort;//云端口
	private String yunSsh;//云ssh
	private String yunUsername;//云的用户名
	private String yunWebUsername;//云web用户名
	private String yunWebPassword;//云web密码
	private String yunWebUrl;//云web路径
	private String yunSshFilePath;//云web文件路径
    private String scanTime;//云扫描时间
	private String stage;//是否，1-23，同时能1阶段和23阶段
	private String needSkills;//需要技能，通过竖线分割
	private String needMembers;//需要队友,通过竖线分割
	private Integer open;//开放组队,0关闭组队，1开放组队，2招募状态
	private Integer finalRank;//线下最终排名
	private Float publicScore;//线下最终得分
	private String  innovationRemarks;//创新应用简介
	private String remarks;//创新应用简介备注
    private Timestamp innovationTime;//创新应用最后提交时间
	// Constructors

	/** default constructor */
	public DcTeam() {
	}

	/** minimal constructor */
	public DcTeam(String name, Integer cmptId, Integer leaderId) {
		this.name = name;
		this.cmptId = cmptId;
		this.leaderId = leaderId;
	}

	/** full constructor */
	public DcTeam(Integer createId, String name, Integer cmptId,
			Integer leaderId, Float publicScore, Float privateScore,
			Integer rank, Timestamp createTime, Timestamp destroyTime,
			Boolean active, String introduce, String avatar,
			Integer commitAccount, Timestamp latestCommit, Integer number,
			Integer code, String workName, String yunIp, String yunPort,
			String yunSsh, String yunUsername, String yunWebUsername,
			String yunWebPassword, String yunWebUrl, String yunSshFilePath) {
		this.createId = createId;
		this.name = name;
		this.cmptId = cmptId;
		this.leaderId = leaderId;
		this.createTime = createTime;
		this.destroyTime = destroyTime;
		this.active = active;
		this.introduce = introduce;
		this.avatar = avatar;
		this.commitAccount = commitAccount;
		this.latestCommit = latestCommit;
		this.number = number;
		this.code = code;
		this.workName = workName;
		this.yunIp = yunIp;
		this.yunPort = yunPort;
		this.yunSsh = yunSsh;
		this.yunUsername = yunUsername;
		this.yunWebUsername = yunWebUsername;
		this.yunWebPassword = yunWebPassword;
		this.yunWebUrl = yunWebUrl;
		this.yunSshFilePath = yunSshFilePath;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "create_id")
	public Integer getCreateId() {
		return this.createId;
	}

	public void setCreateId(Integer createId) {
		this.createId = createId;
	}

	@Column(name = "name", nullable = false, length = 120)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "cmpt_id", nullable = false)
	public Integer getCmptId() {
		return this.cmptId;
	}

	public void setCmptId(Integer cmptId) {
		this.cmptId = cmptId;
	}

	@Column(name = "leader_id", nullable = false)
	public Integer getLeaderId() {
		return this.leaderId;
	}

	public void setLeaderId(Integer leaderId) {
		this.leaderId = leaderId;
	}


	@Column(name = "create_time", length = 19)
	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Column(name = "destroy_time", length = 19)
	public Timestamp getDestroyTime() {
		return this.destroyTime;
	}

	public void setDestroyTime(Timestamp destroyTime) {
		this.destroyTime = destroyTime;
	}

	@Column(name = "active")
	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Column(name = "introduce", length = 65535)
	public String getIntroduce() {
		return this.introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	@Column(name = "avatar")
	public String getAvatar() {
		return this.avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	@Column(name = "commit_account")
	public Integer getCommitAccount() {
		return this.commitAccount;
	}

	public void setCommitAccount(Integer commitAccount) {
		this.commitAccount = commitAccount;
	}

	@Column(name = "latest_commit", length = 19)
	public Timestamp getLatestCommit() {
		return this.latestCommit;
	}

	public void setLatestCommit(Timestamp latestCommit) {
		this.latestCommit = latestCommit;
	}

	@Column(name = "number")
	public Integer getNumber() {
		return this.number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	@Column(name = "code")
	public Integer getCode() {
		return this.code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	@Column(name = "work_name")
	public String getWorkName() {
		return this.workName;
	}

	public void setWorkName(String workName) {
		this.workName = workName;
	}

	@Column(name = "yun_ip")
	public String getYunIp() {
		return this.yunIp;
	}

	public void setYunIp(String yunIp) {
		this.yunIp = yunIp;
	}

	@Column(name = "yun_port")
	public String getYunPort() {
		return this.yunPort;
	}

	public void setYunPort(String yunPort) {
		this.yunPort = yunPort;
	}

	@Column(name = "yun_ssh", length = 500)
	public String getYunSsh() {
		return this.yunSsh;
	}

	public void setYunSsh(String yunSsh) {
		this.yunSsh = yunSsh;
	}

	@Column(name = "yun_username")
	public String getYunUsername() {
		return this.yunUsername;
	}

	public void setYunUsername(String yunUsername) {
		this.yunUsername = yunUsername;
	}

	@Column(name = "yun_web_username")
	public String getYunWebUsername() {
		return this.yunWebUsername;
	}

	public void setYunWebUsername(String yunWebUsername) {
		this.yunWebUsername = yunWebUsername;
	}

	@Column(name = "yun_web_password", length = 1000)
	public String getYunWebPassword() {
		return this.yunWebPassword;
	}

	public void setYunWebPassword(String yunWebPassword) {
		this.yunWebPassword = yunWebPassword;
	}

	@Column(name = "yun_web_url")
	public String getYunWebUrl() {
		return this.yunWebUrl;
	}

	public void setYunWebUrl(String yunWebUrl) {
		this.yunWebUrl = yunWebUrl;
	}

	@Column(name = "yun_ssh_filePath", length = 500)
	public String getYunSshFilePath() {
		return this.yunSshFilePath;
	}

	public void setYunSshFilePath(String yunSshFilePath) {
		this.yunSshFilePath = yunSshFilePath;
	}

    @Column(name = "yun_scan_time", length = 500)
    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

	@Column(name = "stage", length = 500)
	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	@Column(name = "need_skills", length = 500)
	public String getNeedSkills() {
		return needSkills;
	}

	public void setNeedSkills(String needSkills) {
		this.needSkills = needSkills;
	}
	@Column(name = "need_members", length = 500)
	public String getNeedMembers() {
		return needMembers;
	}

	public void setNeedMembers(String needMembers) {
		this.needMembers = needMembers;
	}

	@Column(name = "open")
	public Integer getOpen() {
		return open;
	}

	public void setOpen(Integer open) {
		this.open = open;
	}


	@Column(name = "final_rank")
	public Integer getFinalRank() {
		return finalRank;
	}

	public void setFinalRank(Integer finalRank) {
		this.finalRank = finalRank;
	}

	@Column(name = "innovation_remarks",length = 1000)
	public String getInnovationRemarks() {
		return innovationRemarks;
	}

	public void setInnovationRemarks(String innovationRemarks) {
		this.innovationRemarks = innovationRemarks;
	}

	@Column(name = "innovation_time")
	public Timestamp getInnovationTime() {
		return innovationTime;
	}

	public void setInnovationTime(Timestamp innovationTime) {
		this.innovationTime = innovationTime;
	}

	@Column(name = "remarks")
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name = "public_score")
	public Float getPublicScore() {
		return publicScore;
	}

	public void setPublicScore(Float publicScore) {
		this.publicScore = publicScore;
	}
}