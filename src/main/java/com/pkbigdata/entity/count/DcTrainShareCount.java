package com.pkbigdata.entity.count;
import javax.persistence.*;

/**
 * 课程统计-分享汇总-出售量表
 * @desc	使用Entity生成器生成.
 * @date 	2017/06/19
 */
@Entity
@Table(name = "dc_train_share_count" ,catalog = "dc_data_count")
public class DcTrainShareCount implements java.io.Serializable {
    public DcTrainShareCount(){}

    private Integer id;
	private Integer courseId ;
	private Integer shareNumQq ;
	private Integer shareNumWeibo ;
	private Integer shareNumWeixin ;
	private Integer shareBackQq ;
	private Integer shareBackWeibo ;
	private Integer shareBackWeixin ;
	private Integer saleNum ;				// 售出量
	private String countDate ;
	private java.sql.Timestamp createTime ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setCourseId (Integer courseId){
		this.courseId = courseId ;
	}

	@Column(name = "course_id" ,length = 13 ,nullable = false)
	public Integer getCourseId (){
		return this.courseId ;
	}

	public void setShareNumQq (Integer shareNumQq){
		this.shareNumQq = shareNumQq ;
	}

	@Column(name = "share_num_qq" ,length = 13 ,nullable = false)
	public Integer getShareNumQq (){
		return this.shareNumQq ;
	}

	public void setShareNumWeibo (Integer shareNumWeibo){
		this.shareNumWeibo = shareNumWeibo ;
	}

	@Column(name = "share_num_weibo" ,length = 13 ,nullable = false)
	public Integer getShareNumWeibo (){
		return this.shareNumWeibo ;
	}

	public void setShareNumWeixin (Integer shareNumWeixin){
		this.shareNumWeixin = shareNumWeixin ;
	}

	@Column(name = "share_num_weixin" ,length = 13 ,nullable = false)
	public Integer getShareNumWeixin (){
		return this.shareNumWeixin ;
	}

	public void setShareBackQq (Integer shareBackQq){
		this.shareBackQq = shareBackQq ;
	}

	@Column(name = "share_back_qq" ,length = 13 ,nullable = false)
	public Integer getShareBackQq (){
		return this.shareBackQq ;
	}

	public void setShareBackWeibo (Integer shareBackWeibo){
		this.shareBackWeibo = shareBackWeibo ;
	}

	@Column(name = "share_back_weibo" ,length = 13 ,nullable = false)
	public Integer getShareBackWeibo (){
		return this.shareBackWeibo ;
	}

	public void setShareBackWeixin (Integer shareBackWeixin){
		this.shareBackWeixin = shareBackWeixin ;
	}

	@Column(name = "share_back_weixin" ,length = 13 ,nullable = false)
	public Integer getShareBackWeixin (){
		return this.shareBackWeixin ;
	}

	public void setSaleNum (Integer saleNum){
		this.saleNum = saleNum ;
	}

	@Column(name = "sale_num" ,length = 13 ,nullable = false)
	public Integer getSaleNum (){
		return this.saleNum ;
	}

	public void setCountDate (String countDate){
		this.countDate = countDate ;
	}

	@Column(name = "count_date" ,length = 15 ,nullable = false)
	public String getCountDate (){
		return this.countDate ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

}