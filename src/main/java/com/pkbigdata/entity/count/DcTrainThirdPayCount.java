package com.pkbigdata.entity.count;
import javax.persistence.*;

/**
 * 第三方支付统计
 * @desc	使用Entity生成器生成.
 * @date 	2017/08/24
 */
@Entity
@Table(name = "dc_train_third_pay_count" ,catalog = "dc_data_count")
public class DcTrainThirdPayCount implements java.io.Serializable {
    public DcTrainThirdPayCount(){}

    private Integer id;
	private String goodsNum ;				// 商品表的id
	private String payWay ;					// 支付方式(1.支付宝2.微信支付)
	private Float payMoney ;				// 支付金额
	private Float payWithhold ;				// 支付扣款
	private Float payRate ;					// 支付税率
	private Float endMoney ;
	private Integer status ;				// 结算状态 0 为系统结算 1 为人工点了结算
	private java.util.Date time ;
	private String goodsName ;				// 商品名称
	private String goodsType ;				// 商品类型
	private String goodsCode ;				// 商品编号

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setGoodsNum (String goodsNum){
		this.goodsNum = goodsNum ;
	}

	@Column(name = "goods_num" ,length = 100 ,nullable = false)
	public String getGoodsNum (){
		return this.goodsNum ;
	}

	public void setPayWay (String payWay){
		this.payWay = payWay ;
	}

	@Column(name = "pay_way" ,length = 255 ,nullable = false)
	public String getPayWay (){
		return this.payWay ;
	}

	public void setPayMoney (Float payMoney){
		this.payMoney = payMoney ;
	}

	@Column(name = "pay_money" ,nullable = false)
	public Float getPayMoney (){
		return this.payMoney ;
	}

	public void setPayWithhold (Float payWithhold){
		this.payWithhold = payWithhold ;
	}

	@Column(name = "pay_withhold" ,nullable = false)
	public Float getPayWithhold (){
		return this.payWithhold ;
	}

	public void setPayRate (Float payRate){
		this.payRate = payRate ;
	}

	@Column(name = "pay_rate" ,nullable = false)
	public Float getPayRate (){
		return this.payRate ;
	}

	public void setEndMoney (Float endMoney){
		this.endMoney = endMoney ;
	}

	@Column(name = "end_money" ,nullable = false)
	public Float getEndMoney (){
		return this.endMoney ;
	}

	public void setStatus (Integer status){
		this.status = status ;
	}

	@Column(name = "status" ,length = 1)
	public Integer getStatus (){
		return this.status ;
	}

	public void setTime (java.util.Date time){
		this.time = time ;
	}

	@Column(name = "time" ,nullable = false)
	public java.util.Date getTime (){
		return this.time ;
	}

	public void setGoodsName (String goodsName){
		this.goodsName = goodsName ;
	}

	@Column(name = "goods_name" ,length = 255)
	public String getGoodsName (){
		return this.goodsName ;
	}

	public void setGoodsType (String goodsType){
		this.goodsType = goodsType ;
	}

	@Column(name = "goods_type" ,length = 10)
	public String getGoodsType (){
		return this.goodsType ;
	}

	public void setGoodsCode (String goodsCode){
		this.goodsCode = goodsCode ;
	}

	@Column(name = "goods_code" ,length = 255)
	public String getGoodsCode (){
		return this.goodsCode ;
	}

}