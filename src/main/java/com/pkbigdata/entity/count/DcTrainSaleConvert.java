package com.pkbigdata.entity.count;
import javax.persistence.*;

/**
 * 销售-转化信息
 * @desc	使用Entity生成器生成.
 * @date 	2017/06/12
 */
@Entity
@Table(name = "dc_train_sale_convert" ,catalog = "dc_data_count")
public class DcTrainSaleConvert implements java.io.Serializable {
    public DcTrainSaleConvert(){}

    private Integer id;
	private Integer saleVolumn ;			// 当天的出售量
	private Integer gainBuyTwotimes ;		// 累计的2次购买人数
	private Integer gainBuyThreetimes ;		// 累计的三次购买人数
	private java.sql.Timestamp createTime ;	// 创建时间
	private String countDate ;				// 天

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setSaleVolumn (Integer saleVolumn){
		this.saleVolumn = saleVolumn ;
	}

	@Column(name = "sale_volumn" ,length = 13 ,nullable = false)
	public Integer getSaleVolumn (){
		return this.saleVolumn ;
	}

	public void setGainBuyTwotimes (Integer gainBuyTwotimes){
		this.gainBuyTwotimes = gainBuyTwotimes ;
	}

	@Column(name = "gain_buy_twotimes" ,length = 13 ,nullable = false)
	public Integer getGainBuyTwotimes (){
		return this.gainBuyTwotimes ;
	}

	public void setGainBuyThreetimes (Integer gainBuyThreetimes){
		this.gainBuyThreetimes = gainBuyThreetimes ;
	}

	@Column(name = "gain_buy_threetimes" ,length = 13 ,nullable = false)
	public Integer getGainBuyThreetimes (){
		return this.gainBuyThreetimes ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setCountDate (String countDate){
		this.countDate = countDate ;
	}

	@Column(name = "count_date" ,length = 20 ,nullable = false ,unique = true)
	public String getCountDate (){
		return this.countDate ;
	}

}