package com.pkbigdata.entity.count;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/08/14
 */
@Entity
@Table(name = "dc_train_flow_count" ,catalog = "dc_data_count")
public class DcTrainFlowCount implements java.io.Serializable {
    public DcTrainFlowCount(){}

    private Integer id;
	private Integer userNewnum ;			// 新增用户
	private Integer userLoginnum ;			// 当天用户登录数, 只有官方才有
	private Integer saleVolumn ;			// 该渠道的出售量
	private Integer ipNum ;					// ip数
	private Integer pvNum ;					// pv数
	private Integer uvNum ;					// uv数
	private String promotionSource ;		// 推广渠道名称，如为官网，则为null
	private String date ;					// 数据的日期
	private java.sql.Timestamp createTime ;	// 本条记录添加日期

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserNewnum (Integer userNewnum){
		this.userNewnum = userNewnum ;
	}

	@Column(name = "user_newnum" ,length = 13 ,nullable = false)
	public Integer getUserNewnum (){
		return this.userNewnum ;
	}

	public void setUserLoginnum (Integer userLoginnum){
		this.userLoginnum = userLoginnum ;
	}

	@Column(name = "user_loginnum" ,length = 13)
	public Integer getUserLoginnum (){
		return this.userLoginnum ;
	}

	public void setSaleVolumn (Integer saleVolumn){
		this.saleVolumn = saleVolumn ;
	}

	@Column(name = "sale_volumn" ,length = 13)
	public Integer getSaleVolumn (){
		return this.saleVolumn ;
	}

	public void setIpNum (Integer ipNum){
		this.ipNum = ipNum ;
	}

	@Column(name = "ip_num" ,length = 13 ,nullable = false)
	public Integer getIpNum (){
		return this.ipNum ;
	}

	public void setPvNum (Integer pvNum){
		this.pvNum = pvNum ;
	}

	@Column(name = "pv_num" ,length = 13 ,nullable = false)
	public Integer getPvNum (){
		return this.pvNum ;
	}

	public void setUvNum (Integer uvNum){
		this.uvNum = uvNum ;
	}

	@Column(name = "uv_num" ,length = 13 ,nullable = false)
	public Integer getUvNum (){
		return this.uvNum ;
	}

	public void setPromotionSource (String promotionSource){
		this.promotionSource = promotionSource ;
	}

	@Column(name = "promotion_source" ,length = 255)
	public String getPromotionSource (){
		return this.promotionSource ;
	}

	public void setDate (String date){
		this.date = date ;
	}

	@Column(name = "date" ,length = 12 ,nullable = false)
	public String getDate (){
		return this.date ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

}