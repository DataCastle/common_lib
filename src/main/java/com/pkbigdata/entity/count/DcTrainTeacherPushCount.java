package com.pkbigdata.entity.count;
import javax.persistence.*;

/**
 * 导师提成统计
 * @desc	使用Entity生成器生成.
 * @date 	2017/05/24
 */
@Entity
@Table(name = "dc_train_teacher_push_count" ,catalog = "dc_data_count")
public class DcTrainTeacherPushCount implements java.io.Serializable {
    public DcTrainTeacherPushCount(){}

    private Integer id;
	private String teacherNum ;				// 导师id
	private String teacherName;//导师名
	private Float saleMoney ;//销售金额
	private Integer saleNum ;//销售数量
	private String courseNum ;//课程id
	private String goodsNum ;//商品id
	private String goodsName;//商品名称
	private Float ratio ;					// 提成比例
	private Float rate ;//提成率
	private Float shareMoney ;//分摊后销售额
	private Float pushMoney ;//提成额
	private java.sql.Date time ;//时间
	private Integer status;//结算状态
	private String goodsType;//商品类型
	private String goodsCode;//商品编号
	private String teacherCode;//导师编号

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setTeacherNum (String teacherNum){
		this.teacherNum = teacherNum ;
	}

	@Column(name = "teacher_num" ,length = 32 ,nullable = false)
	public String getTeacherNum (){
		return this.teacherNum ;
	}

	public void setSaleMoney (Float saleMoney){
		this.saleMoney = saleMoney ;
	}

	@Column(name = "sale_money" ,nullable = false)
	public Float getSaleMoney (){
		return this.saleMoney ;
	}

	public void setSaleNum (Integer saleNum){
		this.saleNum = saleNum ;
	}

	@Column(name = "sale_num" ,length = 11 ,nullable = false)
	public Integer getSaleNum (){
		return this.saleNum ;
	}

	public void setCourseNum (String courseNum){
		this.courseNum = courseNum ;
	}

	@Column(name = "course_num" ,length = 20 ,nullable = false)
	public String getCourseNum (){
		return this.courseNum ;
	}

	public void setGoodsNum (String goodsNum){
		this.goodsNum = goodsNum ;
	}

	@Column(name = "goods_num" ,length = 100 ,nullable = false)
	public String getGoodsNum (){
		return this.goodsNum ;
	}

	public void setRatio (Float ratio){
		this.ratio = ratio ;
	}

	@Column(name = "ratio" ,nullable = false)
	public Float getRatio (){
		return this.ratio ;
	}

	public void setRate (Float rate){
		this.rate = rate ;
	}

	@Column(name = "rate" ,nullable = false)
	public Float getRate (){
		return this.rate ;
	}

	public void setShareMoney (Float shareMoney){
		this.shareMoney = shareMoney ;
	}

	@Column(name = "share_money" ,nullable = false)
	public Float getShareMoney (){
		return this.shareMoney ;
	}

	public void setPushMoney (Float pushMoney){
		this.pushMoney = pushMoney ;
	}

	@Column(name = "push_money" ,nullable = false)
	public Float getPushMoney (){
		return this.pushMoney ;
	}

	public void setTime (java.sql.Date time){
		this.time = time ;
	}

	@Column(name = "time" ,nullable = false)
	public java.sql.Date getTime (){
		return this.time ;
	}

	@Column(name="status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name = "teacher_name",nullable = false)
	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	@Column(name = "goods_name",nullable = false)
	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	@Column(name = "goods_type")
	public String getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}

	@Column(name = "goods_code")
	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	@Column(name = "teacher_code")
	public String getTeacherCode() {
		return teacherCode;
	}

	public void setTeacherCode(String teacherCode) {
		this.teacherCode = teacherCode;
	}
}