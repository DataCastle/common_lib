package com.pkbigdata.entity.count;
import javax.persistence.*;

/**
 * 首页销售统计
 * @desc	使用Entity生成器生成.
 * @date 	2017/05/24
 */
@Entity
@Table(name = "dc_train_sale_count" ,catalog = "dc_data_count")
public class DcTrainSaleCount implements java.io.Serializable {
    public DcTrainSaleCount(){}

    private Integer id;
	private Float saleMoney ;				// 销售额
	private Float cost ;					// 消耗成本
	private Float grossProfit ;				// 毛利润
	private Float grossProfitRate ;			// 毛利率
	private java.sql.Date time ;			// 日期
	private Integer status;//状态

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setSaleMoney (Float saleMoney){
		this.saleMoney = saleMoney ;
	}

	@Column(name = "sale_money" ,nullable = false)
	public Float getSaleMoney (){
		return this.saleMoney ;
	}

	public void setCost (Float cost){
		this.cost = cost ;
	}

	@Column(name = "cost" ,nullable = false)
	public Float getCost (){
		return this.cost ;
	}

	public void setGrossProfit (Float grossProfit){
		this.grossProfit = grossProfit ;
	}

	@Column(name = "gross_profit" ,nullable = false)
	public Float getGrossProfit (){
		return this.grossProfit ;
	}

	public void setGrossProfitRate (Float grossProfitRate){
		this.grossProfitRate = grossProfitRate ;
	}

	@Column(name = "gross_profit_rate" ,nullable = false)
	public Float getGrossProfitRate (){
		return this.grossProfitRate ;
	}

	public void setTime (java.sql.Date time){
		this.time = time ;
	}

	@Column(name = "time" ,nullable = false)
	public java.sql.Date getTime (){
		return this.time ;
	}

	@Column(name = "status",nullable = false,length = 1)
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}