package com.pkbigdata.entity.count;
import javax.persistence.*;

/**
 * 码支付统计表
 * @desc	使用Entity生成器生成.
 * @date 	2017/08/24
 */
@Entity
@Table(name = "dc_train_code_pay_count" ,catalog = "dc_data_count")
public class DcTrainCodePayCount implements java.io.Serializable {
    public DcTrainCodePayCount(){}

    private Integer id;
	private String goodsId ;				// 商品表id
	private Float price ;					// 码对应的价格
	private String countDate ;				// 结算的天
	private Integer status ;				// 结算状态 0为系统结算（看作未结算） 1为人工点击结算
	private String goodsName ;
	private String goodsType ;				// g
	private String goodsCode ;				// 商品编号

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setGoodsId (String goodsId){
		this.goodsId = goodsId ;
	}

	@Column(name = "goods_id" ,length = 100 ,nullable = false)
	public String getGoodsId (){
		return this.goodsId ;
	}

	public void setPrice (Float price){
		this.price = price ;
	}

	@Column(name = "price" ,nullable = false)
	public Float getPrice (){
		return this.price ;
	}

	public void setCountDate (String countDate){
		this.countDate = countDate ;
	}

	@Column(name = "count_date" ,length = 10 ,nullable = false)
	public String getCountDate (){
		return this.countDate ;
	}

	public void setStatus (Integer status){
		this.status = status ;
	}

	@Column(name = "status" ,length = 1)
	public Integer getStatus (){
		return this.status ;
	}

	public void setGoodsName (String goodsName){
		this.goodsName = goodsName ;
	}

	@Column(name = "goods_name" ,length = 255)
	public String getGoodsName (){
		return this.goodsName ;
	}

	public void setGoodsType (String goodsType){
		this.goodsType = goodsType ;
	}

	@Column(name = "goods_type" ,length = 10)
	public String getGoodsType (){
		return this.goodsType ;
	}

	public void setGoodsCode (String goodsCode){
		this.goodsCode = goodsCode ;
	}

	@Column(name = "goods_code" ,length = 255)
	public String getGoodsCode (){
		return this.goodsCode ;
	}

}