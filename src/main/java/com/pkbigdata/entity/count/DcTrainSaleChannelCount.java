package com.pkbigdata.entity.count;
import javax.persistence.*;

/**
 * 销售渠道管理统计
 * @desc	使用Entity生成器生成.
 * @date 	2017/05/24
 */
@Entity
@Table(name = "dc_train_sale_channel_count" ,catalog = "dc_data_count")
public class DcTrainSaleChannelCount implements java.io.Serializable {
    public DcTrainSaleChannelCount(){}

    private Integer id;
	private String goodsNum ;				// 商品id
	private String goodsName;//商品名称
	private Integer saleNum ;				// 销售数量
	private Float saleMoney ;				// 销售金额
	private String channelType ;			// 一级渠道名称(1.线上2.线下)
	private String channelOrganization ;	//  单位，如：电子科大、四川大学
	private  String channelOrganizationNum;//单位编号
	private String channelName ;			// (1.某某人2.某某店)
	private String channelNum;//渠道编号
	private String channelTypeNum;//类型编号
	private Float channelRate ;//提成率
	private Integer channelId;//渠道id
	private Float rateMoney;//提成额
	private String channelArea;//渠道区域
	private Integer status ;				// 结算状态
	private java.util.Date time ;			// 日期
	private String goodsType;//商品类型
	private String goodsCode;//商品编号

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setGoodsNum (String goodsNum){
		this.goodsNum = goodsNum ;
	}

	@Column(name = "goods_num" ,length = 32 ,nullable = false)
	public String getGoodsNum (){
		return this.goodsNum ;
	}

	public void setSaleNum (Integer saleNum){
		this.saleNum = saleNum ;
	}

	@Column(name = "sale_num" ,length = 11 ,nullable = false)
	public Integer getSaleNum (){
		return this.saleNum ;
	}

	public void setSaleMoney (Float saleMoney){
		this.saleMoney = saleMoney ;
	}

	@Column(name = "sale_money" ,nullable = false)
	public Float getSaleMoney (){
		return this.saleMoney ;
	}

	public void setChannelType (String channelType){
		this.channelType = channelType ;
	}

	@Column(name = "channel_type" ,length = 255 ,nullable = false)
	public String getChannelType (){
		return this.channelType ;
	}

	public void setChannelOrganization (String channelOrganization){
		this.channelOrganization = channelOrganization ;
	}

	@Column(name = "channel_organization" ,length = 255 ,nullable = false)
	public String getChannelOrganization (){
		return this.channelOrganization ;
	}

	public void setChannelName (String channelName){
		this.channelName = channelName ;
	}

	@Column(name = "channel_name" ,length = 255 ,nullable = false)
	public String getChannelName (){
		return this.channelName ;
	}

	public void setChannelRate (Float channelRate){
		this.channelRate = channelRate ;
	}

	@Column(name = "channel_rate" ,nullable = false)
	public Float getChannelRate (){
		return this.channelRate ;
	}

	public void setStatus (Integer status){
		this.status = status ;
	}

	@Column(name = "status" ,length = 1 ,nullable = false)
	public Integer getStatus (){
		return this.status ;
	}

	public void setTime (java.util.Date time){
		this.time = time ;
	}

	@Column(name = "time" ,nullable = false)
	public java.util.Date getTime (){
		return this.time ;
	}

	@Column(name = "rate_money",nullable = false)
	public Float getRateMoney() {
		return rateMoney;
	}

	public void setRateMoney(Float rateMoney) {
		this.rateMoney = rateMoney;
	}

	@Column(name="channel_num")
	public String getChannelNum() {
		return channelNum;
	}

	public void setChannelNum(String channelNum) {
		this.channelNum = channelNum;
	}

	@Column(name = "channel_id")
	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	@Column(name = "channel_area")
	public String getChannelArea() {
		return channelArea;
	}

	public void setChannelArea(String channelArea) {
		this.channelArea = channelArea;
	}

	@Column(name = "channel_organization_num",nullable = false)
	public String getChannelOrganizationNum() {
		return channelOrganizationNum;
	}

	public void setChannelOrganizationNum(String channelOrganizationNum) {
		this.channelOrganizationNum = channelOrganizationNum;
	}

	@Column(name = "channel_type_num",nullable = false)
	public String getChannelTypeNum() {
		return channelTypeNum;
	}

	public void setChannelTypeNum(String channelTypeNum) {
		this.channelTypeNum = channelTypeNum;
	}

	@Column(name = "goods_name",nullable = false)
	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	@Column(name="goods_type")
	public String getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}

	@Column(name = "goods_code")
	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}
}