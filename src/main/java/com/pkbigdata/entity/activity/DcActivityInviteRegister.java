package com.pkbigdata.entity.activity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by 李龙飞 on 2015-08-27.
 */
@Entity
@Table(name = "dc_activity_invite_register", catalog = "dc_activity")
public class DcActivityInviteRegister implements java.io.Serializable{
    private int id;//邀请注册记录id
    private int fromId;//邀请人id
    private Integer belongId;//所属活动id
    private String inviteEmail;//受邀邮箱
    private Integer toId;//受邀人id
    private Timestamp inviteTime;//邀请时间
    private Timestamp acceptTime;//接受邀请时间
    private Timestamp activeTime;//账号激活时间
    private Boolean isActive;//账号是否激活
    private String inviteCode;//邀请码
    private Boolean isAccept;//是否接受邀请

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "from_id", nullable = false, insertable = true, updatable = true)
    public int getFromId() {
        return fromId;
    }

    public void setFromId(int fromId) {
        this.fromId = fromId;
    }

    @Basic
    @Column(name = "belong_id", nullable = true, insertable = true, updatable = true)
    public Integer getBelongId() {
        return belongId;
    }

    public void setBelongId(Integer belongId) {
        this.belongId = belongId;
    }

    @Basic
    @Column(name = "invite_email", nullable = false, insertable = true, updatable = true, length = 255)
    public String getInviteEmail() {
        return inviteEmail;
    }

    public void setInviteEmail(String inviteEmail) {
        this.inviteEmail = inviteEmail;
    }

    @Basic
    @Column(name = "to_id", nullable = true, insertable = true, updatable = true)
    public Integer getToId() {
        return toId;
    }

    public void setToId(Integer toId) {
        this.toId = toId;
    }

    @Basic
    @Column(name = "invite_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getInviteTime() {
        return inviteTime;
    }

    public void setInviteTime(Timestamp inviteTime) {
        this.inviteTime = inviteTime;
    }

    @Basic
    @Column(name = "accept_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getAcceptTime() {
        return acceptTime;
    }

    public void setAcceptTime(Timestamp acceptTime) {
        this.acceptTime = acceptTime;
    }

    @Basic
    @Column(name = "active_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(Timestamp activeTime) {
        this.activeTime = activeTime;
    }

    @Basic
    @Column(name = "is_active", nullable = true, insertable = true, updatable = true)
    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    @Basic
    @Column(name = "invite_code", nullable = true, insertable = true, updatable = true, length = 255)
    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    @Basic
    @Column(name = "is_accept", nullable = true, insertable = true, updatable = true)
    public Boolean getIsAccept() {
        return isAccept;
    }

    public void setIsAccept(Boolean isAccept) {
        this.isAccept = isAccept;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DcActivityInviteRegister that = (DcActivityInviteRegister) o;

        if (id != that.id) return false;
        if (fromId != that.fromId) return false;
        if (belongId != null ? !belongId.equals(that.belongId) : that.belongId != null) return false;
        if (inviteEmail != null ? !inviteEmail.equals(that.inviteEmail) : that.inviteEmail != null) return false;
        if (toId != null ? !toId.equals(that.toId) : that.toId != null) return false;
        if (inviteTime != null ? !inviteTime.equals(that.inviteTime) : that.inviteTime != null) return false;
        if (acceptTime != null ? !acceptTime.equals(that.acceptTime) : that.acceptTime != null) return false;
        if (activeTime != null ? !activeTime.equals(that.activeTime) : that.activeTime != null) return false;
        if (isActive != null ? !isActive.equals(that.isActive) : that.isActive != null) return false;
        if (inviteCode != null ? !inviteCode.equals(that.inviteCode) : that.inviteCode != null) return false;
        if (isAccept != null ? !isAccept.equals(that.isAccept) : that.isAccept != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + fromId;
        result = 31 * result + (belongId != null ? belongId.hashCode() : 0);
        result = 31 * result + (inviteEmail != null ? inviteEmail.hashCode() : 0);
        result = 31 * result + (toId != null ? toId.hashCode() : 0);
        result = 31 * result + (inviteTime != null ? inviteTime.hashCode() : 0);
        result = 31 * result + (acceptTime != null ? acceptTime.hashCode() : 0);
        result = 31 * result + (activeTime != null ? activeTime.hashCode() : 0);
        result = 31 * result + (isActive != null ? isActive.hashCode() : 0);
        result = 31 * result + (inviteCode != null ? inviteCode.hashCode() : 0);
        result = 31 * result + (isAccept != null ? isAccept.hashCode() : 0);
        return result;
    }
}
