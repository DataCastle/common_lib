package com.pkbigdata.entity.activity;

import javax.persistence.*;

/**
 * Created by 李龙飞 on 2015-08-27.
 */
@Entity
//@OptimisticLocking(type=OptimisticLockType.ALL)
@Table(name = "dc_activity_prize", catalog = "dc_activity")
public class DcActivityPrize implements java.io.Serializable{
    private int id;//奖品id
    private int belongId;//所属活动id
    private String name;//奖品名字
    private String property;//奖品属性
    private String introduce;//奖品介绍
    private float threshhold;//获奖阈值
    private String requireInfo;//获奖要求介绍
    private Integer num;//奖品数量
    private Integer consumeNum;//被领数量

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "belong_id", nullable = false, insertable = true, updatable = true)
    public int getBelongId() {
        return belongId;
    }

    public void setBelongId(int belongId) {
        this.belongId = belongId;
    }

    @Basic
    @Column(name = "name", nullable = true, insertable = true, updatable = true, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "property", nullable = true, insertable = true, updatable = true, length = 255)
    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    @Basic
    @Column(name = "introduce", nullable = true, insertable = true, updatable = true, length = 255)
    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    @Basic
    @Column(name = "threshhold", nullable = false, insertable = true, updatable = true, precision = 0)
    public float getThreshhold() {
        return threshhold;
    }

    public void setThreshhold(float threshhold) {
        this.threshhold = threshhold;
    }

    @Basic
    @Column(name = "require_info", nullable = true, insertable = true, updatable = true, length = 255)
    public String getRequireInfo() {
        return requireInfo;
    }

    public void setRequireInfo(String requireInfo) {
        this.requireInfo = requireInfo;
    }

    @Basic
    @Column(name = "num", nullable = true, insertable = true, updatable = true)
    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    @Basic
    @Column(name = "consume_num", nullable = true, insertable = true, updatable = true)
    public Integer getConsumeNum() {
        return consumeNum;
    }

    public void setConsumeNum(Integer consumeNum) {
        this.consumeNum = consumeNum;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DcActivityPrize that = (DcActivityPrize) o;

        if (id != that.id) return false;
        if (belongId != that.belongId) return false;
        if (Float.compare(that.threshhold, threshhold) != 0) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (property != null ? !property.equals(that.property) : that.property != null) return false;
        if (introduce != null ? !introduce.equals(that.introduce) : that.introduce != null) return false;
        if (requireInfo != null ? !requireInfo.equals(that.requireInfo) : that.requireInfo != null) return false;
        if (num != null ? !num.equals(that.num) : that.num != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + belongId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (property != null ? property.hashCode() : 0);
        result = 31 * result + (introduce != null ? introduce.hashCode() : 0);
        result = 31 * result + (threshhold != +0.0f ? Float.floatToIntBits(threshhold) : 0);
        result = 31 * result + (requireInfo != null ? requireInfo.hashCode() : 0);
        result = 31 * result + (num != null ? num.hashCode() : 0);
        return result;
    }
}
