package com.pkbigdata.entity.activity;



import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ck on 2015-08-27.
 */
@Entity
@Table(name = "dc_activity_join", catalog = "dc_activity")
public class DcActivityJoin implements java.io.Serializable{
    private int id;//参加活动id
    private int userId;//参加用户id
    private int activityId;//活动id
    private Boolean status;//部落、联盟
    private Integer prizeId;//奖品id
    private Boolean isGet;//是否得到奖品
    private Timestamp joinTime;//加入时间
    private Timestamp getTime;//获得时间
    private Integer inviteNum;//邀请人数
    private String winTeamIds;//觉得得奖的队伍ids
    private Integer favoriteTeamId;//最喜欢的队伍id

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_id", nullable = false, insertable = true, updatable = true)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "activity_id", nullable = false, insertable = true, updatable = true)
    public int getActivityId() {
        return activityId;
    }

    public void setActivityId(int activityId) {
        this.activityId = activityId;
    }

    @Basic
    @Column(name = "status", nullable = true, insertable = true, updatable = true)
    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Basic
    @Column(name = "prize_id", nullable = false, insertable = true, updatable = true)
    public Integer getPrizeId() {
        return prizeId;
    }

    public void setPrizeId(Integer prizeId) {
        this.prizeId = prizeId;
    }

    @Basic
    @Column(name = "is_get", nullable = true, insertable = true, updatable = true)
    public Boolean getIsGet() {
        return isGet;
    }

    public void setIsGet(Boolean isGet) {
        this.isGet = isGet;
    }

    @Basic
    @Column(name = "join_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(Timestamp joinTime) {
        this.joinTime = joinTime;
    }

    @Basic
    @Column(name = "get_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getGetTime() {
        return getTime;
    }

    public void setGetTime(Timestamp getTime) {
        this.getTime = getTime;
    }

    @Basic
    @Column(name = "invite_num", nullable = true, insertable = true, updatable = true)
    public Integer getInviteNum() {
        return inviteNum;
    }

    public void setInviteNum(Integer inviteNum) {
        this.inviteNum = inviteNum;
    }


    @Basic
    @Column(name = "win_team_ids", nullable = true, insertable = true, updatable = true)
    public String getWinTeamIds() {
        return winTeamIds;
    }

    public void setWinTeamIds(String winTeamIds) {
        this.winTeamIds = winTeamIds;
    }

    @Basic
    @Column(name = "favorite_team_id", nullable = true, insertable = true, updatable = true)
    public Integer getFavoriteTeamId() {
        return favoriteTeamId;
    }

    public void setFavoriteTeamId(Integer favoriteTeamId) {
        this.favoriteTeamId = favoriteTeamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DcActivityJoin that = (DcActivityJoin) o;

        if (id != that.id) return false;
        if (userId != that.userId) return false;
        if (activityId != that.activityId) return false;
        if (prizeId != that.prizeId) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (isGet != null ? !isGet.equals(that.isGet) : that.isGet != null) return false;
        if (joinTime != null ? !joinTime.equals(that.joinTime) : that.joinTime != null) return false;
        if (getTime != null ? !getTime.equals(that.getTime) : that.getTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + userId;
        result = 31 * result + activityId;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + prizeId;
        result = 31 * result + (isGet != null ? isGet.hashCode() : 0);
        result = 31 * result + (joinTime != null ? joinTime.hashCode() : 0);
        result = 31 * result + (getTime != null ? getTime.hashCode() : 0);
        return result;
    }
}
