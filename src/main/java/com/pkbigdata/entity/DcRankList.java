package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;


/**
 * DcRankList entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="dc_rank_list"
    ,catalog="dc"
)

public class DcRankList  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String name;//排行榜名称
     private Integer cmptId;//竞赛名称
     private String type;//排行榜类别(用于算法类竞赛)
     private Timestamp time;//建立排行榜的时间
     private String hql;//得到排行榜的hql语句


    // Constructors

    /** default constructor */
    public DcRankList() {
    }

    
    /** full constructor */
    public DcRankList(String name, Integer cmptId, String type, Timestamp time, String hql) {
        this.name = name;
        this.cmptId = cmptId;
        this.type = type;
        this.time = time;
        this.hql = hql;
    }

   
    // Property accessors
    @Id @GeneratedValue(strategy=IDENTITY)
    
    @Column(name="id", unique=true, nullable=false)

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(name="name")

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    @Column(name="cmpt_id")

    public Integer getCmptId() {
        return this.cmptId;
    }
    
    public void setCmptId(Integer cmptId) {
        this.cmptId = cmptId;
    }
    
    @Column(name="type")

    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    @Column(name="time", length=19)

    public Timestamp getTime() {
        return this.time;
    }
    
    public void setTime(Timestamp time) {
        this.time = time;
    }
    
    @Column(name="hql", length=65535)

    public String getHql() {
        return this.hql;
    }
    
    public void setHql(String hql) {
        this.hql = hql;
    }
   








}