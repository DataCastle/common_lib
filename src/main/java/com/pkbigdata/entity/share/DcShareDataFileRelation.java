package com.pkbigdata.entity.share;
import javax.persistence.*;

/**
 * 分享数据链接关系表
 * @desc	使用Entity生成器生成.
 * @date 	2017/08/04
 */
@Entity
@Table(name = "dc_share_data_file_relation" ,catalog = "dc_share")
public class DcShareDataFileRelation implements java.io.Serializable {
    public DcShareDataFileRelation(){}

    private Integer id;
	private Integer dataId ;				// 分享实体id
	private Integer fileId ;				// 文件id
	private Integer type ;					// 图片 or 资料

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setDataId (Integer dataId){
		this.dataId = dataId ;
	}

	@Column(name = "data_id" ,length = 13 ,nullable = false)
	public Integer getDataId (){
		return this.dataId ;
	}

	public void setFileId (Integer fileId){
		this.fileId = fileId ;
	}

	@Column(name = "file_id" ,length = 13 ,nullable = false)
	public Integer getFileId (){
		return this.fileId ;
	}

	public void setType (Integer type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 1 ,nullable = false)
	public Integer getType (){
		return this.type ;
	}

}