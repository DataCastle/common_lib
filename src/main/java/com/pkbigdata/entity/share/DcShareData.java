package com.pkbigdata.entity.share;
import javax.persistence.*;

/**
 * dc分享实体
 * @desc	使用Entity生成器生成.
 * @date 	2017/08/04
 */
@Entity
@Table(name = "dc_share_data" ,catalog = "dc_share")
public class DcShareData implements java.io.Serializable {
    public DcShareData(){}

    private Integer id;
	private String name ;					// 资料名字
	private String substract ;				// 摘要
	private String content ;				// 正文
	private String keywords ;				// 关键字
	private Integer type ;					// 类型
	private String typeinfo ;				// 类型说明
	private Integer visitNumber ;			// 浏览次数
	private Integer downloadNumber ;		// 下载次数
	private java.sql.Timestamp createTime ;
	private Integer status ;				// 上下线状态
	private Integer delFlag ;				// 删除
	private Integer createUserId ;			// 创建者id
	private String createUserName ;			// 创建者名字
	private String sort ;					// 排序，越小越在后面

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setName (String name){
		this.name = name ;
	}

	@Column(name = "name" ,length = 400 ,nullable = false)
	public String getName (){
		return this.name ;
	}

	public void setSubstract (String substract){
		this.substract = substract ;
	}

	@Column(name = "substract" ,length = 2000)
	public String getSubstract (){
		return this.substract ;
	}

	public void setContent (String content){
		this.content = content ;
	}

	@Column(name = "content" ,length = 4000 ,nullable = false)
	public String getContent (){
		return this.content ;
	}

	public void setKeywords (String keywords){
		this.keywords = keywords ;
	}

	@Column(name = "keywords" ,length = 1000)
	public String getKeywords (){
		return this.keywords ;
	}

	public void setType (Integer type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 1 ,nullable = false)
	public Integer getType (){
		return this.type ;
	}

	public void setTypeinfo (String typeinfo){
		this.typeinfo = typeinfo ;
	}

	@Column(name = "typeinfo" ,length = 30 ,nullable = false)
	public String getTypeinfo (){
		return this.typeinfo ;
	}

	public void setVisitNumber (Integer visitNumber){
		this.visitNumber = visitNumber ;
	}

	@Column(name = "visit_number" ,length = 13 ,nullable = false)
	public Integer getVisitNumber (){
		return this.visitNumber ;
	}

	public void setDownloadNumber (Integer downloadNumber){
		this.downloadNumber = downloadNumber ;
	}

	@Column(name = "download_number" ,length = 13 ,nullable = false)
	public Integer getDownloadNumber (){
		return this.downloadNumber ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setStatus (Integer status){
		this.status = status ;
	}

	@Column(name = "status" ,length = 1 ,nullable = false)
	public Integer getStatus (){
		return this.status ;
	}

	public void setDelFlag (Integer delFlag){
		this.delFlag = delFlag ;
	}

	@Column(name = "del_flag" ,length = 1 ,nullable = false)
	public Integer getDelFlag (){
		return this.delFlag ;
	}

	public void setCreateUserId (Integer createUserId){
		this.createUserId = createUserId ;
	}

	@Column(name = "create_user_id" ,length = 13 ,nullable = false)
	public Integer getCreateUserId (){
		return this.createUserId ;
	}

	public void setCreateUserName (String createUserName){
		this.createUserName = createUserName ;
	}

	@Column(name = "create_user_name" ,length = 255 ,nullable = false)
	public String getCreateUserName (){
		return this.createUserName ;
	}

	public void setSort (String sort){
		this.sort = sort ;
	}

	@Column(name = "sort" ,length = 255 ,nullable = false)
	public String getSort (){
		return this.sort ;
	}

}