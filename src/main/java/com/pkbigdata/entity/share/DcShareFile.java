package com.pkbigdata.entity.share;// default package

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * DcFile entity. @author MyEclipse Persistence Tools
 * 文档索引表
 */
@Entity
@Table(name = "dc_share_file", catalog = "dc_share")
public class DcShareFile implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer userId;//用户id
	private String userType;//用户类型
	private String fileType;//文件类型
	private String fileName;//文件名
	private String filePath;//文件路径
	private Timestamp commitTime;//文件提交时间
	private String info;//文件描述

	// Constructors

	/** default constructor */
	public DcShareFile() {
	}

	/** minimal constructor */
	public DcShareFile(Integer userId) {
		this.userId = userId;
	}

	/** full constructor */
	public DcShareFile(Integer userId, String userType, String fileType,
                       String fileName, String filePath, Timestamp commitTime, String info) {
		this.userId = userId;
		this.userType = userType;
		this.fileType = fileType;
		this.fileName = fileName;
		this.filePath = filePath;
		this.commitTime = commitTime;
		this.info = info;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "user_id", nullable = false)
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "user_type", length = 20)
	public String getUserType() {
		return this.userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	@Column(name = "file_type", length = 20)
	public String getFileType() {
		return this.fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	@Column(name = "file_name", length = 30)
	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Column(name = "file_path", length = 65535)
	public String getFilePath() {
		return this.filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Column(name = "commit_time", length = 19)
	public Timestamp getCommitTime() {
		return this.commitTime;
	}

	public void setCommitTime(Timestamp commitTime) {
		this.commitTime = commitTime;
	}

	@Column(name = "info")
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

}