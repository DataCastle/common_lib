package com.pkbigdata.entity.share;
import javax.persistence.*;

/**
 * 记录dcShare具体页面浏览的表格
 * @desc	使用Entity生成器生成.
 * @date 	2017/09/01
 */
@Entity
@Table(name = "dc_share_log_visit" ,catalog = "dc_share")
public class DcShareLogVisit implements java.io.Serializable {
    public DcShareLogVisit(){}

    private Integer id;
	private String url ;					// 路径
	private Integer dataId ;				// 数据id
	private java.sql.Timestamp visitTime ;	// 浏览时间
	private Integer userId ;				// 用户id ，游客为null
	private String promotion ;				// 来自的推广渠道
	private String userAgent ;				// UA

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUrl (String url){
		this.url = url ;
	}

	@Column(name = "url" ,length = 1000 ,nullable = false)
	public String getUrl (){
		return this.url ;
	}

	public void setDataId (Integer dataId){
		this.dataId = dataId ;
	}

	@Column(name = "data_id" ,length = 13 ,nullable = false)
	public Integer getDataId (){
		return this.dataId ;
	}

	public void setVisitTime (java.sql.Timestamp visitTime){
		this.visitTime = visitTime ;
	}

	@Column(name = "visit_time" ,nullable = false)
	public java.sql.Timestamp getVisitTime (){
		return this.visitTime ;
	}

	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 13)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setPromotion (String promotion){
		this.promotion = promotion ;
	}

	@Column(name = "promotion" ,length = 500)
	public String getPromotion (){
		return this.promotion ;
	}

	public void setUserAgent (String userAgent){
		this.userAgent = userAgent ;
	}

	@Column(name = "user_agent" ,length = 1000)
	public String getUserAgent (){
		return this.userAgent ;
	}

}