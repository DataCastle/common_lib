package com.pkbigdata.entity.share;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/09/01
 */
@Entity
@Table(name = "dc_share_log" ,catalog = "dc_share")
public class DcShareLog implements java.io.Serializable {
    public DcShareLog(){}

    private Integer id;
	private Integer userId ;
	private java.util.Date downloadTime ;
	private Integer dataId ;
	private String promotion ;				// 推广的渠道来源

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 16)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setDownloadTime (java.util.Date downloadTime){
		this.downloadTime = downloadTime ;
	}

	@Column(name = "download_time")
	public java.util.Date getDownloadTime (){
		return this.downloadTime ;
	}

	public void setDataId (Integer dataId){
		this.dataId = dataId ;
	}

	@Column(name = "data_id" ,length = 16)
	public Integer getDataId (){
		return this.dataId ;
	}

	public void setPromotion (String promotion){
		this.promotion = promotion ;
	}

	@Column(name = "promotion" ,length = 500)
	public String getPromotion (){
		return this.promotion ;
	}

}