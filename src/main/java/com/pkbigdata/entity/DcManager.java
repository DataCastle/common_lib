package com.pkbigdata.entity;// default package

import javax.persistence.*;

/**
 * DcManager entity. @author MyEclipse Persistence Tools
 * 管理员表
 */
@Entity
@Table(name = "dc_manager", catalog = "dc", uniqueConstraints = @UniqueConstraint(columnNames = "email"))
public class DcManager implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;//管理员用户名
	private String password;//管理员密码
	private Boolean super_;//是否是超级管理员
	private String email;//管理员邮箱
	private String phone;//手机号

	// Constructors

	/** default constructor */
	public DcManager() {
	}

	/** full constructor */
	public DcManager(String name, String password, Boolean super_, String email) {
		this.name = name;
		this.password = password;
		this.super_ = super_;
		this.email = email;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 40)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "password", nullable = false, length = 40)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "super", nullable = false)
	public Boolean getSuper_() {
		return this.super_;
	}

	public void setSuper_(Boolean super_) {
		this.super_ = super_;
	}

	@Column(name = "email", unique = true, nullable = false)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name="phone")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}