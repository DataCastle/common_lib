package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ck on 2017/3/23.
 *
 */
@Entity
@Table(name = "dc_message_flag",catalog = "dc")
public class DcMessageFlag  implements java.io.Serializable{

    private Integer id;
    private Integer userId;
    private Integer messageId;
    private Integer type;//(1.删除2 .彻底删除   3.重要   4.已读)
    private Timestamp createTime;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "user_id")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "message_id")
    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    @Column(name = "type")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }
}
