package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;


/**
 * DcStage entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="dc_stage"
    ,catalog="dc"
)

public class DcStage  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private Integer cmptId;
     private Timestamp start;//阶段的开始时间
     private Timestamp end;//阶段的结束时间
     private String  testFile;//测试集A
     private String name;//阶段名称
     private Integer index;//阶段排序
     private String testBFile;//测试集


    // Constructors

    /** default constructor */
    public DcStage() {
    }

    
    /** full constructor */
    public DcStage(Integer cmptId, Timestamp start, Timestamp end, String testFile) {
        this.cmptId = cmptId;
        this.start = start;
        this.end = end;
        this.testFile = testFile;
    }

   
    // Property accessors
    @Id @GeneratedValue(strategy=IDENTITY)
    
    @Column(name="id", unique=true, nullable=false)

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(name="cmpt_id")

    public Integer getCmptId() {
        return this.cmptId;
    }
    
    public void setCmptId(Integer cmptId) {
        this.cmptId = cmptId;
    }
    
    @Column(name="start", length=19)

    public Timestamp getStart() {
        return this.start;
    }
    
    public void setStart(Timestamp start) {
        this.start = start;
    }
    
    @Column(name="end", length=19)

    public Timestamp getEnd() {
        return this.end;
    }
    
    public void setEnd(Timestamp end) {
        this.end = end;
    }
    
    @Column(name="test_file")

    public String getTestFile() {
        return this.testFile;
    }
    
    public void setTestFile(String testFile) {
        this.testFile = testFile;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "\"index\"")
    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    @Column(name = "test_b_file")
    public String getTestBFile() {
        return testBFile;
    }

    public void setTestBFile(String testBFile) {
        this.testBFile = testBFile;
    }
}