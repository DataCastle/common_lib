package com.pkbigdata.entity.email;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Administrator on 2016/2/24.
 */
@Entity
@Table(name = "backup_file", catalog = "xyemail")
public class BackupFile implements java.io.Serializable{
    private int id;
    private String file;
    private String remark;
    private Timestamp time;
    private String filePath;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "file")
    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    @Basic
    @Column(name = "remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Basic
    @Column(name = "time")
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BackupFile that = (BackupFile) o;

        if (id != that.id) return false;
        if (file != null ? !file.equals(that.file) : that.file != null) return false;
        if (remark != null ? !remark.equals(that.remark) : that.remark != null) return false;
        if (time != null ? !time.equals(that.time) : that.time != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (file != null ? file.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        return result;
    }

    @Basic
    @Column(name = "file_path")
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
