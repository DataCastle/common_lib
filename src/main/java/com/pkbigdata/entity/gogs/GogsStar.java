package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "star", catalog = "gogs")
public class GogsStar implements java.io.Serializable{
    private long id;
    private Long uid;
    private Long repoId;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "uid")
    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    @Basic
    @Column(name = "repo_id")
    public Long getRepoId() {
        return repoId;
    }

    public void setRepoId(Long repoId) {
        this.repoId = repoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsStar gogsStar = (GogsStar) o;

        if (id != gogsStar.id) return false;
        if (uid != null ? !uid.equals(gogsStar.uid) : gogsStar.uid != null) return false;
        if (repoId != null ? !repoId.equals(gogsStar.repoId) : gogsStar.repoId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (uid != null ? uid.hashCode() : 0);
        result = 31 * result + (repoId != null ? repoId.hashCode() : 0);
        return result;
    }
}
