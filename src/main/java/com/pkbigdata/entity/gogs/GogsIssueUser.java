package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "issue_user", catalog = "gogs")
public class GogsIssueUser implements java.io.Serializable{
    private long id;
    private Long uid;
    private Long issueId;
    private Long repoId;
    private Long milestoneId;
    private Byte isRead;
    private Byte isAssigned;
    private Byte isMentioned;
    private Byte isPoster;
    private Byte isClosed;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "uid")
    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    @Basic
    @Column(name = "issue_id")
    public Long getIssueId() {
        return issueId;
    }

    public void setIssueId(Long issueId) {
        this.issueId = issueId;
    }

    @Basic
    @Column(name = "repo_id")
    public Long getRepoId() {
        return repoId;
    }

    public void setRepoId(Long repoId) {
        this.repoId = repoId;
    }

    @Basic
    @Column(name = "milestone_id")
    public Long getMilestoneId() {
        return milestoneId;
    }

    public void setMilestoneId(Long milestoneId) {
        this.milestoneId = milestoneId;
    }

    @Basic
    @Column(name = "is_read")
    public Byte getIsRead() {
        return isRead;
    }

    public void setIsRead(Byte isRead) {
        this.isRead = isRead;
    }

    @Basic
    @Column(name = "is_assigned")
    public Byte getIsAssigned() {
        return isAssigned;
    }

    public void setIsAssigned(Byte isAssigned) {
        this.isAssigned = isAssigned;
    }

    @Basic
    @Column(name = "is_mentioned")
    public Byte getIsMentioned() {
        return isMentioned;
    }

    public void setIsMentioned(Byte isMentioned) {
        this.isMentioned = isMentioned;
    }

    @Basic
    @Column(name = "is_poster")
    public Byte getIsPoster() {
        return isPoster;
    }

    public void setIsPoster(Byte isPoster) {
        this.isPoster = isPoster;
    }

    @Basic
    @Column(name = "is_closed")
    public Byte getIsClosed() {
        return isClosed;
    }

    public void setIsClosed(Byte isClosed) {
        this.isClosed = isClosed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsIssueUser that = (GogsIssueUser) o;

        if (id != that.id) return false;
        if (uid != null ? !uid.equals(that.uid) : that.uid != null) return false;
        if (issueId != null ? !issueId.equals(that.issueId) : that.issueId != null) return false;
        if (repoId != null ? !repoId.equals(that.repoId) : that.repoId != null) return false;
        if (milestoneId != null ? !milestoneId.equals(that.milestoneId) : that.milestoneId != null) return false;
        if (isRead != null ? !isRead.equals(that.isRead) : that.isRead != null) return false;
        if (isAssigned != null ? !isAssigned.equals(that.isAssigned) : that.isAssigned != null) return false;
        if (isMentioned != null ? !isMentioned.equals(that.isMentioned) : that.isMentioned != null) return false;
        if (isPoster != null ? !isPoster.equals(that.isPoster) : that.isPoster != null) return false;
        if (isClosed != null ? !isClosed.equals(that.isClosed) : that.isClosed != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (uid != null ? uid.hashCode() : 0);
        result = 31 * result + (issueId != null ? issueId.hashCode() : 0);
        result = 31 * result + (repoId != null ? repoId.hashCode() : 0);
        result = 31 * result + (milestoneId != null ? milestoneId.hashCode() : 0);
        result = 31 * result + (isRead != null ? isRead.hashCode() : 0);
        result = 31 * result + (isAssigned != null ? isAssigned.hashCode() : 0);
        result = 31 * result + (isMentioned != null ? isMentioned.hashCode() : 0);
        result = 31 * result + (isPoster != null ? isPoster.hashCode() : 0);
        result = 31 * result + (isClosed != null ? isClosed.hashCode() : 0);
        return result;
    }
}
