package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "issue", catalog = "gogs")
public class GogsIssue implements java.io.Serializable{
    private long id;
    private Long repoId;
    private Long index;
    private String name;
    private Long posterId;
    private Long milestoneId;
    private Long assigneeId;
    private Byte isPull;
    private Byte isClosed;
    private String content;
    private Integer priority;
    private Integer numComments;
    private Long deadlineUnix;
    private Long createdUnix;
    private Long updatedUnix;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "repo_id")
    public Long getRepoId() {
        return repoId;
    }

    public void setRepoId(Long repoId) {
        this.repoId = repoId;
    }

    @Basic
    @Column(name = "index")
    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "poster_id")
    public Long getPosterId() {
        return posterId;
    }

    public void setPosterId(Long posterId) {
        this.posterId = posterId;
    }

    @Basic
    @Column(name = "milestone_id")
    public Long getMilestoneId() {
        return milestoneId;
    }

    public void setMilestoneId(Long milestoneId) {
        this.milestoneId = milestoneId;
    }

    @Basic
    @Column(name = "assignee_id")
    public Long getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(Long assigneeId) {
        this.assigneeId = assigneeId;
    }

    @Basic
    @Column(name = "is_pull")
    public Byte getIsPull() {
        return isPull;
    }

    public void setIsPull(Byte isPull) {
        this.isPull = isPull;
    }

    @Basic
    @Column(name = "is_closed")
    public Byte getIsClosed() {
        return isClosed;
    }

    public void setIsClosed(Byte isClosed) {
        this.isClosed = isClosed;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "priority")
    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Basic
    @Column(name = "num_comments")
    public Integer getNumComments() {
        return numComments;
    }

    public void setNumComments(Integer numComments) {
        this.numComments = numComments;
    }

    @Basic
    @Column(name = "deadline_unix")
    public Long getDeadlineUnix() {
        return deadlineUnix;
    }

    public void setDeadlineUnix(Long deadlineUnix) {
        this.deadlineUnix = deadlineUnix;
    }

    @Basic
    @Column(name = "created_unix")
    public Long getCreatedUnix() {
        return createdUnix;
    }

    public void setCreatedUnix(Long createdUnix) {
        this.createdUnix = createdUnix;
    }

    @Basic
    @Column(name = "updated_unix")
    public Long getUpdatedUnix() {
        return updatedUnix;
    }

    public void setUpdatedUnix(Long updatedUnix) {
        this.updatedUnix = updatedUnix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsIssue gogsIssue = (GogsIssue) o;

        if (id != gogsIssue.id) return false;
        if (repoId != null ? !repoId.equals(gogsIssue.repoId) : gogsIssue.repoId != null) return false;
        if (index != null ? !index.equals(gogsIssue.index) : gogsIssue.index != null) return false;
        if (name != null ? !name.equals(gogsIssue.name) : gogsIssue.name != null) return false;
        if (posterId != null ? !posterId.equals(gogsIssue.posterId) : gogsIssue.posterId != null) return false;
        if (milestoneId != null ? !milestoneId.equals(gogsIssue.milestoneId) : gogsIssue.milestoneId != null)
            return false;
        if (assigneeId != null ? !assigneeId.equals(gogsIssue.assigneeId) : gogsIssue.assigneeId != null) return false;
        if (isPull != null ? !isPull.equals(gogsIssue.isPull) : gogsIssue.isPull != null) return false;
        if (isClosed != null ? !isClosed.equals(gogsIssue.isClosed) : gogsIssue.isClosed != null) return false;
        if (content != null ? !content.equals(gogsIssue.content) : gogsIssue.content != null) return false;
        if (priority != null ? !priority.equals(gogsIssue.priority) : gogsIssue.priority != null) return false;
        if (numComments != null ? !numComments.equals(gogsIssue.numComments) : gogsIssue.numComments != null)
            return false;
        if (deadlineUnix != null ? !deadlineUnix.equals(gogsIssue.deadlineUnix) : gogsIssue.deadlineUnix != null)
            return false;
        if (createdUnix != null ? !createdUnix.equals(gogsIssue.createdUnix) : gogsIssue.createdUnix != null)
            return false;
        if (updatedUnix != null ? !updatedUnix.equals(gogsIssue.updatedUnix) : gogsIssue.updatedUnix != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (repoId != null ? repoId.hashCode() : 0);
        result = 31 * result + (index != null ? index.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (posterId != null ? posterId.hashCode() : 0);
        result = 31 * result + (milestoneId != null ? milestoneId.hashCode() : 0);
        result = 31 * result + (assigneeId != null ? assigneeId.hashCode() : 0);
        result = 31 * result + (isPull != null ? isPull.hashCode() : 0);
        result = 31 * result + (isClosed != null ? isClosed.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (priority != null ? priority.hashCode() : 0);
        result = 31 * result + (numComments != null ? numComments.hashCode() : 0);
        result = 31 * result + (deadlineUnix != null ? deadlineUnix.hashCode() : 0);
        result = 31 * result + (createdUnix != null ? createdUnix.hashCode() : 0);
        result = 31 * result + (updatedUnix != null ? updatedUnix.hashCode() : 0);
        return result;
    }
}
