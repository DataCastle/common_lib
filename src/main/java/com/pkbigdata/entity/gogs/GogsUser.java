package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "user", catalog = "gogs")
public class GogsUser implements java.io.Serializable{
    private long id;
    private String lowerName;
    private String name;
    private String fullName;
    private String email;
    private String passwd;
    private Integer loginType;
    private long loginSource;
    private String loginName;
    private Integer type;
    private String location;
    private String website;
    private String rands;
    private String salt;
    private Long createdUnix;
    private Long updatedUnix;
    private Byte lastRepoVisibility;
    private int maxRepoCreation;
    private Byte isActive;
    private Byte isAdmin;
    private Byte allowGitHook;
    private Byte allowImportLocal;
    private Byte prohibitLogin;
    private String avatar;
    private String avatarEmail;
    private Byte useCustomAvatar;
    private Integer numFollowers;
    private int numFollowing;
    private Integer numStars;
    private Integer numRepos;
    private String description;
    private Integer numTeams;
    private Integer numMembers;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "lower_name")
    public String getLowerName() {
        return lowerName;
    }

    public void setLowerName(String lowerName) {
        this.lowerName = lowerName;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "full_name")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "passwd")
    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Basic
    @Column(name = "login_type")
    public Integer getLoginType() {
        return loginType;
    }

    public void setLoginType(Integer loginType) {
        this.loginType = loginType;
    }

    @Basic
    @Column(name = "login_source")
    public long getLoginSource() {
        return loginSource;
    }

    public void setLoginSource(long loginSource) {
        this.loginSource = loginSource;
    }

    @Basic
    @Column(name = "login_name")
    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @Basic
    @Column(name = "type")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Basic
    @Column(name = "location")
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Basic
    @Column(name = "website")
    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Basic
    @Column(name = "rands")
    public String getRands() {
        return rands;
    }

    public void setRands(String rands) {
        this.rands = rands;
    }

    @Basic
    @Column(name = "salt")
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Basic
    @Column(name = "created_unix")
    public Long getCreatedUnix() {
        return createdUnix;
    }

    public void setCreatedUnix(Long createdUnix) {
        this.createdUnix = createdUnix;
    }

    @Basic
    @Column(name = "updated_unix")
    public Long getUpdatedUnix() {
        return updatedUnix;
    }

    public void setUpdatedUnix(Long updatedUnix) {
        this.updatedUnix = updatedUnix;
    }

    @Basic
    @Column(name = "last_repo_visibility")
    public Byte getLastRepoVisibility() {
        return lastRepoVisibility;
    }

    public void setLastRepoVisibility(Byte lastRepoVisibility) {
        this.lastRepoVisibility = lastRepoVisibility;
    }

    @Basic
    @Column(name = "max_repo_creation")
    public int getMaxRepoCreation() {
        return maxRepoCreation;
    }

    public void setMaxRepoCreation(int maxRepoCreation) {
        this.maxRepoCreation = maxRepoCreation;
    }

    @Basic
    @Column(name = "is_active")
    public Byte getIsActive() {
        return isActive;
    }

    public void setIsActive(Byte isActive) {
        this.isActive = isActive;
    }

    @Basic
    @Column(name = "is_admin")
    public Byte getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Byte isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Basic
    @Column(name = "allow_git_hook")
    public Byte getAllowGitHook() {
        return allowGitHook;
    }

    public void setAllowGitHook(Byte allowGitHook) {
        this.allowGitHook = allowGitHook;
    }

    @Basic
    @Column(name = "allow_import_local")
    public Byte getAllowImportLocal() {
        return allowImportLocal;
    }

    public void setAllowImportLocal(Byte allowImportLocal) {
        this.allowImportLocal = allowImportLocal;
    }

    @Basic
    @Column(name = "prohibit_login")
    public Byte getProhibitLogin() {
        return prohibitLogin;
    }

    public void setProhibitLogin(Byte prohibitLogin) {
        this.prohibitLogin = prohibitLogin;
    }

    @Basic
    @Column(name = "avatar")
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Basic
    @Column(name = "avatar_email")
    public String getAvatarEmail() {
        return avatarEmail;
    }

    public void setAvatarEmail(String avatarEmail) {
        this.avatarEmail = avatarEmail;
    }

    @Basic
    @Column(name = "use_custom_avatar")
    public Byte getUseCustomAvatar() {
        return useCustomAvatar;
    }

    public void setUseCustomAvatar(Byte useCustomAvatar) {
        this.useCustomAvatar = useCustomAvatar;
    }

    @Basic
    @Column(name = "num_followers")
    public Integer getNumFollowers() {
        return numFollowers;
    }

    public void setNumFollowers(Integer numFollowers) {
        this.numFollowers = numFollowers;
    }

    @Basic
    @Column(name = "num_following")
    public int getNumFollowing() {
        return numFollowing;
    }

    public void setNumFollowing(int numFollowing) {
        this.numFollowing = numFollowing;
    }

    @Basic
    @Column(name = "num_stars")
    public Integer getNumStars() {
        return numStars;
    }

    public void setNumStars(Integer numStars) {
        this.numStars = numStars;
    }

    @Basic
    @Column(name = "num_repos")
    public Integer getNumRepos() {
        return numRepos;
    }

    public void setNumRepos(Integer numRepos) {
        this.numRepos = numRepos;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "num_teams")
    public Integer getNumTeams() {
        return numTeams;
    }

    public void setNumTeams(Integer numTeams) {
        this.numTeams = numTeams;
    }

    @Basic
    @Column(name = "num_members")
    public Integer getNumMembers() {
        return numMembers;
    }

    public void setNumMembers(Integer numMembers) {
        this.numMembers = numMembers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsUser gogsUser = (GogsUser) o;

        if (id != gogsUser.id) return false;
        if (loginSource != gogsUser.loginSource) return false;
        if (maxRepoCreation != gogsUser.maxRepoCreation) return false;
        if (numFollowing != gogsUser.numFollowing) return false;
        if (lowerName != null ? !lowerName.equals(gogsUser.lowerName) : gogsUser.lowerName != null) return false;
        if (name != null ? !name.equals(gogsUser.name) : gogsUser.name != null) return false;
        if (fullName != null ? !fullName.equals(gogsUser.fullName) : gogsUser.fullName != null) return false;
        if (email != null ? !email.equals(gogsUser.email) : gogsUser.email != null) return false;
        if (passwd != null ? !passwd.equals(gogsUser.passwd) : gogsUser.passwd != null) return false;
        if (loginType != null ? !loginType.equals(gogsUser.loginType) : gogsUser.loginType != null) return false;
        if (loginName != null ? !loginName.equals(gogsUser.loginName) : gogsUser.loginName != null) return false;
        if (type != null ? !type.equals(gogsUser.type) : gogsUser.type != null) return false;
        if (location != null ? !location.equals(gogsUser.location) : gogsUser.location != null) return false;
        if (website != null ? !website.equals(gogsUser.website) : gogsUser.website != null) return false;
        if (rands != null ? !rands.equals(gogsUser.rands) : gogsUser.rands != null) return false;
        if (salt != null ? !salt.equals(gogsUser.salt) : gogsUser.salt != null) return false;
        if (createdUnix != null ? !createdUnix.equals(gogsUser.createdUnix) : gogsUser.createdUnix != null)
            return false;
        if (updatedUnix != null ? !updatedUnix.equals(gogsUser.updatedUnix) : gogsUser.updatedUnix != null)
            return false;
        if (lastRepoVisibility != null ? !lastRepoVisibility.equals(gogsUser.lastRepoVisibility) : gogsUser.lastRepoVisibility != null)
            return false;
        if (isActive != null ? !isActive.equals(gogsUser.isActive) : gogsUser.isActive != null) return false;
        if (isAdmin != null ? !isAdmin.equals(gogsUser.isAdmin) : gogsUser.isAdmin != null) return false;
        if (allowGitHook != null ? !allowGitHook.equals(gogsUser.allowGitHook) : gogsUser.allowGitHook != null)
            return false;
        if (allowImportLocal != null ? !allowImportLocal.equals(gogsUser.allowImportLocal) : gogsUser.allowImportLocal != null)
            return false;
        if (prohibitLogin != null ? !prohibitLogin.equals(gogsUser.prohibitLogin) : gogsUser.prohibitLogin != null)
            return false;
        if (avatar != null ? !avatar.equals(gogsUser.avatar) : gogsUser.avatar != null) return false;
        if (avatarEmail != null ? !avatarEmail.equals(gogsUser.avatarEmail) : gogsUser.avatarEmail != null)
            return false;
        if (useCustomAvatar != null ? !useCustomAvatar.equals(gogsUser.useCustomAvatar) : gogsUser.useCustomAvatar != null)
            return false;
        if (numFollowers != null ? !numFollowers.equals(gogsUser.numFollowers) : gogsUser.numFollowers != null)
            return false;
        if (numStars != null ? !numStars.equals(gogsUser.numStars) : gogsUser.numStars != null) return false;
        if (numRepos != null ? !numRepos.equals(gogsUser.numRepos) : gogsUser.numRepos != null) return false;
        if (description != null ? !description.equals(gogsUser.description) : gogsUser.description != null)
            return false;
        if (numTeams != null ? !numTeams.equals(gogsUser.numTeams) : gogsUser.numTeams != null) return false;
        if (numMembers != null ? !numMembers.equals(gogsUser.numMembers) : gogsUser.numMembers != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (lowerName != null ? lowerName.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (passwd != null ? passwd.hashCode() : 0);
        result = 31 * result + (loginType != null ? loginType.hashCode() : 0);
        result = 31 * result + (int) (loginSource ^ (loginSource >>> 32));
        result = 31 * result + (loginName != null ? loginName.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (website != null ? website.hashCode() : 0);
        result = 31 * result + (rands != null ? rands.hashCode() : 0);
        result = 31 * result + (salt != null ? salt.hashCode() : 0);
        result = 31 * result + (createdUnix != null ? createdUnix.hashCode() : 0);
        result = 31 * result + (updatedUnix != null ? updatedUnix.hashCode() : 0);
        result = 31 * result + (lastRepoVisibility != null ? lastRepoVisibility.hashCode() : 0);
        result = 31 * result + maxRepoCreation;
        result = 31 * result + (isActive != null ? isActive.hashCode() : 0);
        result = 31 * result + (isAdmin != null ? isAdmin.hashCode() : 0);
        result = 31 * result + (allowGitHook != null ? allowGitHook.hashCode() : 0);
        result = 31 * result + (allowImportLocal != null ? allowImportLocal.hashCode() : 0);
        result = 31 * result + (prohibitLogin != null ? prohibitLogin.hashCode() : 0);
        result = 31 * result + (avatar != null ? avatar.hashCode() : 0);
        result = 31 * result + (avatarEmail != null ? avatarEmail.hashCode() : 0);
        result = 31 * result + (useCustomAvatar != null ? useCustomAvatar.hashCode() : 0);
        result = 31 * result + (numFollowers != null ? numFollowers.hashCode() : 0);
        result = 31 * result + numFollowing;
        result = 31 * result + (numStars != null ? numStars.hashCode() : 0);
        result = 31 * result + (numRepos != null ? numRepos.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (numTeams != null ? numTeams.hashCode() : 0);
        result = 31 * result + (numMembers != null ? numMembers.hashCode() : 0);
        return result;
    }
}
