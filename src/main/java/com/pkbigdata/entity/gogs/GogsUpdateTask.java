package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "update_task", catalog = "gogs")
public class GogsUpdateTask implements java.io.Serializable{
    private long id;
    private String uuid;
    private String refName;
    private String oldCommitId;
    private String newCommitId;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "uuid")
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Basic
    @Column(name = "ref_name")
    public String getRefName() {
        return refName;
    }

    public void setRefName(String refName) {
        this.refName = refName;
    }

    @Basic
    @Column(name = "old_commit_id")
    public String getOldCommitId() {
        return oldCommitId;
    }

    public void setOldCommitId(String oldCommitId) {
        this.oldCommitId = oldCommitId;
    }

    @Basic
    @Column(name = "new_commit_id")
    public String getNewCommitId() {
        return newCommitId;
    }

    public void setNewCommitId(String newCommitId) {
        this.newCommitId = newCommitId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsUpdateTask that = (GogsUpdateTask) o;

        if (id != that.id) return false;
        if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) return false;
        if (refName != null ? !refName.equals(that.refName) : that.refName != null) return false;
        if (oldCommitId != null ? !oldCommitId.equals(that.oldCommitId) : that.oldCommitId != null) return false;
        if (newCommitId != null ? !newCommitId.equals(that.newCommitId) : that.newCommitId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        result = 31 * result + (refName != null ? refName.hashCode() : 0);
        result = 31 * result + (oldCommitId != null ? oldCommitId.hashCode() : 0);
        result = 31 * result + (newCommitId != null ? newCommitId.hashCode() : 0);
        return result;
    }
}
