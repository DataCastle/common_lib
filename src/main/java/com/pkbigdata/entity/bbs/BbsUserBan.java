package com.pkbigdata.entity.bbs;
import javax.persistence.*;

/**
 * 用户禁言表
 * @desc	使用Entity生成器生成.
 * @date 	2017/09/27
 */
@Entity
@Table(name = "bbs_user_ban" ,catalog = "bbs")
public class BbsUserBan implements java.io.Serializable {
    public BbsUserBan(){}

    private Integer id;
	private Integer userId ;
	private java.sql.Timestamp createTime ;
	private String reason ;					// 被禁言原因
	private Integer hour ;					// 被禁言时长（小时）
	private java.sql.Timestamp endTime ;	// 解除禁言时间
	private Integer isRemove ;				// 0未解除  1解除
	private java.sql.Timestamp modifyTime ;	// 修改禁言时间
	private String modifyDesc ;				// 解除禁言备注
	private java.sql.Timestamp updateTime ;	// 更新时间

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 11 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setReason (String reason){
		this.reason = reason ;
	}

	@Column(name = "reason" ,length = 1000)
	public String getReason (){
		return this.reason ;
	}

	public void setHour (Integer hour){
		this.hour = hour ;
	}

	@Column(name = "hour" ,length = 255 ,nullable = false)
	public Integer getHour (){
		return this.hour ;
	}

	public void setEndTime (java.sql.Timestamp endTime){
		this.endTime = endTime ;
	}

	@Column(name = "end_time" ,nullable = false)
	public java.sql.Timestamp getEndTime (){
		return this.endTime ;
	}

	public void setIsRemove (Integer isRemove){
		this.isRemove = isRemove ;
	}

	@Column(name = "is_remove" ,length = 1 ,nullable = false)
	public Integer getIsRemove (){
		return this.isRemove ;
	}

	public void setModifyTime (java.sql.Timestamp modifyTime){
		this.modifyTime = modifyTime ;
	}

	@Column(name = "modify_time")
	public java.sql.Timestamp getModifyTime (){
		return this.modifyTime ;
	}

	public void setModifyDesc (String modifyDesc){
		this.modifyDesc = modifyDesc ;
	}

	@Column(name = "modify_desc" ,length = 255)
	public String getModifyDesc (){
		return this.modifyDesc ;
	}

	public void setUpdateTime (java.sql.Timestamp updateTime){
		this.updateTime = updateTime ;
	}

	@Column(name = "update_time" ,nullable = false)
	public java.sql.Timestamp getUpdateTime (){
		return this.updateTime ;
	}

}