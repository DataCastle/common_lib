package com.pkbigdata.entity.bbs;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/09/26
 */
@Entity
@Table(name = "bbs_uninterested" ,catalog = "bbs")
public class BbsUninterested implements java.io.Serializable {
    public BbsUninterested(){}

    private Integer id;
	private Integer userId ;
	private Integer tid ;
	private java.sql.Timestamp createTime ;

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 11 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setTid (Integer tid){
		this.tid = tid ;
	}

	@Column(name = "tid" ,length = 11 ,nullable = false)
	public Integer getTid (){
		return this.tid ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

}