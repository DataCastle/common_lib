package com.pkbigdata.entity.bbs;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/09/27
 */
@Entity
@Table(name = "bbs_report" ,catalog = "bbs")
public class BbsReport implements java.io.Serializable {
    public BbsReport(){}

    private Integer id;
	private Integer tid ;					// 被举报的文章或者回复的id
	private Integer tidType ;				// 1文章  2回复
	private Integer userId ;
	private String reason ;					// 原因
	private String description ;			// 备注（自定义文字）
	private java.sql.Timestamp createTime ;
	private Integer isHandle ;
	private Integer sectionId; //板块id


	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setTid (Integer tid){
		this.tid = tid ;
	}

	@Column(name = "tid" ,length = 11 ,nullable = false)
	public Integer getTid (){
		return this.tid ;
	}

	public void setTidType (Integer tidType){
		this.tidType = tidType ;
	}

	@Column(name = "tid_type" ,length = 1 ,nullable = false)
	public Integer getTidType (){
		return this.tidType ;
	}

	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 11 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setReason (String reason){
		this.reason = reason ;
	}

	@Column(name = "reason" ,length = 255 ,nullable = false)
	public String getReason (){
		return this.reason ;
	}

	public void setDescription (String description){
		this.description = description ;
	}

	@Column(name = "description" ,length = 2000 ,nullable = false)
	public String getDescription (){
		return this.description ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	public void setIsHandle (Integer isHandle){
		this.isHandle = isHandle ;
	}

	@Column(name = "is_handle" ,length = 1 ,nullable = false)
	public Integer getIsHandle (){
		return this.isHandle ;
	}

	@Column(name = "section_id" ,nullable = false)
	public Integer getSectionId() {
		return sectionId;
	}

	public void setSectionId(Integer sectionId) {
		this.sectionId = sectionId;
	}
}