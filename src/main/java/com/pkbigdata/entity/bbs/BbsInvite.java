package com.pkbigdata.entity.bbs;
import javax.persistence.*;

/**
 * 邀请表
 * @desc	使用Entity生成器生成.
 * @date 	2017/09/22
 */
@Entity
@Table(name = "bbs_invite" ,catalog = "bbs")
public class BbsInvite implements java.io.Serializable {
    public BbsInvite(){}

    private Integer id;
	private Integer userId ;				// 邀请人id
	private Integer invitedUserId ;			// 被邀请人的id
	private Integer tid ;					// 文章id
	private java.sql.Timestamp createTime ;	// 邀请时间

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 11 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setInvitedUserId (Integer invitedUserId){
		this.invitedUserId = invitedUserId ;
	}

	@Column(name = "invited_user_id" ,length = 11 ,nullable = false)
	public Integer getInvitedUserId (){
		return this.invitedUserId ;
	}

	public void setTid (Integer tid){
		this.tid = tid ;
	}

	@Column(name = "tid" ,length = 11 ,nullable = false)
	public Integer getTid (){
		return this.tid ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

}