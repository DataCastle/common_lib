package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * BbsLabel entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_label", catalog = "bbs", uniqueConstraints = @UniqueConstraint(columnNames = "name") )

public class BbsLabel implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;//标签名称
	private Timestamp inTime;
	private Integer topicCount;//使用次数

	// Constructors

	/** default constructor */
	public BbsLabel() {
	}

	/** full constructor */
	public BbsLabel(String name, Timestamp inTime, Integer topicCount) {
		this.name = name;
		this.inTime = inTime;
		this.topicCount = topicCount;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", unique = true, nullable = false)

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "in_time", nullable = false, length = 19)

	public Timestamp getInTime() {
		return this.inTime;
	}

	public void setInTime(Timestamp inTime) {
		this.inTime = inTime;
	}

	@Column(name = "topic_count", nullable = false)

	public Integer getTopicCount() {
		return this.topicCount;
	}

	public void setTopicCount(Integer topicCount) {
		this.topicCount = topicCount;
	}

}