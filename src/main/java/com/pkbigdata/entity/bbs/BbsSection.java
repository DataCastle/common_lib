package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * BbsSection entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_section", catalog = "bbs", uniqueConstraints = { @UniqueConstraint(columnNames = "tab"),
		@UniqueConstraint(columnNames = "name") })

public class BbsSection implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;//版块名称
	private String tab;//版块标签
	private Integer showStatus;//是否显示
	private Integer displayIndex;//排序
	private Integer defaultShow;//默认显示模块 0默认，1显示
	private Integer fatherNode;//父节点
	private Timestamp time;

	// Constructors

	/** default constructor */
	public BbsSection() {
	}

	/** minimal constructor */
	public BbsSection(String name, String tab, Integer showStatus, Integer displayIndex, Integer defaultShow) {
		this.name = name;
		this.tab = tab;
		this.showStatus = showStatus;
		this.displayIndex = displayIndex;
		this.defaultShow = defaultShow;
	}

	/** full constructor */
	public BbsSection(String name, String tab, Integer showStatus, Integer displayIndex, Integer defaultShow,
			Integer fatherNode, Timestamp time) {
		this.name = name;
		this.tab = tab;
		this.showStatus = showStatus;
		this.displayIndex = displayIndex;
		this.defaultShow = defaultShow;
		this.fatherNode = fatherNode;
		this.time = time;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", unique = true, nullable = false, length = 45)

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "tab", unique = true, nullable = false, length = 45)

	public String getTab() {
		return this.tab;
	}

	public void setTab(String tab) {
		this.tab = tab;
	}

	@Column(name = "show_status", nullable = false)

	public Integer getShowStatus() {
		return this.showStatus;
	}

	public void setShowStatus(Integer showStatus) {
		this.showStatus = showStatus;
	}

	@Column(name = "display_index", nullable = false)

	public Integer getDisplayIndex() {
		return this.displayIndex;
	}

	public void setDisplayIndex(Integer displayIndex) {
		this.displayIndex = displayIndex;
	}

	@Column(name = "default_show", nullable = false)

	public Integer getDefaultShow() {
		return this.defaultShow;
	}

	public void setDefaultShow(Integer defaultShow) {
		this.defaultShow = defaultShow;
	}

	@Column(name = "father_node")

	public Integer getFatherNode() {
		return this.fatherNode;
	}

	public void setFatherNode(Integer fatherNode) {
		this.fatherNode = fatherNode;
	}

	@Column(name = "time", length = 19)

	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

}