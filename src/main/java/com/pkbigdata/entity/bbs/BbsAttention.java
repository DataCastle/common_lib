package com.pkbigdata.entity.bbs;
import javax.persistence.*;

/**
 * 关注表
 * @desc	使用Entity生成器生成.
 * @date 	2017/09/22
 */
@Entity
@Table(name = "bbs_attention" ,catalog = "bbs")
public class BbsAttention implements java.io.Serializable {
    public BbsAttention(){}

    private Integer id;
	private Integer userId ;				// 用户id
	private Integer attentionUserId ;		// 关注的用户id
	private java.sql.Timestamp createTime ;	// 关注时间

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 11 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setAttentionUserId (Integer attentionUserId){
		this.attentionUserId = attentionUserId ;
	}

	@Column(name = "attention_user_id" ,length = 11 ,nullable = false)
	public Integer getAttentionUserId (){
		return this.attentionUserId ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

}