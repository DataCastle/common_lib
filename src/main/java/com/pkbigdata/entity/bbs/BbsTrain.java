package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BbsTrain entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_train", catalog = "bbs")

public class BbsTrain implements java.io.Serializable {

	// Fields

	private Integer id;
	private String title;//培训名称
	private String img;//显示图片
	private String url;//视频连接
	private Timestamp time;//时间
	private Integer readTimes;//阅读次数
	private Integer typeId;//类型id

	// Constructors

	/** default constructor */
	public BbsTrain() {
	}

	/** minimal constructor */
	public BbsTrain(String title, Integer typeId, String img, String url) {
		this.title = title;
		this.typeId = typeId;
		this.img = img;
		this.url = url;
	}

	/** full constructor */
	public BbsTrain(String title, Integer typeId, String img, String url, Timestamp time, Integer readTimes) {
		this.title = title;
		this.typeId = typeId;
		this.img = img;
		this.url = url;
		this.time = time;
		this.readTimes = readTimes;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "title", nullable = false)

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "type_id", nullable = false)

	public Integer getTypeId() {
		return this.typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	@Column(name = "img", nullable = false, length = 500)

	public String getImg() {
		return this.img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Column(name = "url", nullable = false, length = 500)

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "time", length = 19)

	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Column(name = "read_times")

	public Integer getReadTimes() {
		return this.readTimes;
	}

	public void setReadTimes(Integer readTimes) {
		this.readTimes = readTimes;
	}

}