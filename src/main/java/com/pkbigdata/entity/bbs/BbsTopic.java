package com.pkbigdata.entity.bbs;
import javax.persistence.*;

/**
 * 文章表
 * @desc	使用Entity生成器生成.
 * @date 	2017/09/29
 */
@Entity
@Table(name = "bbs_topic" ,catalog = "bbs")
public class BbsTopic implements java.io.Serializable {
    public BbsTopic(){}

    private Integer id;
	private String oldId ;
	private Integer sId ;					// 版块id
	private String title ;					// 话题标题
	private String content ;				// 话题内容
	private java.sql.Timestamp inTime ;		// 录入时间
	private java.sql.Timestamp modifyTime ;	// 修改时间
	private java.sql.Timestamp lastReplyTime ;	// 最后回复话题时间，用于排序
	private String lastReplyAuthorId ;		// 最后回复话题的用户id
	private Integer reply ;					// 回复量
	private Integer view ;					// 浏览量
	private Integer clickNum ;				// 点赞量
	private Integer trampleNum ;			// 踩数量
	private Double historyWeight ;
	private Double cacheWeight ;
	private Integer authorId ;				// 话题作者id
	private Integer reposted ;				// 1：转载 0：原创
	private String originalUrl ;			// 原文连接
	private Integer top ;					// 竞赛圈置顶   1置顶 0默认
	private Integer cmptTop ;				// 竞赛详情置顶
	private Integer type ;					// 0：普通，1:通知，2：公告
	private Integer good ;					// 1精华 0默认
	private Integer showStatus ;			// 1显示0不显示
	private Integer complexStatus ;			// 综合（0默认1综合）
	private Integer loginToView ;			// 登录可查看
	private Integer backToView ;			// 回复可查看
	private Integer isPreview ;				// 0.已发布 1.未发布(草稿)
	private Integer isdelete ;				// 是否删除
	private Integer isAdminDelete ;			// 用户删除

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setOldId (String oldId){
		this.oldId = oldId ;
	}

	@Column(name = "old_id" ,length = 255)
	public String getOldId (){
		return this.oldId ;
	}

	public void setSId (Integer sId){
		this.sId = sId ;
	}

	@Column(name = "s_id" ,length = 11 ,nullable = false)
	public Integer getSId (){
		return this.sId ;
	}

	public void setTitle (String title){
		this.title = title ;
	}

	@Column(name = "title" ,length = 255)
	public String getTitle (){
		return this.title ;
	}

	public void setContent (String content){
		this.content = content ;
	}

	@Column(name = "content")
	public String getContent (){
		return this.content ;
	}

	public void setInTime (java.sql.Timestamp inTime){
		this.inTime = inTime ;
	}

	@Column(name = "in_time" ,nullable = false)
	public java.sql.Timestamp getInTime (){
		return this.inTime ;
	}

	public void setModifyTime (java.sql.Timestamp modifyTime){
		this.modifyTime = modifyTime ;
	}

	@Column(name = "modify_time")
	public java.sql.Timestamp getModifyTime (){
		return this.modifyTime ;
	}

	public void setLastReplyTime (java.sql.Timestamp lastReplyTime){
		this.lastReplyTime = lastReplyTime ;
	}

	@Column(name = "last_reply_time")
	public java.sql.Timestamp getLastReplyTime (){
		return this.lastReplyTime ;
	}

	public void setLastReplyAuthorId (String lastReplyAuthorId){
		this.lastReplyAuthorId = lastReplyAuthorId ;
	}

	@Column(name = "last_reply_author_id" ,length = 32)
	public String getLastReplyAuthorId (){
		return this.lastReplyAuthorId ;
	}

	public void setReply (Integer reply){
		this.reply = reply ;
	}

	@Column(name = "reply" ,length = 11)
	public Integer getReply (){
		return this.reply ;
	}

	public void setView (Integer view){
		this.view = view ;
	}

	@Column(name = "view" ,length = 11)
	public Integer getView (){
		return this.view ;
	}

	public void setClickNum (Integer clickNum){
		this.clickNum = clickNum ;
	}

	@Column(name = "click_num" ,length = 11)
	public Integer getClickNum (){
		return this.clickNum ;
	}

	public void setTrampleNum (Integer trampleNum){
		this.trampleNum = trampleNum ;
	}

	@Column(name = "trample_num" ,length = 11)
	public Integer getTrampleNum (){
		return this.trampleNum ;
	}

	public void setHistoryWeight (Double historyWeight){
		this.historyWeight = historyWeight ;
	}

	@Column(name = "history_weight")
	public Double getHistoryWeight (){
		return this.historyWeight ;
	}

	public void setCacheWeight (Double cacheWeight){
		this.cacheWeight = cacheWeight ;
	}

	@Column(name = "cache_weight")
	public Double getCacheWeight (){
		return this.cacheWeight ;
	}

	public void setAuthorId (Integer authorId){
		this.authorId = authorId ;
	}

	@Column(name = "author_id" ,length = 16 ,nullable = false)
	public Integer getAuthorId (){
		return this.authorId ;
	}

	public void setReposted (Integer reposted){
		this.reposted = reposted ;
	}

	@Column(name = "reposted" ,length = 11)
	public Integer getReposted (){
		return this.reposted ;
	}

	public void setOriginalUrl (String originalUrl){
		this.originalUrl = originalUrl ;
	}

	@Column(name = "original_url" ,length = 255)
	public String getOriginalUrl (){
		return this.originalUrl ;
	}

	public void setTop (Integer top){
		this.top = top ;
	}

	@Column(name = "top" ,length = 11)
	public Integer getTop (){
		return this.top ;
	}

	public void setCmptTop (Integer cmptTop){
		this.cmptTop = cmptTop ;
	}

	@Column(name = "cmpt_top" ,length = 11)
	public Integer getCmptTop (){
		return this.cmptTop ;
	}

	public void setType (Integer type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 11)
	public Integer getType (){
		return this.type ;
	}

	public void setGood (Integer good){
		this.good = good ;
	}

	@Column(name = "good" ,length = 11)
	public Integer getGood (){
		return this.good ;
	}

	public void setShowStatus (Integer showStatus){
		this.showStatus = showStatus ;
	}

	@Column(name = "show_status" ,length = 11)
	public Integer getShowStatus (){
		return this.showStatus ;
	}

	public void setComplexStatus (Integer complexStatus){
		this.complexStatus = complexStatus ;
	}

	@Column(name = "complex_status" ,length = 11)
	public Integer getComplexStatus (){
		return this.complexStatus ;
	}

	public void setLoginToView (Integer loginToView){
		this.loginToView = loginToView ;
	}

	@Column(name = "login_to_view" ,length = 11)
	public Integer getLoginToView (){
		return this.loginToView ;
	}

	public void setBackToView (Integer backToView){
		this.backToView = backToView ;
	}

	@Column(name = "back_to_view" ,length = 11)
	public Integer getBackToView (){
		return this.backToView ;
	}

	public void setIsPreview (Integer isPreview){
		this.isPreview = isPreview ;
	}

	@Column(name = "is_preview" ,length = 1)
	public Integer getIsPreview (){
		return this.isPreview ;
	}

	public void setIsdelete (Integer isdelete){
		this.isdelete = isdelete ;
	}

	@Column(name = "isdelete" ,length = 1)
	public Integer getIsdelete (){
		return this.isdelete ;
	}

	public void setIsAdminDelete (Integer isAdminDelete){
		this.isAdminDelete = isAdminDelete ;
	}

	@Column(name = "is_admin_delete" ,length = 1)
	public Integer getIsAdminDelete (){
		return this.isAdminDelete ;
	}

}