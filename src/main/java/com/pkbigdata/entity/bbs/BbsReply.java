package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BbsReply entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_reply", catalog = "bbs")

public class BbsReply implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer tid;//贴子id
	private String content;//回复内容
	private Timestamp inTime;
	private Integer authorId;//回复人id
	private Integer best;//采纳为最佳答案 0默认，1采纳
	private Integer isdelete;//用户删除
	private Integer clickNum;//点赞次数
	private Integer trampleNum;//踩次数
	private Integer isAdminDelete;//管理员删除
	private Integer replyUserId;//回复某个用户的id

	// Constructors

	/** default constructor */
	public BbsReply() {
	}

	/** minimal constructor */
	public BbsReply(Integer tid, Timestamp inTime, Integer authorId, Integer best, Integer isdelete) {
		this.tid = tid;
		this.inTime = inTime;
		this.authorId = authorId;
		this.best = best;
		this.isdelete = isdelete;
	}

	/** full constructor */
	public BbsReply(Integer tid, String content, Timestamp inTime, Integer authorId, Integer best, Integer isdelete) {
		this.tid = tid;
		this.content = content;
		this.inTime = inTime;
		this.authorId = authorId;
		this.best = best;
		this.isdelete = isdelete;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "tid", nullable = false)

	public Integer getTid() {
		return this.tid;
	}

	public void setTid(Integer tid) {
		this.tid = tid;
	}

	@Column(name = "content")

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "in_time", nullable = false, length = 19)

	public Timestamp getInTime() {
		return this.inTime;
	}

	public void setInTime(Timestamp inTime) {
		this.inTime = inTime;
	}

	@Column(name = "author_id", nullable = false)

	public Integer getAuthorId() {
		return this.authorId;
	}

	public void setAuthorId(Integer authorId) {
		this.authorId = authorId;
	}

	@Column(name = "best", nullable = false)

	public Integer getBest() {
		return this.best;
	}

	public void setBest(Integer best) {
		this.best = best;
	}

	@Column(name = "isdelete", nullable = false)

	public Integer getIsdelete() {
		return this.isdelete;
	}

	public void setIsdelete(Integer isdelete) {
		this.isdelete = isdelete;
	}
	@Column(name = "click_num")
	public Integer getClickNum() {
		return clickNum;
	}

	public void setClickNum(Integer clickNum) {
		this.clickNum = clickNum;
	}
	@Column(name = "trample_num")
	public Integer getTrampleNum() {
		return trampleNum;
	}

	public void setTrampleNum(Integer trampleNum) {
		this.trampleNum = trampleNum;
	}

	@Column(name = "is_admin_delete")
	public Integer getIsAdminDelete() {
		return isAdminDelete;
	}

	public void setIsAdminDelete(Integer isAdminDelete) {
		this.isAdminDelete = isAdminDelete;
	}

	@Column(name = "reply_user_id")
	public Integer getReplyUserId() {
		return replyUserId;
	}

	public void setReplyUserId(Integer replyUserId) {
		this.replyUserId = replyUserId;
	}
}