package com.pkbigdata.entity.bbs;
import javax.persistence.*;

/**
 * 
 * @desc	使用Entity生成器生成.
 * @date 	2017/09/29
 */
@Entity
@Table(name = "bbs_share" ,catalog = "bbs")
public class BbsShare implements java.io.Serializable {
    public BbsShare(){}

    private Integer id;
	private Integer userId ;				// 分享人
	private Integer tid ;					// 文章id
	private String type ;					// 分享方式
	private String content;               //分享内容
	private java.sql.Timestamp createTime ;	// 分享时间

	@Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
         return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	public void setUserId (Integer userId){
		this.userId = userId ;
	}

	@Column(name = "user_id" ,length = 11 ,nullable = false)
	public Integer getUserId (){
		return this.userId ;
	}

	public void setTid (Integer tid){
		this.tid = tid ;
	}

	@Column(name = "tid" ,length = 11 ,nullable = false)
	public Integer getTid (){
		return this.tid ;
	}

	public void setType (String type){
		this.type = type ;
	}

	@Column(name = "type" ,length = 255)
	public String getType (){
		return this.type ;
	}

	public void setCreateTime (java.sql.Timestamp createTime){
		this.createTime = createTime ;
	}

	@Column(name = "create_time" ,nullable = false)
	public java.sql.Timestamp getCreateTime (){
		return this.createTime ;
	}

	@Column(name = "content")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}