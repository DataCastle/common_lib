package com.pkbigdata.entity;


import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Administrator on 2016/5/18.
 */
@Entity
@Table(name = "dc_user_complaint", catalog = "dc")
public class DcUserComplaint implements java.io.Serializable{
  private Integer id;
  private Integer userId;//投诉人Id
  private String complaintUser;//对谁投诉的用户名、手机、邮箱
  private Integer  complaintUserId;//对谁投诉的Id
  private String  imgPath;//图片路径
  private String content;//投诉内容
  private String status;//回复状态（0：未回复，1：已回复）
  private Timestamp createTime;//投诉时间
  private Timestamp updateTime;//回复时间


    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "user_id", nullable = false)
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "complaint_user", nullable = false)
    public String getComplaintUser() {
        return complaintUser;
    }

    public void setComplaintUser(String complaintUser) {
        this.complaintUser = complaintUser;
    }

    @Column(name = "complaint_user_id", nullable = false)
    public Integer getComplaintUserId() {
        return complaintUserId;
    }

    public void setComplaintUserId(Integer complaintUserId) {
        this.complaintUserId = complaintUserId;
    }

    @Column(name = "img_path")
    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    @Column(name = "content", nullable = false)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name = "status", nullable = false)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "create_time", nullable = false)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Column(name = "update_time")
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
}
