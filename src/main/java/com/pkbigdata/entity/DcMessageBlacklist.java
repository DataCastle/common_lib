package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ck on 2016/12/22.
 */
@Entity
@Table(name = "dc_message_blacklist",catalog = "dc")
public class DcMessageBlacklist implements java.io.Serializable{
    private Integer id;
    private Integer senderId;//拒绝谁发的消息
    private Integer receiverId;//拒绝人Id
    private String type;//类型("私信")
    private Timestamp operateTime;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "sender_id")
    public Integer getSenderId() {
        return senderId;
    }

    public void setSenderId(Integer senderId) {
        this.senderId = senderId;
    }

    @Column(name="receiver_id")
    public Integer getReceiverId() {
        return receiverId;
    }


    public void setReceiverId(Integer receiverId) {
        this.receiverId = receiverId;
    }

    @Column(name="type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    @Column(name="operate_time")
    public Timestamp getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(Timestamp operateTime) {
        this.operateTime = operateTime;
    }
}
