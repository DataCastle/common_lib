package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * DcAchievement entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "dc_achievement", catalog = "dc")
public class DcAchievement implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer userId;//用户id
	private Timestamp time;//成就获取时间
	private Integer cmptId;//竞赛id
	private String cmptName;//竞赛名称
	private Integer achievement;//成就映射id
	private String achievementName;//成就名称
	private String prize;//获得的奖励

	// Constructors

	/** default constructor */
	public DcAchievement() {
	}

	/** minimal constructor */
	public DcAchievement(Integer userId) {
		this.userId = userId;
	}

	/** full constructor */
	public DcAchievement(Integer userId, Timestamp time, Integer cmptId,
			String cmptName, Integer achievement, String achievementName) {
		this.userId = userId;
		this.time = time;
		this.cmptId = cmptId;
		this.cmptName = cmptName;
		this.achievement = achievement;
		this.achievementName = achievementName;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "user_id", nullable = false)
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Column(name = "cmpt_id")
	public Integer getCmptId() {
		return this.cmptId;
	}

	public void setCmptId(Integer cmptId) {
		this.cmptId = cmptId;
	}

	@Column(name = "cmpt_name")
	public String getCmptName() {
		return this.cmptName;
	}

	public void setCmptName(String cmptName) {
		this.cmptName = cmptName;
	}

	@Column(name = "achievement")
	public Integer getAchievement() {
		return this.achievement;
	}

	public void setAchievement(Integer achievement) {
		this.achievement = achievement;
	}

	@Column(name = "achievement_name")
	public String getAchievementName() {
		return this.achievementName;
	}

	public void setAchievementName(String achievementName) {
		this.achievementName = achievementName;
	}

	@Column(name = "prize")
	public String getPrize() {
		return prize;
	}

	public void setPrize(String prize) {
		this.prize = prize;
	}
}