package com.pkbigdata.entity;

import javax.persistence.*;

/**
 * Created by ck on 2016/12/6.
 */
@Entity
@Table(name = "dc_group_team",catalog = "dc")
public class DcGroupTeam implements java.io.Serializable{
    private Integer id;
    private Integer groupId;
    private Integer teamId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "group_id")
    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    @Column(name="team_id")
    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }
}
