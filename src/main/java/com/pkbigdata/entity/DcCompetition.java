package com.pkbigdata.entity;// default package

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * DcCompetition entity. @author MyEclipse Persistence Tools
 * 竞赛主表
 */
@Entity
@Table(name = "dc_competition", catalog = "dc", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
public class DcCompetition implements java.io.Serializable {

	// Fields

	private Integer id;
    private String name;//竞赛名称
    private String type;//竞赛类型
    private String icon;//首页默认图
    private String image;//竞赛图片,详情图片，非透明，详情页logo图
    private String backGround;//竞赛背景
    private String introduction;//竞赛简介
    private Timestamp startTime;//竞赛总的开始时间
    private Timestamp endTime;//竞赛总的结束时间
    private String rewardType;//奖励类型
    private String rewardTypeInfo;//奖励类型描述
    private String reward;//竞赛奖励
    private String uplaodType;//作品上传方式
    private Boolean calculated;//竞赛是否结算
    private Integer editid;//被编辑的竞赛id
    private Integer hostid;//主办方id
    private Boolean passCreate;//通过创建
    private Boolean preview;//预览
    private String privacy;//公开还是私有
    private Boolean published;//发布
    private Boolean history;//历史竞赛(主要给前端显示用)
    private Integer teams;//参与队伍总数
    private String timeProcess;//时间流程
    private String banner;//详情页banner图
    private String indexImage;//首页浮动logo图
    private Integer rankIndex;//排行榜索引，大于等于0就是越大越好，小于0就是越小越好
    private String mold;//类型(训练赛,竞技赛)
    private String level;//等级(训练赛才有:初级,中级,高级)
    private Integer competitorNum;  //参加比赛的人数
    private Integer competitorFNum; //虚拟的参加比赛的人数
    private Integer sort;       //排序字段


    // Constructors

    /** default constructor */
    public DcCompetition() {
    }

    /** minimal constructor */
    public DcCompetition(Boolean calculated, Integer hostid, String icon,
                         String image, String name, Boolean passCreate, Boolean preview,
                         String privacy, Boolean published, Integer teams, String token,
                         String type) {
        this.calculated = calculated;
        this.hostid = hostid;
        this.icon = icon;
        this.image = image;
        this.name = name;
        this.passCreate = passCreate;
        this.preview = preview;
        this.privacy = privacy;
        this.published = published;
        this.teams = teams;
        this.type = type;
    }

    /** full constructor */
    public DcCompetition(Integer algorithm, Boolean calculated,
                         String competitionRules, String dataDescription,
                         String description, Integer editid, Timestamp endTime,
                         String evaluationCriteria, Integer hostid, String icon,
                         String image, String introduction, String name, Boolean passCreate,
                         Boolean preview, String privacy, Boolean published, String reward,
                         Timestamp startTime, Integer teams, String token, String type,
                         String backGround, String rewardType, String rewardTypeInfo,
                         String uplaodType) {

        this.calculated = calculated;
        this.editid = editid;
        this.endTime = endTime;
        this.hostid = hostid;
        this.icon = icon;
        this.image = image;
        this.introduction = introduction;
        this.name = name;
        this.passCreate = passCreate;
        this.preview = preview;
        this.privacy = privacy;
        this.published = published;
        this.reward = reward;
        this.startTime = startTime;
        this.teams = teams;
        this.type = type;
        this.backGround = backGround;
        this.rewardType = rewardType;
        this.rewardTypeInfo = rewardTypeInfo;
        this.uplaodType = uplaodType;
    }

    // Property accessors
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    @Column(name = "calculated", nullable = false)
    public Boolean getCalculated() {
        return this.calculated;
    }

    public void setCalculated(Boolean calculated) {
        this.calculated = calculated;
    }

    @Column(name = "editid")
    public Integer getEditid() {
        return this.editid;
    }

    public void setEditid(Integer editid) {
        this.editid = editid;
    }

    @Column(name = "end_time", length = 19)
    public Timestamp getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }



    @Column(name = "hostid", nullable = false)
    public Integer getHostid() {
        return this.hostid;
    }

    public void setHostid(Integer hostid) {
        this.hostid = hostid;
    }

    @Column(name = "icon", nullable = false)
    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Column(name = "image", nullable = false)
    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Column(name = "introduction")
    public String getIntroduction() {
        return this.introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    @Column(name = "name", unique = true, nullable = false, length = 64)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "passCreate", nullable = false)
    public Boolean getPassCreate() {
        return this.passCreate;
    }

    public void setPassCreate(Boolean passCreate) {
        this.passCreate = passCreate;
    }

    @Column(name = "preview", nullable = false)
    public Boolean getPreview() {
        return this.preview;
    }

    public void setPreview(Boolean preview) {
        this.preview = preview;
    }

    @Column(name = "privacy", nullable = false, length = 16)
    public String getPrivacy() {
        return this.privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    @Column(name = "published", nullable = false)
    public Boolean getPublished() {
        return this.published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    @Column(name = "reward", length = 16)
    public String getReward() {
        return this.reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    @Column(name = "start_time", length = 19)
    public Timestamp getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    @Column(name = "teams", nullable = false)
    public Integer getTeams() {
        return this.teams;
    }

    public void setTeams(Integer teams) {
        this.teams = teams;
    }



    @Column(name = "type", nullable = false, length = 32)
    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "back_ground")
    public String getBackGround() {
        return this.backGround;
    }

    public void setBackGround(String backGround) {
        this.backGround = backGround;
    }

    @Column(name = "reward_type")
    public String getRewardType() {
        return this.rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    @Column(name = "reward_type_info")
    public String getRewardTypeInfo() {
        return this.rewardTypeInfo;
    }

    public void setRewardTypeInfo(String rewardTypeInfo) {
        this.rewardTypeInfo = rewardTypeInfo;
    }

    @Column(name = "uplaod_type")
    public String getUplaodType() {
        return this.uplaodType;
    }

    public void setUplaodType(String uplaodType) {
        this.uplaodType = uplaodType;
    }

    @Column(name = "time_process")
    public String getTimeProcess() {
        return timeProcess;
    }

    public void setTimeProcess(String timeProcess) {
        this.timeProcess = timeProcess;
    }

    @Column(name = "banner")
    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    @Column(name = "history")
    public Boolean getHistory() {
        return history;
    }

    public void setHistory(Boolean history) {
        this.history = history;
    }

    @Column(name = "index_image")
    public String getIndexImage() {
        return indexImage;
    }

    public void setIndexImage(String indexImage) {
        this.indexImage = indexImage;
    }

    @Column(name = "rank_index")
    public Integer getRankIndex() {
        return rankIndex;
    }

    public void setRankIndex(Integer rankIndex) {
        this.rankIndex = rankIndex;
    }

    @Column(name = "mold")
    public String getMold() {
        return mold;
    }

    public void setMold(String mold) {
        this.mold = mold;
    }

    @Column(name="level")
    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
    @Column(name="competitor_num")
    public Integer getCompetitorNum() {
        return competitorNum;
    }

    public void setCompetitorNum(Integer competitorNum) {
        this.competitorNum = competitorNum;
    }
    @Column(name="competitor_fake_num")
    public Integer getCompetitorFNum() {
        return competitorFNum;
    }

    public void setCompetitorFNum(Integer competitorFakeNum) {
        this.competitorFNum = competitorFakeNum;
    }
    @Column(name="sort")
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}