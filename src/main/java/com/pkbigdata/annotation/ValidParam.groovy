package com.pkbigdata.annotation

import java.lang.annotation.*

/**
 * Created by wd on 2017/3/31.
 */
@Target([ElementType.METHOD])
@Retention(RetentionPolicy.RUNTIME)
@Documented
@interface ValidParam {

}