package com.pkbigdata.annotation.util

import com.pkbigdata.service.util.RedisUtil
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.AfterReturning
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.reflect.MethodSignature
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.LocalVariableTableParameterNameDiscoverer
import org.springframework.expression.EvaluationContext
import org.springframework.expression.Expression
import org.springframework.expression.common.TemplateParserContext
import org.springframework.expression.spel.standard.SpelExpressionParser
import org.springframework.expression.spel.support.StandardEvaluationContext
import org.springframework.stereotype.Component

import java.lang.reflect.Method


/**
 * Created by jay on 2016/11/29 0029.
 * 缓存主动更新设置，aop
 */
@Aspect
@Component
class CacheRemove {
    @Value('${cachePrefix}')
    String cachePrefix;

    @Autowired
    RedisUtil redisUtil;

    /**
     * 通过注解的方式去更新缓存
     * @param point
     */
    @AfterReturning(value = "@annotation(com.pkbigdata.annotation.CacheRemove)",returning = "returnValue")
    public void afterReturning(JoinPoint point, def returnValue){
        try {
            def annotation = ((MethodSignature)point.getSignature()).getMethod().getAnnotation(com.pkbigdata.annotation.CacheRemove.class)
            def paramName = getParams(((MethodSignature)point.getSignature()).getMethod()).each {
                println(it.toString())
            }
            if (annotation.condition()&& (!annotation.condition().equals(""))){
                def condition = el(annotation.condition(),returnValue,paramName,point.getArgs())
                if (condition==false){//condition执行为真才继续清除，否则直接跳过
                    println("缓存condition为false,跳过")
                    return
                }
            }
            String key = annotation.cacheClass().getName()
            if (annotation.method().length==1){//单method
                key+=annotation.method()[0]
                if ("" != annotation.key()){
                    key+=annotation.key()
                    redisUtil.remove(cachePrefix+key)
                }else {
                    redisUtil.removeByPrefix(cachePrefix+key)
                }
            }else{
                annotation.method().each {
                    redisUtil.removeByPrefix(cachePrefix+key+it)
                }
            }
            println("清除的缓存key："+cachePrefix+key)
        }catch (Exception e){
            println("清除的缓存异常")
            e.printStackTrace()
        }catch(Error e){
            e.printStackTrace()
        }
    }

    /**
     * el表达式执行结果
     * @param el
     * @param returnValue
     * @param paramName
     * @param param
     * @return
     */
    static def el(String el,Object returnValue,String[] paramName,Object[] param) {
        Expression expression = new SpelExpressionParser().parseExpression(el, new TemplateParserContext());
        EvaluationContext context = new StandardEvaluationContext();
        if (paramName?.size()!=param?.size()){
            throw new Exception("清除缓存的参数异常")
        }
        paramName.eachWithIndex {e,i->
            context.setVariable(e, param[i]);
        }
        context.setVariable('returnValue', returnValue);
        try {
            return expression.getValue(context)
        }catch (Exception e) {
            e.printStackTrace()
        }catch(IllegalAccessError e){
            e.printStackTrace()
        }catch(Error e){
            e.printStackTrace()
        }
        return false
    }

    /**
     * 获取方法的参数名列表
     * @param method
     */
     static String[] getParams(Method method){
        new LocalVariableTableParameterNameDiscoverer().getParameterNames(method)
    }

}
