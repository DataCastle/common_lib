package com.pkbigdata.annotation.util

import com.pkbigdata.model.Model
import com.pkbigdata.model.Mv
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.reflect.MethodSignature
import org.springframework.stereotype.Component
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes

import javax.servlet.http.HttpServletRequest
import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.ValidatorFactory
import javax.validation.executable.ExecutableValidator
import java.lang.reflect.Method

/**
 * Created by wd on 2017/3/31.
 * 写在 返回Model()的函数上。
 * 目前测试结果是：
 * 几乎可以使用所有的标准限制注解，如果有参数错误，会返回Model()
 * msg=message1</br>message2
 * flag=false
 * 如果限制内没有写message，会显示默认值
 * 在如下情况下：
 * *@NotNull(message = "名字不能为空") @RequestParam(value = "uname" ,defaultValue = "") String uname,
 * uname为空时会设置默认值，NotNull不会起作用。
 */
@Aspect
@Component
class ValidParam {
    static private ExecutableValidator validator
    static {
        ValidatorFactory factory=Validation.buildDefaultValidatorFactory()
        validator=factory.getValidator().forExecutables()
        if(validator==null){
            throw new IllegalStateException("@ValidParam ,validator init failed")
        }
    }

    @Around(value ="@annotation(com.pkbigdata.annotation.ValidParam)")
    public Object valid(ProceedingJoinPoint point){
        Object target=point.getTarget()
        //println(target.getClass().getName()+"  origin="+AopUtils.getTargetClass(target).getClass().getName())
        Object[] params=point.getArgs()
        Method method=((MethodSignature)point.getSignature()).getMethod()
        Set<ConstraintViolation<?>> violations=validator.validateParameters(
                target,
                method,
                params
        )
        int error=violations.size()
        if(error>0){
            StringBuilder sb=new StringBuilder()
            for(ConstraintViolation<?> violation:violations) {
                sb.append(violation.getMessage()+"</br>")
            }
            //有错误，返回根据情况返回model 或者Mv
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder .getRequestAttributes()).getRequest()
            String requestType = request.getHeader("X-Requested-With")
            String url=request.getRequestURL().toString()
            //println("url=${url},type=${requestType}")
            if(requestType==null && !url.endsWith(".json")){
                //如果是页面
                return new Mv("/common/404").end()
            }else{
                //如果是json 或者异步请求
                Model model=new Model()
                String msg=sb.toString()
                msg=msg.substring(0,msg.length()-5)
                return model.setMsg(msg)
            }
        }else{
            //没有错误，继续执行
            return point.proceed()
        }
    }
}
