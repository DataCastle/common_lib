package com.pkbigdata.annotation.util

import com.pkbigdata.model.Model
import com.pkbigdata.model.Mv
import com.pkbigdata.service.util.RedisUtil
import com.pkbigdata.util.CommonUtil
import com.pkbigdata.util.JsonToMap
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.reflect.MethodSignature
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.util.concurrent.TimeUnit

/**
 * Created by jay on 2017/3/30.
 */
@Aspect
@Component
class PassCheck {
    @Autowired
    RedisUtil redisUtil;

    static String RobotURL = "/static_page/robot.html"

    @Around(value = "@annotation(com.pkbigdata.annotation.PassCheckIp)")
    public  Object passCheckIp(ProceedingJoinPoint  point){
        def annotation = ((MethodSignature)point.getSignature()).getMethod().getAnnotation(com.pkbigdata.annotation.PassCheckIp.class)
        if (annotation.day().length!=2){
            throw new Exception("单位天的格式错误")
        }
        if (annotation.hour().length!=2){
            throw new Exception("单位小时的格式错误")
        }
        if (annotation.minute().length!=2){
            throw new Exception("单位分钟的格式错误")
        }
        if (annotation.day()[0]<=0&&
                annotation.hour()[0]<=0&&
                annotation.minute()[0]<=0){
            return point.proceed()
        }

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder .getRequestAttributes()).getRequest();
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        String requestType = request.getHeader("X-Requested-With");
        //获取实际的访问地址
        String url = request.getRequestURL().toString()


        def ip = CommonUtil.getRemortIP(request)
        if (annotation.day()[1]){
            def result = check(response,requestType,annotation.day(),url,ip,"DAYS")
            if (result!=true){
                return result
            }
        }
        if (annotation.hour()[1]){
            def result = check(response,requestType,annotation.hour(),url,ip,"HOURS")
            if (result!=true){
                return result
            }
        }
        if (annotation.minute()[1]){
            def result = check(response,requestType,annotation.minute(),url,ip,"MINUTES")
            if (result!=true){
                return result
            }
        }
        return point.proceed()
    }

    def check(HttpServletResponse response,String requestType,int[] times,String url,String ip,String timeType){
        def sendRedirect={String redirUrl->
            if(requestType==null && !url.endsWith(".json")){
                return new Mv("redirect:"+redirUrl)
            } else {
                return new Model().setFlag(false).put("url_redirect",redirUrl)
            }
        }
        if (!redisUtil.getValue("${ip}_IP_${timeType}_EXPIRE_${url}")){
            redisUtil.addValue("${ip}_IP_${timeType}_EXPIRE_${url}","EXPIRE",times[0],TimeUnit[timeType] as TimeUnit)
            redisUtil.addValue("${ip}_IP_${timeType}_${url}",1 as Integer,30,TimeUnit[timeType] as TimeUnit)
        }else{
            def time = redisUtil.getValue("${ip}_IP_${timeType}_${url}")+1
            if (time>times[1]){
                //限制访问了，可以做图形验证码
                return sendRedirect(RobotURL)
            }
            redisUtil.addValue("${ip}_IP_${timeType}_${url}",time)
        }
        return true
    }


    @Around(value = "@annotation(com.pkbigdata.annotation.PassCheckSession)")
    public  Object passCheckSession(ProceedingJoinPoint  point){
        def annotation = ((MethodSignature)point.getSignature()).getMethod().getAnnotation(com.pkbigdata.annotation.PassCheckSession.class)
        if (annotation.day().length!=2){
            throw new Exception("单位天的格式错误")
        }
        if (annotation.hour().length!=2){
            throw new Exception("单位小时的格式错误")
        }
        if (annotation.minute().length!=2){
            throw new Exception("单位分钟的格式错误")
        }
        if (annotation.day()[0]<=0&&
                annotation.hour()[0]<=0&&
                annotation.minute()[0]<=0){
            return point.proceed()
        }

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder .getRequestAttributes()).getRequest();
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        String requestType = request.getHeader("X-Requested-With");
        //获取实际的访问地址
        String url = request.getRequestURL().toString()


        def ip = request.getSession().getId()
        if (annotation.day()[1]){
            def result = check(response,requestType,annotation.day(),url,ip,"DAYS")
            if (result!=true){
                return result
            }
        }
        if (annotation.hour()[1]){
            def result = check(response,requestType,annotation.hour(),url,ip,"HOURS")
            if (result!=true){
                return result
            }
        }
        if (annotation.minute()[1]){
            def result = check(response,requestType,annotation.minute(),url,ip,"MINUTES")
            if (result!=true){
                return result
            }
        }
        return point.proceed()
    }



    /**
     * 异步接口，返回json数据
     *
     * @param response
     * @throws IOException
     */
    static void returnLoginJson(HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        Model model = new Model()
        model.setFlag(false)
        model.put("url_redirect",RobotURL)
        out.print(JsonToMap.beanToJson(model));
        response.setStatus(200);
        out.flush();
        out.close();
    }
}
