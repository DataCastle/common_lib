package com.pkbigdata.annotation

import java.lang.annotation.*

/**
 * Created by jay on 2017/3/30.
 * 不采用自然时刻，使用相对时间。限制同一个会话的访问频率
 * 数组的长度必须等于2,不然抛出异常
 * indexPage 0 为时间单位，indexPage 1为次数
 */
@Target([ElementType.METHOD, ElementType.TYPE ])
@Retention(RetentionPolicy.RUNTIME)
@Documented
@interface PassCheckSession {
    int[] minute() default [0,0]
    int[] hour() default [0,0]
    int[] day() default [0,0]
}